import es from "../../../resources/locale/es";

const languageList = {
    es
};

export default languageList;
