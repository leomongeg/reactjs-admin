/**
 * Allows return a complex value with a cleaned value and the selected values
 */
export interface CleanedAndValue {
    value: any;
    cleaned: any[];
}