export const showSpinner = (btn: any) => {
    btn.checked = false;
    btn.spinner = true;
};

export const showCheck = (btn: any) => {
    btn.spinner = false;
    btn.checked = true;
};

export const showText = (btn: any) => {
    btn.checked = false;
    btn.spinner = false;
};