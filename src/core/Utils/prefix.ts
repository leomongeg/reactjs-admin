import Container              from "typedi";
import { ApplicationStore }   from "../Store/ApplicationStore";
import { ConfigurationStore } from "../Store/ConfigurationStore";

export const prefix = Container.get(ApplicationStore).getStore(ConfigurationStore).getConfiguration().getPrefix();