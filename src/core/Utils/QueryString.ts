export const findGetParameter = (parameterName: string): string|undefined => {
    let result = undefined,
        tmp = [];
    location.search
            .substr(1)
            .split("&")
            .forEach((item) => {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            });
    return result;
};