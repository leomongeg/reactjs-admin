/**
 * Merge information into a destination, it creates not existing fields
 * @example: let updatedFields = EntityMerge.merge(origin, destination);
 * @param origin
 * @param destination
 * @param force
 * @param ignore
 */
export function merge(origin: any, destination: any, force: boolean = false, ignore?: string[]) {
    let fieldsUpdated = 0;
    for (const attribute in origin) {
        // skip ignore fields
        if (ignore !== undefined && ignore.indexOf(attribute) !== -1) {
            continue;
        }
        if (Array.isArray(origin[attribute])) {
            destination[attribute] = origin[attribute].map((item: any, index: number) => {
                const result = (destination[attribute] !== undefined) ? (destination[attribute][index] !== undefined ? destination[attribute][index] : {}) : {};
                merge(item, result);
                return result;
            });
            fieldsUpdated++;
        }
        // Process sub objects validating fields
        else if (typeof origin[attribute] === "object") {
            // Validate ObjectId
            if (origin[attribute]._bsontype !== undefined && origin[attribute]._bsontype === "ObjectID") {
                destination[attribute] = origin[attribute];
                fieldsUpdated++;
                return fieldsUpdated;
            }
            else if (destination[attribute] === undefined) {
                destination[attribute] = {}; // Initialize field
            }
            fieldsUpdated = fieldsUpdated + merge(origin[attribute], destination[attribute]);
            /*if (Array.isArray(origin[attribute])) {
                const keys = Object.keys(origin[attribute]);
                destination[attribute] = keys.map(key => destination[attribute][key]);
            }*/
        }
        else if ((destination[attribute] !== origin[attribute]
            && origin[attribute] !== "" && origin[attribute] !== undefined) || force) {
            destination[attribute] = origin[attribute];
            fieldsUpdated++;
        }
    }
    return fieldsUpdated;
}

/**
 * Clone a object into a new instance
 * @param origin
 */
export function clone<T>(origin: T, ignore?: string[]) {
    if (origin === undefined) {
        return origin;
    }
    const cloned = {} as T;
    merge(origin, cloned, true, ignore);
    return cloned;
}