import { CRUDRoute }                    from "../Admin/Base/BaseAdmin";
import { AxiosInstance, AxiosResponse } from "axios";

export const body = document.getElementsByTagName("body")[0];

/**
 * Convert all the first character of the given string uppercase.
 *
 * @param text
 */
export function ucFirst(text: string) {
    text = text.toLowerCase()
               .split(" ")
               .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
               .join(" ");

    return text;
}

/**
 * Format the total amount
 *
 * @param amount
 */
export function formatAmount(amount: number): string {
    return parseFloat(amount.toString()).toFixed(2)
                                        .replace(/\d(?=(\d{3})+\.)/g, "$& ");
}

/**
 * Fix any accidental duplicated // in a generated url.
 *
 * @param url
 */
export function fixUrl(url: string) {
    return url.replace(/([^:]\/)\/+/g, "$1");
}

/**
 * Build the request options
 *
 * @param options
 * @param entityIdentifier
 * @param modelObject
 * @private
 */
export function buildRequestOptionsObj(options: CRUDRoute | string, defaultMethod: "POST" | "GET" | "DELETE" | "PUT"): CRUDRoute {
    if (typeof options !== "string")
        return options;
    return {
        method: defaultMethod,
        path  : options,
    };
}

/**
 * Make API Request.
 *
 * @param options
 * @private
 */
export async function makeRequest(options: { endpoint: string, params: any | undefined, method: string }, axios: AxiosInstance): Promise<AxiosResponse | undefined> {
    let response;
    switch (options.method) {
        case "DELETE":
            response = await axios.delete(options?.endpoint || "", {
                params: options?.params
            });
            break;
        case "GET":
            response = await axios.get(options?.endpoint || "", {
                params: options?.params
            });
            break;
        case "POST":
            response = await axios.post(options?.endpoint || "", options?.params);
            break;
        case "PUT":
            response = await axios.put(options?.endpoint || "", options?.params);
            break;
    }
    return response;
}