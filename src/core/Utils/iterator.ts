/**
 * cm-profile-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 12/01/18
 */

export default {
    iterator: function* () {
        const properties = Object.keys(this);
        for (const i of properties) {
            yield [i, this[i]];
        }
    }
};