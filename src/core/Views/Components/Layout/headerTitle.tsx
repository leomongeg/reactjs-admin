import React from "react";

/**
 * Header title component fragment
 */
function headerTitle() {
    return (
        <React.Fragment>
            <div className="">
                <h1 className="">Header Title</h1>
            </div>
        </React.Fragment>
    );
}

export default headerTitle;