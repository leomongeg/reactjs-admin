import * as React                                from "react";
import { Switch }                                from "react-router-dom";
import Routes, { IRoute }                        from "../../../Routes/routes";
import Route                                     from "../Route/Route";
import { ROUTING_CONFIG }                        from "../../../../config/routing.config";
import FooterViewModel                           from "../../../ViewModel/Footer/FooterViewModel";
import HeaderViewModel                           from "../../../ViewModel/Header/HeaderViewModel";
import SimpleReactValidatorLocaleLoader          from "../../../Utils/i18n/SimpleReactValidatorLocaleLoader";
import { createStyles, CssBaseline, withStyles } from "@material-ui/core";
import { createMuiTheme }                        from "@material-ui/core/styles";
import { ThemeProvider }                         from "@material-ui/styles";
import BreadcrumbView                            from "../Navigation/Breadcrumb/BreadcrumbView";
import { Container }                             from "typedi";
import { AdminService }                          from "../../../Service/AdminService";
import ThemeConfig                               from "../../../../config/ThemeConfig";
import { ThemeOptions }                          from "@material-ui/core/styles/createMuiTheme";
import ModalViewModel                            from "../../../ViewModel/Components/SystemModal/ModalViewModel";


class ScrollToTop extends React.Component<any, any> {
    /**
     * React component life cycle event
     * @param prevProps
     */
    public componentDidUpdate(prevProps: any) {
        window.scrollTo(0, 0);
        // if (this.props.location !== prevProps.location) {
        //     window.scrollTo(0, 0);
        // }
    }

    /**
     * Component render
     */
    public render(): React.ReactNode {
        return this.props.children;
    }
}


class Layout extends React.Component<any, any> {
    private readonly routes: IRoute[];

    constructor(props: any) {
        super(props);
        this.routes = Routes;
        Container.get(AdminService).initializeAdmins(this.routes);
    }

    /**
     * render
     */
    public render(): React.ReactNode {
        const {classes} = this.props;

        return (
            <ThemeProvider theme={theme}>
                <main>
                    <ScrollToTop>
                        <SimpleReactValidatorLocaleLoader locale="es">
                            <div className={classes.root}>
                                <CssBaseline/>
                                <HeaderViewModel/>
                                <main className={classes.content}>
                                    <div className={classes.toolbar}/>
                                    <BreadcrumbView/>
                                    <Switch>
                                        {this.routes.map((page, key) => {
                                            return (
                                                <Route
                                                    exact={page.exact}
                                                    auth={page.auth}
                                                    path={ROUTING_CONFIG.ROUTING_PREFIX + page.path}
                                                    name={page.name}
                                                    key={page.name}
                                                    layout={page!.layout}
                                                    component={page.component}
                                                    middleware={page.middleware}
                                                    options={page.options}
                                                    adminClassInstance={page.adminClassInstance}
                                                />
                                            );
                                        })}
                                    </Switch>
                                </main>
                                <FooterViewModel/>
                                <ModalViewModel/>
                            </div>
                        </SimpleReactValidatorLocaleLoader>
                    </ScrollToTop>
                </main>
            </ThemeProvider>
        );
    }
}

/**
 * Customize the theme options here
 */
const theme = createMuiTheme(ThemeConfig.themeOptions as ThemeOptions);

const styles = createStyles((theme: any) => ({
    toolbar: theme.mixins.toolbar,
    root   : {
        display: "flex",
    },
    content: {
        flexGrow: 1,
        padding : theme.spacing(3),
    }
}));

export default withStyles(styles)(Layout);
