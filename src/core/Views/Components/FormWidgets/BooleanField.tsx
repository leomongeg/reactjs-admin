import React, { Component, ReactNode } from "react";
import FormControlLabel                from "@material-ui/core/FormControlLabel";
import Switch                          from "@material-ui/core/Switch";
import FormHelperText                  from "@material-ui/core/FormHelperText";
import FormControl                     from "@material-ui/core/FormControl";
import { withStyles, WithStyles }      from "@material-ui/core";
import { createStyles } from "@material-ui/core/styles";
import IBaseProps       from "../../../ViewModel/Props/IBaseProps";

/**
 * Boolean Field props definition
 */
export interface BooleanFieldProps extends WithStyles, IBaseProps {
    label: string;
    // options: any[];
    helperText: string;
    className: string;
    propertyKey: string;
    elementProps: any;
    setViewRef: any;
}

class BooleanField extends Component<BooleanFieldProps, any> {
    /**
     * Component render
     */
    render(): ReactNode {
        const {helperText, className, classes, value, label, propertyKey, onChange} = this.props;
        const elementProps                                                          = {...this.props};
        delete elementProps.propertyKey;
        delete elementProps.helperText;
        delete elementProps.label;
        delete elementProps.className;
        delete elementProps.classes;
        delete elementProps.onChange;
        delete elementProps.error;
        delete elementProps.elementProps;
        delete elementProps.setViewRef;

        return (<FormControl className={className}>
            <FormControlLabel
                className={classes.label}
                labelPlacement="start"
                control={
                    <Switch
                        {...elementProps}
                        checked={value}
                        name={propertyKey}
                        onChange={(event: any, checked: boolean) => {
                            onChange(checked);
                        }}
                    />
                }
                label={label}
            />
            <FormHelperText variant="outlined">{helperText}</FormHelperText>
        </FormControl>);
    }
}

const styles = createStyles((theme: any) => ({
    label: {
        flexDirection: "row",
        marginLeft   : 0
    }
}));

export default withStyles(styles)(BooleanField);