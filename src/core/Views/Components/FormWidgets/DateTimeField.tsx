import React, { Component, ReactElement, ReactNode } from "react";
import MomentUtils                                   from "@date-io/moment";
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
    KeyboardDateTimePicker,
    DateTimePicker,
    DatePicker,
    TimePicker
}                                                    from "@material-ui/pickers";
import FormControl                                   from "@material-ui/core/FormControl";
import FormHelperText                                from "@material-ui/core/FormHelperText";
import moment, { Moment }                            from "moment";

export enum DateTimePickerFieldType {
    KEYBOARD_PICKER_DATE     = "KEYBOARD_PICKER_DATE",
    KEYBOARD_PICKER_TIME     = "KEYBOARD_PICKER_TIME",
    KEYBOARD_PICKER_DATETIME = "KEYBOARD_PICKER_DATETIME",
    PICKER_DATETIME          = "PICKER_DATETIME",
    PICKER_DATE              = "PICKER_DATE",
    PICKER_TIME              = "PICKER_TIME"
}

/**
 * Define the DateTime Field props
 */
interface DateTimeFieldProps {
    dateUtilsProvider?: any;
    pickerType?: DateTimePickerFieldType;

    label: string;
    // options: any[];
    helperText: string;
    className: string;
    propertyKey: string;
    value: any;
    onChange: (val: any) => void;
    elementProps: any;
    setViewRef: any;
}

class DateTimeField extends Component<DateTimeFieldProps, any> {

    /**
     * Return the proper picker according to the type prop
     * @private
     */
    private getPickerElement(): any {
        const {pickerType} = this.props;
        let pickerElement;
        switch (pickerType) {
            case DateTimePickerFieldType.PICKER_DATE:
                pickerElement = DatePicker;
                break;
            case DateTimePickerFieldType.PICKER_TIME:
                pickerElement = TimePicker;
                break;
            case DateTimePickerFieldType.KEYBOARD_PICKER_DATE:
                pickerElement = KeyboardDatePicker;
                break;
            case DateTimePickerFieldType.KEYBOARD_PICKER_TIME:
                pickerElement = KeyboardTimePicker;
                break;
            case DateTimePickerFieldType.KEYBOARD_PICKER_DATETIME:
                pickerElement = KeyboardDateTimePicker;
                break;
            default:
                pickerElement = DateTimePicker;
                break;
        }

        return pickerElement;
    }


    /**
     * Component Render
     */
    render(): ReactNode {
        const {className, helperText, dateUtilsProvider, propertyKey, onChange, value} = this.props;
        const elementProps                                                      = {...this.props};
        delete elementProps.propertyKey;
        delete elementProps.helperText;
        delete elementProps.className;
        delete elementProps.pickerType;
        delete elementProps.onChange;
        delete elementProps.dateUtilsProvider;
        delete elementProps.elementProps;
        delete elementProps.setViewRef;

        const PickerElement = this.getPickerElement();
        return (
            <FormControl className={className}>
                <MuiPickersUtilsProvider utils={dateUtilsProvider || MomentUtils}>
                    <PickerElement
                        {...elementProps}
                        onChange={(value: any) => {
                            if (value)
                                onChange((value as Moment).toDate());
                            else onChange(value);
                        }}
                        InputLabelProps={{
                            shrink: value !== undefined && value !== ""
                        }}
                        id={`date-picker-${propertyKey}`}
                    /></MuiPickersUtilsProvider>
                <FormHelperText variant="outlined">{helperText}</FormHelperText>
            </FormControl>);
    }
}

export default DateTimeField;