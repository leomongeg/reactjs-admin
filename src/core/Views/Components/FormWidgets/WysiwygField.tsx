import React, { Component, ReactNode }      from "react";
import { createStyles, withStyles }         from "@material-ui/core";
import { WithTranslation, withTranslation } from "react-i18next";
import InputLabel                           from "@material-ui/core/InputLabel";
import FormHelperText                       from "@material-ui/core/FormHelperText";
import FormControl                          from "@material-ui/core/FormControl";
import { ClassKeyOfStyles, ClassNameMap }   from "@material-ui/styles/withStyles";
import {
    CKEditorDefaultOnInit, CKEditorDefaultOnProcessBody,
    CKEditorDefaultUploadAdapter
}                                           from "../../../UploadAdapter/CKEditorDefaultUploadAdapter";
import { defaultConfig }                    from "../../../../config/CKEditorConfig";

const CKEditor      = require("@ckeditor/ckeditor5-react");
const ClassicEditor = require("ckeditor5-build-classic");


/**
 * Field Widget props
 */
interface WysiwygFieldProps extends WithTranslation {
    onInitImageUploadRequest?: CKEditorDefaultOnInit;
    onProcessBodyImageUpload?: CKEditorDefaultOnProcessBody;
    onProcessImageUploadResponse?: any;
    placeholder?: string;
    editorConfig?: any;

    // Default props
    label: string;
    helperText: string;
    className: string;
    propertyKey: string;
    value: any;
    onChange: (val: any) => void;
    classes: ClassNameMap<ClassKeyOfStyles<string>>;
    error: boolean;
}

class WysiwygField extends Component<WysiwygFieldProps, any> {

    /**
     * Component Render
     */
    render(): ReactNode {
        const {
                  t, value, label, classes, className, helperText, propertyKey, error, onChange,
                  onInitImageUploadRequest, onProcessBodyImageUpload,
                  editorConfig, placeholder, onProcessImageUploadResponse
              } = this.props;

        const config       = {...(editorConfig || defaultConfig)};
        config.placeholder = t(placeholder || "");

        return (<FormControl className={`${className} ${classes.fileWidgetContent}`}>
            <InputLabel className={`MuiInputLabel-shrink ${classes.label}`}
                        htmlFor={`select-${propertyKey}`}>{label}</InputLabel>
            <div className={classes.editorContainer}>
                <CKEditor
                    editor={ClassicEditor}
                    data={value}
                    config={config}
                    onInit={(editor: any) => {
                        if (onInitImageUploadRequest && onProcessBodyImageUpload && onProcessImageUploadResponse) {
                            editor.plugins.get("FileRepository").createUploadAdapter = (loader: any) => {
                                return new CKEditorDefaultUploadAdapter(loader,
                                                                        onInitImageUploadRequest,
                                                                        onProcessBodyImageUpload,
                                                                        onProcessImageUploadResponse);
                            };
                        }
                    }}
                    onChange={(event: any, editor: any) => {
                        onChange(editor.getData());
                    }}
                    onBlur={(event: any, editor: any) => {
                        onChange(editor.getData());
                    }}
                    onFocus={(event: any, editor: any) => {
                    }}
                />
            </div>
            <FormHelperText
                error={!!error}
                variant="outlined">{helperText}</FormHelperText>
        </FormControl>);
    }

}

const styles = createStyles((theme: any) => ({
    fileWidgetContent: {
        textAlign : "left",
        "& form"  : {
            "& div": {
                marginBottom: "0.5em"
            }
        },
        "& button": {
            marginBottom: "0.5em"
        }
    },
    label            : {
        position    : "relative",
        marginBottom: ".5em",
    },
    card             : {
        display: "flex"
    },
    details          : {
        display      : "flex",
        flexDirection: "column",
    },
    content          : {
        flex: "1 0 auto",
    },
    cover            : {
        width: 151,
    },
    editorContainer  : {
        color: "black"
    }
}));

export default withStyles(styles)(withTranslation("")((WysiwygField)));