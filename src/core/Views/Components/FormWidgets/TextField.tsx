import React, { Component, ReactNode }                                     from "react";
import MaterialUITextField, { TextFieldProps as MaterialUITextFieldProps } from "@material-ui/core/TextField";

/**
 * Form Widget TextField props
 */
export type TextFieldProps = MaterialUITextFieldProps & {
    label: string;
    propertyKey: string;
    elementProps: any;
    setViewRef: any;
};

class TextField extends Component <TextFieldProps, any> {

    /**
     * Component Render
     */
    render(): ReactNode {
        const {onChange}   = this.props;
        const elementProps = {...this.props};
        delete elementProps.propertyKey;
        delete elementProps.onChange;
        delete elementProps.elementProps;
        delete elementProps.setViewRef;
        return (<MaterialUITextField
            {...elementProps}
            onChange={(event: any) => {
                if (onChange) onChange(event.target.value);
            }}
        />);
    }
}

export default TextField;