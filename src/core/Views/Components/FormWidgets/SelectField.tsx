import React, { Component, ReactNode } from "react";
import MaterialUINativeSelect          from "@material-ui/core/NativeSelect";
import { NativeSelectProps }           from "@material-ui/core/NativeSelect/NativeSelect";
import InputLabel                      from "@material-ui/core/InputLabel";
import FormHelperText                  from "@material-ui/core/FormHelperText";
import FormControl                     from "@material-ui/core/FormControl";

/**
 * Form Widget TextField props
 */
export type SelectFieldProps = NativeSelectProps & {
    label: string;
    options: any[];
    onOptionMapper?: (option: any) => { value: string, displayText: string };
    helperText: string;
    propertyKey: string;
    elementProps: any;
    setViewRef: any;
};

class SelectField extends Component<SelectFieldProps, any> {

    /**
     * Map options to render
     */
    private mapOptions(options: any[]): any {
        const {onOptionMapper} = this.props;
        return options.map((_option: any, index: number) => {
            const option = onOptionMapper !== undefined ? onOptionMapper(_option) : _option;
            return (<option key={`option-${index}`} value={option.value}>{option.displayText}</option>);
        });
    }

    /**
     * Component Render
     */
    render(): ReactNode {
        const {options, label, helperText, propertyKey, onChange} = this.props;

        const elementProps = {...this.props};
        delete elementProps.helperText;
        delete elementProps.onOptionMapper;
        delete elementProps.propertyKey;
        delete elementProps.onChange;
        delete elementProps.elementProps;
        delete elementProps.setViewRef;

        return (<FormControl className={this.props.className}>
            <InputLabel htmlFor={`select-${propertyKey}`}>{label}</InputLabel>
            <MaterialUINativeSelect
                inputProps={{
                    name: propertyKey,
                    id  : `select-${propertyKey}`,
                }}
                {...elementProps}
                onChange={(event: any) => {
                    if (onChange) onChange(event.target.value);
                }}
            >
                <option aria-label="None" value=""/>
                {
                    this.mapOptions(options)
                }
            </MaterialUINativeSelect>
            <FormHelperText variant="outlined">{helperText}</FormHelperText>
        </FormControl>);
    }
}

export default SelectField;