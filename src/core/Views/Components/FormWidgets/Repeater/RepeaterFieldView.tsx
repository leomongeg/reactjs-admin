import React, { Component, ReactNode }                        from "react";
import { createStyles, WithStyles, withStyles }               from "@material-ui/core";
import { withTranslation }                                    from "react-i18next";
import IBaseProps                                             from "../../../../ViewModel/Props/IBaseProps";
import InputLabel                                             from "@material-ui/core/InputLabel";
import FormControl                                            from "@material-ui/core/FormControl";
import { FormSchema, IFormSection, IFormWidgetSchema }        from "../../../../Admin/Base/FormSchema";
import Paper                                                  from "@material-ui/core/Paper";
import Grid                                                   from "@material-ui/core/Grid";
import WidgetWrapperViewModel, { WidgetWrapperViewModelType } from "../../../../ViewModel/Base/WidgetWrapperViewModel";
import Button                                                 from "@material-ui/core/Button";
import SimpleReactValidator                                   from "simple-react-validator";
import { observer }                                           from "mobx-react";
import FormHelperText                                         from "@material-ui/core/FormHelperText";
import AddBoxIcon                                             from "@material-ui/icons/AddBox";
import DeleteIcon                                             from "@material-ui/icons/Delete";
import IconButton                                             from "@material-ui/core/IconButton";
import Accordion                                              from "@material-ui/core/Accordion";
import AccordionSummary                                       from "@material-ui/core/AccordionSummary";
import AccordionDetails                                       from "@material-ui/core/AccordionDetails";
import Typography                                             from "@material-ui/core/Typography";
import AddIcon                                                from "@material-ui/icons/Add";
import RemoveIcon                                             from "@material-ui/icons/Remove";

/**
 * Component props
 */
interface RepeaterFieldViewProps extends IBaseProps, WithStyles {
    propertyKey: string;
    formSchemas: FormSchema[];
    label: string;
    validator: SimpleReactValidator;
    insertWidgetInLeaf: (leaf: number, widget: WidgetWrapperViewModelType) => void;
    addOther: () => void;
    helperText: string;
    repeaterItemsLabel: (t: any, index: number) => string;
    deleteElement: (index: number) => void;
    setRef: (ref: any) => void;
    panelExpanded: boolean;
    handleExpandable: (panel: string) => (event: React.ChangeEvent<{}>, isExpanded: boolean) => void;
}

@observer
class RepeaterFieldView extends Component<RepeaterFieldViewProps, any> {

    constructor(props: RepeaterFieldViewProps) {
        super(props);
        props.setRef(this);
    }

    /**
     * Render widget
     * @private
     */
    private renderWidgets(section: IFormSection, schemaIndex: number, key: string) {
        const {classes, validator, insertWidgetInLeaf} = this.props;
        return (section.widgets.map((widget: IFormWidgetSchema, itemIndex: number) => {
            return (<WidgetWrapperViewModel
                className={classes.fullWidth}
                key={`widget-${schemaIndex}-${key}-${itemIndex}-${widget.key}`}
                validator={validator}
                objectKey={widget.key}
                widget={widget}
                setViewRef={(widget: WidgetWrapperViewModelType) => insertWidgetInLeaf(schemaIndex, widget)}
            />);
        }));
    }


    /**
     * Component View render
     */
    render(): ReactNode {
        const {
                  t, classes, label, propertyKey, formSchemas, addOther, panelExpanded,
                  helperText, repeaterItemsLabel, deleteElement, handleExpandable
              } = this.props;
        return (<FormControl className={classes.fileWidgetContent}>
            <Accordion expanded={panelExpanded} onChange={handleExpandable(propertyKey)}>
                <AccordionSummary
                    expandIcon={panelExpanded ? <RemoveIcon/> : <AddIcon/>}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header"
                >
                    <Typography className={classes.heading}>{label}</Typography>
                    <Typography className={classes.secondaryHeading}>{helperText}</Typography>
                </AccordionSummary>
                <AccordionDetails className={classes.accordionDetails}>
                    <Paper className={classes.repeaterContainer}>
                        {
                            formSchemas.map((schema: FormSchema, schemaIndex: number) => {
                                const sectionsCount = schema.sections.size;
                                return (
                                    <React.Fragment key={`repeater-${propertyKey}-${schema.uuid}`}>
                                        <Paper className={`${classes.repeaterContainer} ${classes.fullWidth}`}>
                                            <div className={classes.itemInfo}>
                                                <InputLabel className={`MuiInputLabel-shrink ${classes.label}`}
                                                            htmlFor={`select-${propertyKey}`}>{
                                                    repeaterItemsLabel ? repeaterItemsLabel(t, schemaIndex) :
                                                    t("__repeater_item_label_index__", {index: schemaIndex + 1})
                                                }</InputLabel>
                                                <IconButton
                                                    aria-label="delete"
                                                    onClick={() => {deleteElement(schemaIndex); }}
                                                >
                                                    <DeleteIcon fontSize="small"/>
                                                </IconButton>
                                            </div>
                                            <Grid container spacing={2}>

                                                {
                                                    Array.from(schema.sections.keys()).map((key: string, index: number) => {
                                                        const section = schema.sections.get(key);
                                                        if (section === undefined || section.widgets.length === 0) return undefined;


                                                        return (
                                                            <Grid key={`grid-${schemaIndex}-${key}`}
                                                                  item {...section.breakpoints}>

                                                                {
                                                                    sectionsCount > 1 ?
                                                                    <Paper className={classes.paper}>
                                                                        {
                                                                            this.renderWidgets(section, schemaIndex, key)
                                                                        }
                                                                    </Paper> :
                                                                    this.renderWidgets(section, schemaIndex, key)
                                                                }

                                                            </Grid>
                                                        );
                                                    })
                                                }
                                            </Grid>
                                        </Paper>
                                    </React.Fragment>
                                );
                            })
                        }
                        <Grid item xs={12} sm={12} className={classes.buttonGrid}>
                            <Paper className={classes.buttonContainer}>
                                <Button
                                    onClick={() => {addOther(); }}
                                    variant="outlined"
                                    startIcon={<AddBoxIcon/>}
                                >
                                    {t("__repeater_add_other_btn__")}
                                </Button>
                            </Paper>
                        </Grid>
                    </Paper>
                    <FormHelperText
                        className={classes.helperText}
                        variant="outlined">{helperText}</FormHelperText>
                </AccordionDetails>
            </Accordion>
        </FormControl>);
    }

}

const styles = createStyles((theme: any) => ({
    fileWidgetContent: {
        width     : "100%",
        textAlign : "left",
        "& form"  : {
            "& div": {
                marginBottom: "0.5em"
            }
        },
        "& button": {
            marginBottom: "0.5em"
        }
    },
    label            : {
        position    : "relative",
        marginBottom: ".5em",
    },
    heading          : {
        fontSize  : theme.typography.pxToRem(15),
        flexBasis : "33.33%",
        flexShrink: 0,
    },
    secondaryHeading : {
        fontSize: theme.typography.pxToRem(12),
        color   : theme.palette.text.secondary,
    },
    accordionDetails : {
        display: "unset"
    },
    root             : {
        flexGrow: 1,
    },
    fullWidth        : {
        marginBottom: "15px",
        width       : "100%"
    },
    paper            : {
        padding  : theme.spacing(2),
        textAlign: "center",
        color    : theme.palette.text.secondary,
    },
    repeaterContainer: {
        padding: theme.spacing(2),
        // textAlign: "center",
        color  : theme.palette.text.secondary,
    },
    buttonContainer  : {
        textAlign: "left",
        "& > *"  : {
            margin: theme.spacing(1),
        }
    },
    buttonGrid       : {
        marginTop: "1em"
    },
    itemInfo         : {
        display   : "flex",
        textAlign : "left",
        "& button": {
            marginBottom: "unset",
            padding     : "0.1em",
            position    : "absolute",
            right       : "1.2em"
        },
        "& label" : {
            marginBottom: "1em",
            marginTop   : "unset"
        }
    },
    helperText       : {
        marginTop: "1.5em"
    }
}));

export default withStyles(styles)(withTranslation("")((RepeaterFieldView)));