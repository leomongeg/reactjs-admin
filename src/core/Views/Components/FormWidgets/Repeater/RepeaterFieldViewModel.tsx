import React, { ReactNode }              from "react";
import { observer }                      from "mobx-react";
import { observable }                    from "mobx";
import { TFunction }                     from "i18next";
import { FormSchema, IFormWidgetSchema } from "../../../../Admin/Base/FormSchema";
import { WidgetWrapperViewModelType }    from "../../../../ViewModel/Base/WidgetWrapperViewModel";
import SimpleReactValidator              from "simple-react-validator";
import RepeaterFieldView                 from "./RepeaterFieldView";
import { BaseWidgetViewModel }           from "../ Base/BaseWidgetViewModel";
import { ModalStore, ModalType }         from "../../../../Store/ModalStore";


/**
 * props
 */
interface RepeaterFieldViewModelProps {
    allowEmpty?: boolean;
    repeaterItemsLabel?: (t: TFunction, index: number) => string;
    buildRepeatableFields: (schema: FormSchema) => any;
    defaultExpanded?: boolean;
    alwaysOpen?: boolean;
    confirmDeleteItem?: boolean;
    maxEmbeds?: number;
}

@observer
class RepeaterFieldViewModel extends BaseWidgetViewModel<RepeaterFieldViewModelProps, any> {

    private static readonly INDEX_SEPARATOR: string = "|$%-%$|";

    @observable
    private formSchemas: FormSchema[] = [];

    @observable
    private commitFormSchemas: FormSchema[] = [];

    private embedSectionsRef: Map<String, WidgetWrapperViewModelType[]> = new Map<String, WidgetWrapperViewModelType[]>();

    private viewRef: any;

    @observable
    private panelExpanded: boolean = false;

    @observable
    private readonly validator: SimpleReactValidator;

    constructor(props: any) {
        super(props);
        this.validator                      = new SimpleReactValidator({
                                                                           element: ((message: string) => message),
                                                                           locale : "es",
                                                                       });
        const {defaultExpanded, alwaysOpen} = this.props.elementProps;
        if (!!alwaysOpen) this.panelExpanded = true;
        else this.panelExpanded = !!defaultExpanded;
    }

    /**
     * React life cycle event.
     */
    componentDidMount() {
        this.buildEmbedFields();
    }

    /**
     * Build the form if the initial value is defined
     * @private
     */
    private buildEmbedFields() {
        const {value, elementProps} = this.props;
        const {allowEmpty}          = elementProps;

        if (value === undefined || (Array.isArray(value) && value.length == 0)) {
            if (!allowEmpty) this.buildFromValues([], true);
            return;
        }
        if (!Array.isArray(value)) throw Error("Value should be array or undefined in Form widget type TYPE_REPEATER");
        this.buildFromValues(value, false);
    }

    /**
     * Build from values
     *
     * @param values
     * @private
     */
    private buildFromValues(values: any[], addNew: boolean = false) {
        const {buildRepeatableFields} = this.props.elementProps;
        values.forEach((subject: any) => {
            const schema   = new FormSchema();
            schema.subject = subject;
            buildRepeatableFields(schema);
            this.formSchemas.push(schema);
        });
        if (addNew) {
            const schema = new FormSchema();
            buildRepeatableFields(schema);
            this.formSchemas.push(schema);
        }
        this.reBuildWidgetKeyIndex();
        this.validator.purgeFields();
    }

    private addOther = () => {
        const {maxEmbeds} = this.props.elementProps;
        if (maxEmbeds && this.formSchemas.length >= maxEmbeds) {
            // @TODO: Implement a message for the user
            return;
        }
        const values     = this.getValue();
        this.formSchemas = [];
        this.buildFromValues(values, true);
    };

    /**
     * Re-Build the widget key index
     *
     * @private
     */
    private reBuildWidgetKeyIndex() {
        this.formSchemas.forEach((schema: FormSchema, index: number) => {
            Array.from(schema.sections.keys()).forEach((sectionKey: string) => {
                const section = schema.sections.get(sectionKey);
                section?.widgets.forEach((widget: IFormWidgetSchema) => {
                    widget.key = `${widget.key}${RepeaterFieldViewModel.INDEX_SEPARATOR}${index}`;
                });
            });
        });
        this.commitFormSchemas = [];
        this.commitFormSchemas = this.formSchemas;
        this.validator.purgeFields();
    }

    /**
     * Trigger the validations
     */
    public triggerEmbedValidationsHasErrors(): boolean {
        if (!this.validator.allValid()) {
            this.validator.showMessages();
            this.panelExpanded = true;
            this.forceUpdate();
            this.viewRef.forceUpdate();
            return true;
        }
        return false;
    }

    /**
     * Get the embed values
     */
    public getValue() {

        return Array.from(this.embedSectionsRef.keys()).map((key: string, index: number) => {
            const widgets = this.embedSectionsRef.get(key);
            const item    = {};
            widgets?.forEach((widget: WidgetWrapperViewModelType) => {
                const key    = widget.objectKey.split(RepeaterFieldViewModel.INDEX_SEPARATOR);
                item[key[0]] = widget.getValue();
            });
            return item;
        });
    }

    /**
     * Insert new widget
     *
     * @param leaf
     * @param widget
     * @private
     */
    private insertWidgetInLeaf = (leaf: number, widget: WidgetWrapperViewModelType) => {
        const key = `leaf-${leaf}`;
        if (!this.embedSectionsRef.has(key)) {
            this.embedSectionsRef.set(key, []);
        }

        this.embedSectionsRef.get(key)?.push(widget);
    };


    /**
     * Remove item from list.
     *
     * @param index
     */
    private deleteElement = (index: number) => {
        const {allowEmpty, confirmDeleteItem} = this.props.elementProps;
        const values                          = this.getValue();
        if (!allowEmpty && values.length === 1) {
            return;
        }

        if (!!confirmDeleteItem) {
            this.getStore(ModalStore).configureModal({
                                                         title: "__repeater_delete_confirm_title__",
                                                         body : "__repeater_delete_confirm_body__",

                                                         type: ModalType.CONFIRM
                                                     })
                .addPrimaryClickHandler(async (modalStore: ModalStore) => {
                    this.performRemoveElement(values, index);
                    modalStore.doHideModal();
                })
                .doShowModal();
            return;
        }

        this.performRemoveElement(values, index);
    };

    /**
     * Execute the process to remove an embed element.
     * @private
     */
    private performRemoveElement(values: any, index: number): void {
        values.splice(index, 1);
        this.formSchemas = [];
        this.embedSectionsRef.delete(`leaf-${index}`);

        const viewRef = new Map<String, WidgetWrapperViewModelType[]>();
        Array.from(this.embedSectionsRef.keys()).map((key: string, i: number) => {
            viewRef.set(`leaf-${i}`, this.embedSectionsRef.get(key) || []);
        });
        this.embedSectionsRef = viewRef;
        this.buildFromValues(values);
    }

    /**
     * Change the expandable status of the accordion.
     *
     * @param panel
     */
    private handleExpandable = (panel: string) => (event: React.ChangeEvent<{}>, isExpanded: boolean) => {
        const {alwaysOpen} = this.props.elementProps;
        if (!!alwaysOpen) return;
        this.panelExpanded = isExpanded;
    };

    /**
     * Render function
     */
    render(): ReactNode {
        const {propertyKey, label, helperText} = this.props;
        const {repeaterItemsLabel}             = this.props.elementProps;

        return (<RepeaterFieldView
            repeaterItemsLabel={repeaterItemsLabel}
            propertyKey={propertyKey}
            formSchemas={this.commitFormSchemas}
            label={label}
            validator={this.validator}
            insertWidgetInLeaf={this.insertWidgetInLeaf}
            addOther={this.addOther}
            helperText={helperText}
            deleteElement={this.deleteElement}
            setRef={(ref: any) => this.viewRef = ref}
            panelExpanded={this.panelExpanded}
            handleExpandable={this.handleExpandable}
        />);
    }
}

export default RepeaterFieldViewModel;