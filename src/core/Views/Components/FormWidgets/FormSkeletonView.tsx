import React, { Component, ReactNode } from "react";
import Skeleton                        from "@material-ui/lab/Skeleton";
import Paper                           from "@material-ui/core/Paper";
import Typography             from "@material-ui/core/Typography";
import { IFormWidgetSchema }  from "../../../Admin/Base/FormSchema";
import WidgetWrapperViewModel from "../../../ViewModel/Base/WidgetWrapperViewModel";
import Grid                   from "@material-ui/core/Grid";
import { withStyles }                  from "@material-ui/core";
import { withTranslation }             from "react-i18next";
import { createStyles }                from "@material-ui/core/styles";

class FormSkeletonView extends Component<any, any> {

    /**
     * Component render
     */
    render(): ReactNode {
        const {classes} = this.props;
        return (
            <>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Typography variant="h2">
                            <Skeleton/>
                        </Typography>
                        <Typography variant="h2">
                            <Skeleton/>
                        </Typography>
                        <Typography variant="h2">
                            <Skeleton/>
                        </Typography>
                        <Typography variant="h2">
                            <Skeleton/>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Typography variant="button">
                            <Skeleton/>
                        </Typography>
                    </Paper>
                </Grid>
            </>
        );
    }
}

const styles = createStyles((theme: any) => ({
    paper: {
        padding  : theme.spacing(2),
        textAlign: "center",
        color    : theme.palette.text.secondary,
    }
}));
export default withStyles(styles)(FormSkeletonView);