import React, { Component, ReactNode }      from "react";
import uuid                                 from "uuid";
import FormControl                          from "@material-ui/core/FormControl";
import Typography                           from "@material-ui/core/Typography";
import FormHelperText                       from "@material-ui/core/FormHelperText";
import InputLabel                           from "@material-ui/core/InputLabel";
import Button                               from "@material-ui/core/Button";
import IconButton                           from "@material-ui/core/IconButton";
import DeleteIcon                           from "@material-ui/icons/Delete";
import Link                                 from "@material-ui/core/Link";
import Card                                 from "@material-ui/core/Card";
import CardContent                          from "@material-ui/core/CardContent";
import CardMedia                            from "@material-ui/core/CardMedia";
import { createStyles, withStyles }         from "@material-ui/core";
import { ClassKeyOfStyles, ClassNameMap }   from "@material-ui/styles/withStyles";
import { WithTranslation, withTranslation } from "react-i18next";
import { observer }                         from "mobx-react";

const FileUploadProgress = require("react-fileupload-progress").default;

/**
 * Widget props
 */
interface FileUploadFieldProps extends WithTranslation {
    formCustomizer?: (form: FormData, value: any) => void;
    authorizationHeader?: { key: string, value: string };
    fileTargetUrl?: string;
    onProgress?: (e: any, request: XMLHttpRequest, progress: any) => void;
    onLoad?: (e: any, request: XMLHttpRequest) => void;
    parseResponse?: (response: any) => any;
    onError?: (e: any, request: XMLHttpRequest) => void;
    onAbort?: (e: any, request: XMLHttpRequest) => void;
    beforeSend?: (request: XMLHttpRequest) => void;
    filePathFromValue?: (value: any) => string;
    deleteHandler?: (value: any) => void;


    label: string;
    helperText: string;
    className: string;
    propertyKey: string;
    value: any;
    onChange: (val: any) => void;
    classes: ClassNameMap<ClassKeyOfStyles<string>>;
    error: boolean;
    elementProps: any;
    setViewRef: any;
}

@observer
class FileUploadField extends Component<FileUploadFieldProps, any> {
    private readonly fileUploadIdentifier: string;

    constructor(props: FileUploadFieldProps) {
        super(props);
        const {propertyKey}       = props;
        this.fileUploadIdentifier = `${propertyKey}-${uuid().toString()}`;
    }

    /**
     * Upload file form render
     */
    private formRender = (onSubmitHandler: any) => {
        const {t, propertyKey} = this.props;
        return (<form id={this.fileUploadIdentifier}
                      className={propertyKey}
                      ref="form"
                      method="post" onSubmit={onSubmitHandler}>
            <div>
                <input type="file" name="file"/>
            </div>
            <Button size={"small"} variant="contained" type={"submit"} color="secondary">
                {t("__upload__")}
            </Button>
        </form>);
    };

    /**
     * Bind the form
     */
    private formGetter = () => {
        return new FormData(document.getElementById(this.fileUploadIdentifier) as HTMLFormElement || undefined);
    };

    private getFilePath = (): string => {
        const {value, filePathFromValue} = this.props;
        if (value === undefined) return "";
        if (typeof value === "string") return value;
        return filePathFromValue === undefined ? "" : filePathFromValue(value);
    };

    private deleteHandler = async () => {
        const {deleteHandler, value, onChange} = this.props;
        if (deleteHandler) {
            await deleteHandler(value);
        }
        onChange(undefined);
    };

    /**
     * Component Render
     */
    render(): ReactNode {
        const {
                  classes, onChange, parseResponse, className, label, value, onAbort, onError, onProgress, error,
                  helperText, propertyKey, fileTargetUrl, formCustomizer, beforeSend, onLoad, authorizationHeader
              } = this.props;
        return (
            <FormControl className={`${className} ${classes.fileWidgetContent}`}>

                <InputLabel className={`MuiInputLabel-shrink ${classes.label}`}
                            htmlFor={`select-${propertyKey}`}>{label}</InputLabel>
                {
                    !value ? <FileUploadProgress key={this.fileUploadIdentifier}
                                                 url={fileTargetUrl || ""}
                                                 method="post"
                                                 onProgress={(e: any, request: any, progress: any) => {
                                                     if (onProgress) onProgress(e, request, progress);
                                                 }}
                                                 onLoad={(e: any, request: any) => {
                                                     const response = JSON.parse(request.response);
                                                     if (onLoad) onLoad(e, request);
                                                     onChange(parseResponse === undefined ?
                                                              response : parseResponse(response)
                                                     );
                                                 }}
                                                 onError={(e: any, request: any) => {
                                                     if (onError) onError(e, request);
                                                 }}
                                                 onAbort={(e: any, request: any) => {
                                                     if (onAbort) onAbort(e, request);
                                                 }}
                                                 formGetter={this.formGetter}
                                                 formRenderer={this.formRender}
                                                 formCustomizer={(form: any) => {
                                                     if (formCustomizer !== undefined) formCustomizer(form, value);
                                                     return form;
                                                 }}
                                                 beforeSend={(request: any) => {
                                                     if (authorizationHeader)
                                                         request.setRequestHeader(
                                                             authorizationHeader.key,
                                                             authorizationHeader.value
                                                         );
                                                     if (beforeSend) beforeSend(request);
                                                     return request;
                                                 }}
                    /> : undefined
                }
                {
                    value ? <Card className={classes.card}>
                        <div className={classes.details}>
                            <CardContent className={classes.content}>
                                <Typography variant="subtitle1" gutterBottom>
                                    {label}
                                </Typography>
                                <Link href={this.getFilePath()} target={"_blank"}>
                                    <Typography variant="subtitle2" color="textSecondary">
                                        {this.getFilePath().substr(this.getFilePath().lastIndexOf("/") + 1)}
                                    </Typography>
                                </Link>
                            </CardContent>
                            <div className={classes.controls}>
                                <IconButton aria-label="delete" onClick={this.deleteHandler}>
                                    <DeleteIcon/>
                                </IconButton>
                            </div>
                        </div>
                        {
                            <CardMedia
                                className={classes.cover}
                                image={
                                    (/\.(gif|jpe?g|tiff|png|webp|bmp)$/i).test(this.getFilePath()) ?
                                    this.getFilePath() : "/src/resources/images/fileicon.png"
                                }
                            />
                        }
                    </Card> : undefined
                }
                <FormHelperText
                    error={!!error}
                    variant="outlined">{helperText}</FormHelperText>
            </FormControl>
        );
    }
}

const styles = createStyles((theme: any) => ({
    fileWidgetContent: {
        textAlign : "left",
        "& form"  : {
            "& div": {
                marginBottom: "0.5em"
            }
        },
        "& button": {
            marginBottom: "0.5em"
        }
    },
    label            : {
        position    : "relative",
        marginBottom: ".5em",
    },
    card             : {
        display: "flex"
    },
    details          : {
        display      : "flex",
        flexDirection: "column",
    },
    content          : {
        flex: "1 0 auto",
    },
    cover            : {
        width         : 151,
        backgroundSize: "contain"
    },
}));

export default withStyles(styles)(withTranslation("")((FileUploadField)));