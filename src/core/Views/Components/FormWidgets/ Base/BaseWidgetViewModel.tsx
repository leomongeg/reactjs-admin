import { ClassKeyOfStyles, ClassNameMap } from "@material-ui/styles/withStyles";
import { BaseViewModel }                  from "../../../../ViewModel/Base/BaseViewModel";

/**
 * Base Widget Props
 */
interface BaseWidgetProps<T> {
    elementProps: T;

    label: string;
    helperText: string;
    className: string;
    propertyKey: string;
    value: any;
    onChange: (val: any) => void;
    classes: ClassNameMap<ClassKeyOfStyles<string>>;
    error: boolean;
    setViewRef?: (widget: any) => void;
}

export abstract class BaseWidgetViewModel<P, S> extends BaseViewModel<BaseWidgetProps<P>, any> {
    protected constructor(props: BaseWidgetProps<P>) {
        super(props);
        const {setViewRef} = this.props;
        if (setViewRef)
            setViewRef(this);
    }
}