import React, { Component, ReactNode }      from "react";
import { createStyles, withStyles }         from "@material-ui/core";
import { WithTranslation, withTranslation } from "react-i18next";
import { ClassKeyOfStyles, ClassNameMap }   from "@material-ui/styles/withStyles";
import FormControl                          from "@material-ui/core/FormControl";
import FormHelperText                       from "@material-ui/core/FormHelperText";
import InputLabel                           from "@material-ui/core/InputLabel";
import {
    IMultiSelectDataSourceStore, MultiSelectDefaultDataSourceOptions,
    MultiSelectItem,
    MultiSelectModel
}                                           from "../../../Store/MultipleSelect/DefaultSelectDataSourceStore";

const Select       = require("react-select/async").default;
const makeAnimated = require("react-select/animated").default;


/**
 * Widget props
 */
interface SelectMultipleFieldProps extends WithTranslation {
    // Widget Props
    closeMenuOnSelect?: boolean;
    isMulti?: boolean;
    widgetStyles?: { [key: string]: (styles: any, options: any) => any }; // https://react-select.com/styles
    dataSourceStore?: IMultiSelectDataSourceStore;
    staticData?: { label: string, value: string }[];
    model?: MultiSelectModel;
    dataSourceOptions?: MultiSelectDefaultDataSourceOptions;

    // Default props
    label: string;
    helperText: string;
    className: string;
    propertyKey: string;
    value: any;
    onChange: (val: any) => void;
    classes: ClassNameMap<ClassKeyOfStyles<string>>;
    error: boolean;
    elementProps: any;
    setViewRef: any;
}

class SelectMultipleField extends Component<SelectMultipleFieldProps, any> {

    private searchTimer: any;

    private loadedValues: any[] = [];

    /**
     * React life cycle events
     */
    componentDidMount() {
        // this.defaultValueTypeChecker();
    }

    /**
     * React life cycle events.
     */
    componentWillUnmount() {
        const {dataSourceStore} = this.props;
        if (dataSourceStore) dataSourceStore.cleanData();
    }

    /**
     * Load the remove data
     * @param inputValue
     * @private
     */
    private loadData = (inputValue: string) => {
        if (inputValue.replace(/\W/g, "").length < 3 &&
            inputValue.replace(/\W/g, "").length !== 0) {
            return;
        }

        // if last timer is active clear it
        if (this.searchTimer) {
            clearTimeout(this.searchTimer);
        }

        const {dataSourceStore, staticData, model, dataSourceOptions} = this.props;

        if (dataSourceStore) {
            return new Promise(resolve => {
                this.searchTimer = setTimeout(async () => {
                    (dataSourceStore.fetchItemsUsingDefaultDataSource !== undefined &&
                        typeof dataSourceStore.fetchItemsUsingDefaultDataSource === "function") ?
                    await dataSourceStore.fetchItemsUsingDefaultDataSource(inputValue, model, dataSourceOptions) :
                    await dataSourceStore.fetchItems(inputValue, model);
                    resolve(dataSourceStore.getItems().map((item: any) => {
                        return {
                            label     : item[model?.label || ""],
                            value     : item[model?.value || ""],
                            fullObject: item
                        };
                    }));
                }, 500);
            });

        }

        if (staticData) {
            return new Promise(resolve => {
                this.searchTimer = setTimeout(() => {
                    resolve(staticData.filter(item => item.label.toLowerCase().includes(inputValue.toLowerCase())));
                }, 500);
            });
        }

        return [];
    };

    /**
     * React Select styles
     * @private
     */
    private defaultStyles = {
        option    : (styles: any, options: any) => {
            const {isFocused} = options;
            return {
                ...styles,
                backgroundColor: isFocused ? "rgba(55, 132, 255, 0.25)" : undefined
            };
        },
        multiValue: (styles: any, options: any) => {
            return {
                ...styles,
                backgroundColor: "rgba(55, 132, 255, 1)",
            };
        }
    };

    /**
     * Load the React Select Styles
     *
     * @URL https://react-select.com/styles
     * @private
     */
    private loadStyles(): any {
        const {widgetStyles} = this.props;

        if (widgetStyles === undefined) {
            return this.defaultStyles;
        }

        return widgetStyles;
    }

    /**
     * Validates if the default value implements the proper structure
     * @Deprecated
     * @private
     */
    private defaultValueTypeChecker() {
        const {value, propertyKey} = this.props;
        console.log(value);
        if (Array.isArray(value)) {
            value.forEach((item) => {
                if (!item.hasOwnProperty("fullObject") || !item.fullObject) {
                    console.error(`The defaults Values for the widget SelectMultipleField requires the property fullObject maybe you forget to added in the initialValue prop as part of the array values that should be of type MultiSelectItem in your admin class for ${propertyKey} formSchema. Or maybe the value for fullObject is undefined. Or if you are using in a configureEditView, implements a mapper with the defaultValues. Ex: mapper: () => subject.categories Remember the mapper values needs conform the MultiSelectItem type.`);
                }
            });

            if (value.length > 0)
                this.mapValuesOnChange(value);
        }
    }

    /**
     * Map the values and pass to the onChange prop.
     *
     * @param value
     * @private
     */
    private mapValuesOnChange(value: MultiSelectItem[] | MultiSelectItem) {
        const {onChange, isMulti, dataSourceStore, model} = this.props;
        if (!value) {
            onChange([]);
            return;
        }

        if (!isMulti && !Array.isArray(value)) {
            onChange([value.fullObject]);
            return;
        }

        const result: any[] = [];
        (value as MultiSelectItem[]).forEach(item => {
            result.push(item.fullObject);
        });
        onChange(result);
    }

    /**
     * value
     */
    private mapValue(value: any) {
        const {isMulti, model} = this.props;

        if (isMulti) {
            const filtered = value.filter((item: any) => item !== undefined).map((item: any) => {
                return {
                    label     : item[model?.label || ""],
                    value     : item[model?.value || ""],
                    fullObject: item
                };
            });
            return filtered;
        }
        const item = (Array.isArray(value) && value.length > 0) ? value[0] : value;
        return {
            label     : item[model?.label || ""],
            value     : item[model?.value || ""],
            fullObject: item
        };
    }

    /**
     * Component render
     */
    render(): ReactNode {
        const {isMulti, closeMenuOnSelect, value, label, classes, className, helperText, propertyKey, error} = this.props;

        return (
            <FormControl className={`${className} ${classes.fileWidgetContent}`}>
                <InputLabel className={`MuiInputLabel-shrink ${classes.label}`}
                            htmlFor={`select-${propertyKey}`}>{label}</InputLabel>
                <Select
                    className={classes.select}
                    placeholder={label}
                    cacheOptions={true}
                    defaultOptions
                    components={makeAnimated()}
                    isMulti={isMulti === undefined ? false : isMulti}
                    closeMenuOnSelect={closeMenuOnSelect === undefined ? false : closeMenuOnSelect}
                    loadOptions={this.loadData}
                    // defaultValue={isMulti ? value : (Array.isArray(value) && value.length > 0 ?
                    // value[0] : value)}
                    value={this.mapValue(value)}
                    onChange={(value: MultiSelectItem[]) => {
                        this.mapValuesOnChange(value);
                    }}
                    styles={this.loadStyles()}
                />
                <FormHelperText
                    error={!!error}
                    variant="outlined">{helperText}</FormHelperText>
            </FormControl>);
    }

}

const styles = createStyles((theme: any) => ({
    fileWidgetContent: {
        textAlign : "left",
        "& form"  : {
            "& div": {
                marginBottom: "0.5em"
            }
        },
        "& button": {
            marginBottom: "0.5em"
        }
    },
    label            : {
        position    : "relative",
        marginBottom: ".5em",
    },
    card             : {
        display: "flex"
    },
    details          : {
        display      : "flex",
        flexDirection: "column",
    },
    content          : {
        flex: "1 0 auto",
    },
    cover            : {
        width: 151,
    },
    select           : {
        "& div[class$='control']"    : {
            backgroundColor: "transparent"
        },
        "& div[class$='menu']"       : {
            "z-index"      : 2,
            backgroundColor: theme.palette.background.paper
        },
        "& div[class$='placeholder']": {
            color: "white"
        },
        "& div[class$='singleValue']": {
            color: "white"
        }
    }
}));

export default withStyles(styles)(withTranslation("")((SelectMultipleField)));