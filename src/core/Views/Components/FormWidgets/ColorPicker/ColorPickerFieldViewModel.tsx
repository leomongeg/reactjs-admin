import { BaseWidgetViewModel } from "../ Base/BaseWidgetViewModel";
import React, { ReactNode } from "react";
import ColorPickerFieldView from "./ColorPickerFieldView";
import { observer } from "mobx-react";
import { observable } from "mobx";

/**
 * Field props
 */
interface ColorPickerFieldViewModelProps {
    propertyColorValue?: string;
    defaultValue?: {r: string, g: string, b: string, a: string} | string;
}

@observer
class ColorPickerFieldViewModel extends BaseWidgetViewModel<ColorPickerFieldViewModelProps, any> {
    @observable
    private showColorPicker: boolean = false;
    @observable
    private color: any = {
        r: "241",
        g: "112",
        b: "19",
        a: "100",
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props: any) {
        super(props);
    }

    /**
     * Component did mount
     */
    componentDidMount(): void {
        const {value, elementProps} = this.props;
        try {
            const defaultValue = elementProps.defaultValue;
            // Hex
            if (defaultValue !== undefined && typeof defaultValue === "string") {
                this.hexToRGB(defaultValue);
            } else if (defaultValue !== undefined) {
                this.color = defaultValue;
            }
            const propertyColorValue = elementProps.propertyColorValue === undefined ? "hex" : elementProps.propertyColorValue;
            if (value !== undefined) {
                switch (propertyColorValue) {
                    case "hex":
                        this.hexToRGB(value);
                        break;
                    case "rgb":
                        this.color = value;
                        break;
                }
            }
        } catch (e) {
            console.log(e);
        }
    }

    /**
     * Handle open picker
     */
    private handleOpenPicker = (state: boolean) => {
        this.showColorPicker = state;
    };

    /**
     * Handle color change
     * @param color
     * @param propertyColorValue
     */
    private handleChange = (color: any, propertyColorValue: string = "hex") => {
        const {onChange} = this.props;
        const possibleOptions = ["hex", "rgb"];
        if (!possibleOptions.includes(propertyColorValue)) {
            throw new Error("Only use: " + possibleOptions.join(","));
        }
        this.color = color.rgb;
        onChange(color[propertyColorValue]);
    };

    /**
     * Convert hex to RGB object
     * @param hex
     */
    private hexToRGB = (hex: string) => {
        if (typeof hex === "string") {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                return r + r + g + g + b + b;
            });

            const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            if (result) {
                this.color = {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16),
                    a: 1
                };
            }
        }
    };

    /**
     * Render function
     */
    render(): ReactNode {
        const { label, helperText, error, propertyKey, className, elementProps } = this.props;
        return (<ColorPickerFieldView
            label={label}
            helperText={helperText}
            error={error}
            className={className}
            propertyKey={propertyKey}
            handleOpenPicker={this.handleOpenPicker}
            showColorPicker={this.showColorPicker}
            color={this.color}
            handleChange={this.handleChange}
            {...elementProps}
        />);
    }
}

export default ColorPickerFieldViewModel;