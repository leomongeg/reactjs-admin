import React, { Component, ReactNode } from "react";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import InputLabel from "@material-ui/core/InputLabel";
import { observer } from "mobx-react";
import { SketchPicker } from "react-color";

/**
 * Color Picker Field View Props definition
 */
export interface ColorPickerFieldViewProps extends WithStyles {
    propertyKey: string;
    label: string;
    helperText: string;
    error: boolean;
    className: string;
    showColorPicker: boolean;
    handleOpenPicker: (state: boolean) => void;
    color: any;
    handleChange: (color: any, propertyColorValue?: string) => void;
    propertyColorValue?: string;
}
@observer
class ColorPickerFieldView extends Component<ColorPickerFieldViewProps, any> {
    /**
     * Render function
     */
    render(): ReactNode {
        const { classes, helperText, label, propertyKey, error, className, showColorPicker, handleOpenPicker, color, handleChange, propertyColorValue } = this.props;
        return (<FormControl className={`${className} ${classes.fileWidgetContent}`}>
            <InputLabel className={`MuiInputLabel-shrink ${classes.label}`}
                        htmlFor={`select-${propertyKey}`}>{label}</InputLabel>
            <div>
                <div className={ classes.swatch } onClick={ () => { handleOpenPicker(!showColorPicker); } }>
                    <div style={{background: `rgba(${ color.r }, ${ color.g }, ${ color.b }, ${ color.a })` }} className={ classes.color } />
                </div>
                {
                    showColorPicker ?
                        <div className={ classes.popover }>
                            <SketchPicker color={ color } onChange={ (color: any) => {
                                handleChange(color, propertyColorValue);
                            } }/>
                        </div> : undefined
                }
            </div>
            <FormHelperText
                className={classes.helperText}
                error={!!error}
                variant="outlined">{helperText}</FormHelperText>
        </FormControl>);
    }
}
const styles = createStyles((theme: any) => ({
    fileWidgetContent: {
        textAlign : "left",
        "& form"  : {
            "& div": {
                marginBottom: "0.5em"
            }
        },
        "& button": {
            marginBottom: "0.5em"
        }
    },
    label            : {
        position    : "relative",
        marginBottom: ".5em",
    },
    swatch: {
        padding: "5px",
        background: "#fff",
        borderRadius: "1px",
        boxShadow: "0 0 0 1px rgba(0,0,0,.1)",
        display: "inline-block",
        cursor: "pointer",
    },
    color: {
        width: "36px",
        height: "14px",
        borderRadius: "2px",
    },
    popover: {
        position: "absolute",
        zIndex: "2",
    },
    cover: {
        position: "fixed",
        top: "0px",
        right: "0px",
        bottom: "0px",
        left: "0px",
    },
}));
export default withStyles(styles)(ColorPickerFieldView);