import React, { Component }                    from "react";
import { createStyles, withStyles, withTheme } from "@material-ui/core";
import { withTranslation }                     from "react-i18next";
import { withRouter }                from "react-router";
import IBaseProps                    from "../../../ViewModel/Props/IBaseProps";
import { ITableSchema, TableSchema } from "../../../Admin/Base/TableSchema";
import DeleteIcon                    from "@material-ui/icons/Delete";
import EditIcon                                from "@material-ui/icons/Edit";
import Button                                  from "@material-ui/core/Button";
import SlowMotionVideoIcon                     from "@material-ui/icons/SlowMotionVideo";
import { SvgIconComponent }                    from "@material-ui/icons";

/**
 * Component props definition
 */
interface TableActionsProps extends IBaseProps {
    schema: ITableSchema;
    data: any;
    actions: any[];
}

class TableActions extends Component<TableActionsProps, any> {
    /**
     * Return the action icon
     *
     * @param action
     */
    private getIcon(action: any) {
        if (action.icon) return <action.icon/>;
        let icon = undefined;
        switch (action.name) {
            case "__edit__":
                icon = <EditIcon/>;
                break;
            case "__delete__":
                icon = <DeleteIcon/>;
                break;
            default:
                icon = <SlowMotionVideoIcon/>;
                break;
        }

        return icon;
    }

    /**
     * Component Render
     */
    render(): React.ReactNode {
        const {t, classes, history, actions, data, schema} = this.props;
        return (<div>
            {
                actions.map((action: any, index: number) => {
                    return (<Button
                        key={`action-btn-${action.name}-${index}`}
                        size="small"
                        variant="outlined"
                        // color="secondary"
                        onClick={() => {
                            action.handler(data, history);
                        }}
                        className={classes.button}
                        startIcon={this.getIcon(action)}
                    >
                        {t(action.name)}
                    </Button>);
                })
            }
        </div>);
    }
}

const styles = createStyles((theme: any) => ({
    button: {
        marginRight  : "2px",
        textTransform: "capitalize"
    }
}));

// @ts-ignore Missing typings in with router hoc
export default withRouter(withStyles(styles)(withTheme(withTranslation("")(TableActions))));