import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { createStyles, WithStyles } from "@material-ui/core/styles";
import SimpleReactValidator from "simple-react-validator";
import { withStyles } from "@material-ui/core";
import { withTranslation } from "react-i18next";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import IBaseProps from "../../../ViewModel/Props/IBaseProps";
import WidgetWrapperViewModel, { WidgetWrapperViewModelType } from "../../../ViewModel/Base/WidgetWrapperViewModel";
import { IFormSection, IFormWidgetSchema } from "../../../Admin/Base/FormSchema";
import { BaseViewModel } from "../../../ViewModel/Base/BaseViewModel";

/**
 * Form parameters
 */
interface TableFilterProps extends IBaseProps, WithStyles {
    validator: SimpleReactValidator;
    viewRef: (component: WidgetWrapperViewModelType) => void;
    sections: Map<string, IFormSection>;
    ajaxWorking: boolean;
    handleFilter: () => void;
    handleClean: () => void;
}

class TableFiltersView extends BaseViewModel<TableFilterProps, any> {

    /**
     * Form structure
     */
    render() {
        const { t, classes, ajaxWorking, sections, validator, viewRef, handleFilter, handleClean } = this.props;
        return (
            Array.from(sections.keys()).map((key: string) => {
                const section = sections.get(key);
                if (section === undefined || section.widgets.length === 0) return undefined;

                return (
                    <Grid key={`grid-${key}`} item {...section.breakpoints}>
                <Paper className={classes.paper}>
                    {/*{index === 0 && <Typography title={"Header"}/>}*/}
                {
                    section.widgets.map((widget: IFormWidgetSchema) => {
                        return (<WidgetWrapperViewModel
                            className={classes.fullWidth}
                        key={`widget-${widget.key}`}
                        validator={validator}
                        objectKey={widget.key}
                        widget={widget}
                        setViewRef={(widget: WidgetWrapperViewModelType) => {viewRef(widget); }}
                        />);
                    })
                }
                <Grid item xs={12} sm={12} className={classes.buttonContainer}>
                <Button variant="contained" color="primary" onClick={() => { handleFilter(); }}>
                    {!ajaxWorking && <span>{t("__filter__")}</span>}
                {ajaxWorking && <CircularProgress size={24}/>}
                </Button>
                <Button
                     onClick={() => { handleClean(); }}
                    variant="outlined">
                    {t("__clean__")}</Button>
                </Grid>
                </Paper>
                </Grid>
                );
            })
        );
    }
}

const styles = createStyles((theme: any) => ({
    root           : {
        flexGrow: 1,
    },
    fullWidth      : {
        marginBottom: "15px",
        width       : "100%"
    },
    paper          : {
        padding  : theme.spacing(2),
        textAlign: "center",
        color    : theme.palette.text.secondary,
    },
    buttonContainer: {
        textAlign: "left",
        "& > *"  : {
            margin: theme.spacing(1),
        }
    }
}));
export default withStyles(styles)(withTranslation("")(TableFiltersView));