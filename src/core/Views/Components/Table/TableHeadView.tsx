import React, { Component } from "react";
import IBaseProps           from "../../../ViewModel/Props/IBaseProps";
import { withStyles }       from "@material-ui/core";
import { withTranslation }       from "react-i18next";
import { createStyles, lighten } from "@material-ui/core/styles";
import TableCell                 from "@material-ui/core/TableCell";
import TableHead                 from "@material-ui/core/TableHead";
import TableRow                  from "@material-ui/core/TableRow";
import TableSortLabel            from "@material-ui/core/TableSortLabel";
import Checkbox             from "@material-ui/core/Checkbox";
import { ITableSchema }     from "../../../Admin/Base/TableSchema";
import { withRouter }       from "react-router";

/**
 * Component Props definition
 */
interface TableHeadViewProps extends IBaseProps {
    tableSchema: ITableSchema[];
}

class TableHeadView extends Component<TableHeadViewProps, any> {

    /**
     * Component Render
     */
    render(): React.ReactNode {
        const {t, classes, tableSchema} = this.props;
        const numSelected               = 0;
        const rowCount                  = 0;
        const orderBy                   = "0";
        const order                     = "desc";
        return (<TableHead>
            <TableRow>
                { /* @TODO implement checkbox functionality */ }
                {/*<TableCell padding="checkbox">
                    <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={() => {}}
                        inputProps={{"aria-label": "select all desserts"}}
                    />
                </TableCell>*/}
                {tableSchema.map((item: ITableSchema) => (<TableCell
                    key={item.key}
                    align={item.options?.numeric ? "right" : "left"}
                    padding={item.options?.disablePadding ? "none" : "default"}
                    sortDirection={orderBy === item.key ? order : false}
                >
                    <TableSortLabel
                        active={orderBy === item.key}
                        direction={orderBy === item.key ? order : "asc"}
                        onClick={() => {}}
                    >
                        {t(item.label)}
                        {orderBy === item.key ? (
                            <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
                        ) : undefined}
                    </TableSortLabel>
                </TableCell>))}
            </TableRow>
        </TableHead>);
    }
}

const styles = createStyles((theme: any) => ({}));

export default withStyles(styles)(withTranslation("")(TableHeadView));