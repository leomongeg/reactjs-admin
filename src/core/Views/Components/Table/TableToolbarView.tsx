import React, { Component }      from "react";
import { createStyles, lighten } from "@material-ui/core/styles";
import { withStyles }            from "@material-ui/core";
import { withTranslation }       from "react-i18next";
import clsx                      from "clsx";
import Toolbar                   from "@material-ui/core/Toolbar";
import Typography                from "@material-ui/core/Typography";
import IconButton                from "@material-ui/core/IconButton";
import Tooltip                   from "@material-ui/core/Tooltip";
import DeleteIcon                from "@material-ui/icons/Delete";
import FilterListIcon   from "@material-ui/icons/FilterList";
import IBaseProps       from "../../../ViewModel/Props/IBaseProps";
import CircularProgress from "@material-ui/core/CircularProgress";
import AddBoxIcon                from "@material-ui/icons/AddBox";
import { withRouter }            from "react-router";
import { FilterSchema } from "../../../Admin/Base/FilterSchema";

/**
 * Component props definitions
 */
interface TableToolbarViewProps extends IBaseProps {
    title: string;
    isLoading: boolean;
    addNew: string | false;
    toggleFilter: () => void;
    hasFilters: boolean;
}

class TableToolbarView extends Component<TableToolbarViewProps, any> {


    /**
     * Component Render
     */
    render(): React.ReactNode {
        const {t, classes, title, isLoading, addNew, history, hasFilters, toggleFilter} = this.props;
        const numSelected                                     = 0;
        return (
            <Toolbar
                className={clsx(classes.root, {
                    [classes.highlight]: numSelected > 0,
                })}
            >
                {numSelected > 0 ? (
                    <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
                        {numSelected} selected
                    </Typography>
                ) : (
                     <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                         {t(title)}
                     </Typography>
                 )}
                {numSelected > 0 ? (
                    <Tooltip title="Delete">
                        <IconButton aria-label="delete">
                            <DeleteIcon/>
                        </IconButton>
                    </Tooltip>
                ) : (
                     <>
                         { /* @TODO: implement filter actions */ }
                         { hasFilters ? <Tooltip title={t("filter_list_tooltip").toString()}>
                             <IconButton aria-label={t("filter_list_tooltip").toLowerCase()} onClick={() => {
                                 toggleFilter();
                             }}>
                                 {isLoading ?
                                     <CircularProgress
                                         className={classes.loader}
                                         size={20}
                                     /> : <FilterListIcon/>}
                             </IconButton>
                         </Tooltip> : ""
                         }

                         {
                             addNew ? <Tooltip title={t("add_new_item").toString()}>
                                 <IconButton
                                     aria-label={t("add_new_item").toLowerCase()}
                                     onClick={() => {
                                         history.push(addNew);
                                     }}
                                 >
                                     <AddBoxIcon/>
                                 </IconButton>
                             </Tooltip> : undefined
                         }
                     </>
                 )}
            </Toolbar>
        );
    }
}

const styles = createStyles((theme: any) => ({
    root     : {
        paddingLeft : theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    highlight:
        theme.palette.type === "light"
        ? {
                color          : theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
        : {
                color          : theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    title    : {
        flex: "1 1 100%",
    },
    loader   : {}
}));

// @ts-ignore Ignore issue with typings in withRouter
export default withRouter(withStyles(styles)(withTranslation("AdminTable")(TableToolbarView)));