import * as React                   from "react";
import { Route as RouteRouter }     from "react-router";
import { Container }                from "typedi";
import { MiddlewareHandlerService } from "../../../Service/MiddlewareHandlerService";
import { AuthMiddleware }           from "../../../Middleware/AuthMiddleware";
import { ConfigurationStore }       from "../../../Store/ConfigurationStore";
import BaseMiddleware               from "../../../Middleware/Base/BaseMiddleware";
import { ApplicationStore }         from "../../../Store/ApplicationStore";

export default class Route extends React.Component<any> {

    constructor(props: any) {
        super(props);
        this.resetFooterHeader();
    }

    /**
     * Configuration store instance
     * @return {ConfigurationStore}
     */
    get configurationStore(): ConfigurationStore {
        return Container.get(ApplicationStore).getStore(ConfigurationStore);
    }

    /**
     * Resets the footer and header
     */
    public resetFooterHeader() {
        this.configurationStore
            .getConfiguration()
            .setShowFooter(true)
            .setShowHeader(true)
            .setShowMenuLogo(true);
    }

    /**
     * React component life cycle event
     */
    public componentDidMount(): void {
        const options = this.props.options || {},
              {name}  = this.props;

        this.configurationStore.setCurrentRolePage(options.currentRole || "")
            .setCurrentRouteName(name);
    }

    /**
     * Component render
     */
    render() {
        let {options}                                                                               = this.props;
        const {auth, component: COMPONENT, middleware, layout: LAYOUT, adminClassInstance, ...rest} = this.props;
        const middlewareService                                                                     = Container.get(MiddlewareHandlerService),
              authMiddleware                                                                        = middlewareService.getMiddleware(AuthMiddleware.getAlias());
        const adminProp                                                                             = {adminClassInstance: adminClassInstance};
        return (
            <RouteRouter {...rest} render={(props) => {
                if (auth) {
                    // validate if logged
                    if (!authMiddleware.validate())
                        return authMiddleware.reject(props);
                }

                if (middleware) {
                    options = options || {};
                    for (const item of middleware) {
                        if ((item as BaseMiddleware).setMatch)
                            (item as BaseMiddleware).setMatch(props.match);

                        const valid = (item as BaseMiddleware).validate(options);

                        if (!valid) {
                            return (item as BaseMiddleware).reject(props);
                        }
                    }
                }

                return LAYOUT ? <LAYOUT>
                                  <COMPONENT {...props} {...adminProp}/>
                              </LAYOUT>
                              : <COMPONENT {...props} {...adminProp}/>;
            }}/>
        );
    }
}