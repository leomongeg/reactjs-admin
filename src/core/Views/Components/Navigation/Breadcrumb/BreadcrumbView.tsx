import React, { Component } from "react";
import Breadcrumbs          from "@material-ui/core/Breadcrumbs";
import Link                 from "@material-ui/core/Link";
import { UserStore }        from "../../../../Store/UserStore";
import { Container }        from "typedi";
import { computed }         from "mobx";
import User                 from "../../../../../Model/User/User";

class BreadcrumbView extends Component<{}, {}> {

    private userStore: UserStore;

    constructor(props: any) {
        super(props);
        this.userStore = Container.get(UserStore);
    }

    /**
     * Return the current user;
     */
    @computed
    get user(): User | undefined {
        return this.userStore.getUser();
    }

    /**
     * Breadcrumb handle click
     */
    private handleClick() {
        console.log("[BreadcrumbView] Handle Click");
    }

    /**
     * Component Render
     */
    render(): React.ReactNode {
        return (
            <>
                {
                    this.user === undefined ? undefined :
                    <Breadcrumbs aria-label="breadcrumb">
                        <Link color="inherit" onClick={this.handleClick}>
                            Material-UI
                        </Link>
                        <Link color="inherit" href={void (0)} onClick={this.handleClick}>
                            Core
                        </Link>
                        <Link
                            color="textPrimary"
                            href={void (0)}
                            onClick={this.handleClick}
                            aria-current="page"
                        >
                            Breadcrumb
                        </Link>
                    </Breadcrumbs>
                }
            </>
        );
    }
}

export default BreadcrumbView;