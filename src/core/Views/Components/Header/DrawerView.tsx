import React, { Component, ReactNode } from "react";
import { createStyles, withStyles }    from "@material-ui/core";
import { withTranslation }             from "react-i18next";
import { withRouter }                  from "react-router";
import IBaseProps                      from "../../../ViewModel/Props/IBaseProps";
import ArrowDropDownIcon               from "@material-ui/icons/ArrowDropDown";
import ArrowRightIcon                  from "@material-ui/icons/ArrowRight";
import Divider                         from "@material-ui/core/Divider";
import TreeView                        from "@material-ui/lab/TreeView";
import Label                           from "@material-ui/icons/Label";
import MenuItem                        from "./MenuItem";
import { IMenuGroups }                 from "../../../Store/AdminMenuStore";
import Avatar                          from "@material-ui/core/Avatar";
import Typography                      from "@material-ui/core/Typography";
import ExitToAppIcon                   from "@material-ui/icons/ExitToApp";
import IconButton                      from "@material-ui/core/IconButton";
import ThemeConfig                     from "../../../../config/ThemeConfig";
import User                            from "../../../../Model/User/User";


/**
 * Component Props Definition
 */
interface DrawerViewProps extends IBaseProps {
    groups: IMenuGroups;
    user: User;
    handleLogout: () => void;
}

class DrawerView extends Component<DrawerViewProps, any> {
    /**
     * Build menu section as three mode
     */
    private buildThree(items: ReactNode[], groupItems: any, key: string) {
        const {t, classes, history} = this.props;
        items.push(<MenuItem
            className={classes.menuItem}
            key={`item-root-${key}`} nodeId={`item-root-${key}`}
            labelText={t(key)}
            labelIcon={Label}
        >
            {
                groupItems.map((item: any, index: number) => {
                    return (<MenuItem
                        onClick={() => {
                            item.handleClick(history, item.route);
                        }}
                        className={classes.menuItem}
                        key={`${key}-${index}`} nodeId={`${key}-${index}`}
                        labelText={t(item.route.displayText)}
                        labelIcon={item.route.icon ? item.route.icon : Label}/>);
                })
            }
        </MenuItem>);
    }

    /**
     * Build menu section as plain mode
     */
    private buildPlain(items: ReactNode[], groupItems: any, key: string) {
        const {t, classes, history} = this.props;
        for (const index in groupItems) {
            const item = groupItems[index];
            items.push(<MenuItem
                onClick={() => {
                    item.handleClick(history, item.route);
                }}
                className={classes.menuItem}
                key={`${key}-${index}`} nodeId={`${key}-${index}`}
                labelText={t(item.route.displayText)}
                labelIcon={item.route.icon ? item.route.icon : Label}/>);
        }
    }

    /**
     * Test
     */
    private groupsToRender(): ReactNode[] {
        const items    = [];
        const {groups} = this.props;
        const sections = Object.keys(groups);

        for (const key of sections) {
            const groupItems = groups[key];
            const three      = ThemeConfig.menuOptions.treeMode;
            items.push(<Divider key={`divider-${key}`}/>);
            if (three) {
                this.buildThree(items, groupItems, key);
            } else {
                this.buildPlain(items, groupItems, key);
            }
        }

        return items;
    }

    /**
     * React Component Render function
     */
    render(): React.ReactNode {
        const {classes, handleLogout, user} = this.props;

        // @TODO: Handle roles

        return (
            <div>
                <div className={classes.toolbar}>
                    <IconButton size="small" onClick={handleLogout}><ExitToAppIcon fontSize="small"/></IconButton>
                    <div>

                        <Avatar className={classes.avatar}/>
                    </div>
                    <div className={classes.userInfoContainer}>
                        <Typography>{user.name}</Typography>
                    </div>
                </div>
                <TreeView
                    className={classes.threeViewRoot}
                    defaultCollapseIcon={<ArrowDropDownIcon/>}
                    defaultExpandIcon={<ArrowRightIcon/>}
                    defaultEndIcon={<div style={{width: 24}}/>}
                >
                    {
                        this.groupsToRender().map((item: ReactNode) => {
                            return item;
                        })
                    }
                </TreeView>
                {/*<Divider/>*/}
                {/*<List>*/}
                {/*    {["All mail", "Trash", "Spam"].map((text, index) => (*/}
                {/*        <ListItem button key={text}>*/}
                {/*            <ListItemIcon>{index % 2 === 0 ? <InboxIcon/> : <MailIcon/>}</ListItemIcon>*/}
                {/*            <ListItemText primary={text}/>*/}
                {/*        </ListItem>*/}
                {/*    ))}*/}
                {/*</List>*/}
            </div>
        );
    }

}

const styles = createStyles((theme: any) => ({
    toolbar          : {
        ...theme.mixins.toolbar,
        textAlign: "right"
    },
    avatar           : {
        margin: "0 auto",
        // marginTop: "0.5em"
    },
    userInfoContainer: {
        textAlign   : "center",
        marginTop   : "0.5em",
        marginBottom: "0.5em"
    },
    threeViewRoot    : {
        flexGrow: 1
    },
    menuItem         : {
        padding: "8px 0px"
    }
}));

// @ts-ignore Missing typings in with router hoc
export default withRouter(withStyles(styles)(withTranslation("")(DrawerView)));