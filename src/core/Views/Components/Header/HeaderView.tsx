import React, { Component, ReactNode }         from "react";
import { withTranslation } from "react-i18next";
import IBaseProps          from "../../../ViewModel/Props/IBaseProps";
import AppBar              from "@material-ui/core/AppBar";
import IconButton                              from "@material-ui/core/IconButton";
import Toolbar                                 from "@material-ui/core/Toolbar";
import MenuIcon                                from "@material-ui/icons/Menu";
import Typography                              from "@material-ui/core/Typography";
import { createStyles, withStyles, withTheme } from "@material-ui/core";
import Hidden                                  from "@material-ui/core/Hidden";
import Drawer              from "@material-ui/core/Drawer";
import DrawerViewModel     from "../../../ViewModel/Header/DrawerViewModel";
import { observer }        from "mobx-react";

/**
 * Props interface
 */
interface HeaderViewProps extends IBaseProps {
    title: string;
    mobileOpen: boolean;
    handleDrawerToggle: () => void;
}

@observer
class HeaderView extends Component<HeaderViewProps, any> {
    /**
     * render
     */
    public render(): ReactNode {
        const {t, classes, theme, title, mobileOpen, handleDrawerToggle} = this.props;

        return (
            <>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            edge="start"
                            onClick={handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="h6" noWrap>
                            {t(title)}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <nav className={classes.drawer} aria-label="mailbox folders">
                    <Hidden smUp implementation="css">
                        <Drawer
                            variant="temporary"
                            anchor={theme.direction === "rtl" ? "right" : "left"}
                            open={mobileOpen}
                            onClose={handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            ModalProps={{
                                keepMounted: true, // Better open performance on mobile.
                            }}
                        >
                            <DrawerViewModel/>
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open
                        >
                            <DrawerViewModel/>
                        </Drawer>
                    </Hidden>
                </nav>
            </>
        );
    }
}

const styles = createStyles((theme: any) => ({
    appBar     : {
        [theme.breakpoints.up("sm")]: {
            width     : `calc(100% - ${240}px)`,
            marginLeft: 240,
        },
    },
    menuButton : {
        marginRight                 : theme.spacing(2),
        [theme.breakpoints.up("sm")]: {
            display: "none",
        },
    },
    toolbar    : theme.mixins.toolbar,
    drawer     : {
        [theme.breakpoints.up("sm")]: {
            width     : 240,
            flexShrink: 0,
        },
    },
    drawerPaper: {
        width: 240,
    },
    content    : {
        flexGrow: 1,
        padding : theme.spacing(3),
    },
}));

export default withStyles(styles)(withTheme(withTranslation("common")(HeaderView)));
