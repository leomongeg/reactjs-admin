import React, { Component, CSSProperties } from "react";
import { createStyles, withStyles }        from "@material-ui/core";
import Typography                          from "@material-ui/core/Typography";
import TreeItem, { TreeItemProps }         from "@material-ui/lab/TreeItem";
import { SvgIconProps }                    from "@material-ui/core/SvgIcon";
import { ClassKeyOfStyles, ClassNameMap }  from "@material-ui/styles/withStyles";

declare module "csstype" {
    /**
     * Define style properties
     */
    interface Properties {
        "--tree-view-color"?: string;
        "--tree-view-bg-color"?: string;
    }
}

/**
 * Component Props Definition
 */
interface MenuItemProps extends TreeItemProps {
    bgColor?: string;
    color?: string;
    labelIcon: React.ElementType<SvgIconProps>;
    labelInfo?: string;
    labelText: string;
    classes: ClassNameMap<ClassKeyOfStyles<string>>;
}

class MenuItem extends Component<MenuItemProps, any> {

    /**
     * Component Render function
     */
    render(): React.ReactNode {
        const {classes, labelText, labelIcon: LabelIcon, labelInfo, color, bgColor, nodeId, ...other} = this.props;
        return (<TreeItem
            nodeId={nodeId}
            label={
                <div className={classes.labelRoot}>
                    <LabelIcon color="inherit" className={classes.labelIcon}/>
                    <Typography variant="body2" className={classes.labelText}>
                        {labelText}
                    </Typography>
                    <Typography variant="caption" color="inherit">
                        {labelInfo}
                    </Typography>
                </div>
            }
            style={{
                "--tree-view-color"   : color || "#575757",
                "--tree-view-bg-color": bgColor || "#bdbdbd",
            } as CSSProperties}
            classes={{
                root    : classes.root,
                content : classes.content,
                expanded: classes.expanded,
                selected: classes.selected,
                group   : classes.group,
                label   : classes.label,
            }}
            {...other}
        />);
    }

}

const styles = createStyles((theme: any) => ({
    root     : {
        color                                                                               : theme.palette.text.secondary,
        "&:hover > $content"                                                                : {
            backgroundColor: theme.palette.action.hover,
        },
        "&:focus > $content, &$selected > $content"                                         : {
            backgroundColor: `var(--tree-view-bg-color, ${theme.palette.grey[400]})`,
            color          : "var(--tree-view-color)",
        },
        "&:focus > $content $label, &:hover > $content $label, &$selected > $content $label": {
            backgroundColor: "transparent",
        },
    },
    content  : {
        color                  : theme.palette.text.secondary,
        borderTopRightRadius   : theme.spacing(2),
        borderBottomRightRadius: theme.spacing(2),
        paddingRight           : theme.spacing(1),
        fontWeight             : theme.typography.fontWeightMedium,
        "$expanded > &"        : {
            fontWeight: theme.typography.fontWeightRegular,
        },
    },
    group    : {
        marginLeft  : 0,
        "& $content": {
            paddingLeft: theme.spacing(2),
        },
    },
    expanded : {},
    selected : {},
    label    : {
        fontWeight: "inherit",
        color     : "inherit",
    },
    labelRoot: {
        display   : "flex",
        alignItems: "center",
        padding   : theme.spacing(0.5, 0),
    },
    labelIcon: {
        marginRight: theme.spacing(1),
    },
    labelText: {
        fontWeight: "inherit",
        flexGrow  : 1,
    }
}));

export default withStyles(styles)(MenuItem);