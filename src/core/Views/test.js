function changeStuff(a, b, c)
{
    a = a * 10;
    b.item = "changed";
    c = {item: "changed"};
}

var num = 10;
var obj1 = {item: "unchanged"};
var obj2 = {item: "unchanged"};

changeStuff(num, obj1, obj2);

console.log(num); // 100
console.log(obj1.item); // changed
console.log(obj2.item); // unchanged







































var output = (function(x) {
    delete x;
    return x;
})(0);

console.log(output);