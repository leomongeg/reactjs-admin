import React, { Component } from "react";
import IBaseProps from "../../ViewModel/Props/IBaseProps";
import { withTranslation } from "react-i18next";
import { createStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core";
import TableToolbarView from "../Components/Table/TableToolbarView";
import TableHeadView from "../Components/Table/TableHeadView";
import { ITableSchema } from "../../Admin/Base/TableSchema";
import { observer } from "mobx-react";
import TableActions from "../Components/Table/TableActions";
import CircularProgress from "@material-ui/core/CircularProgress";
import { observable } from "mobx";
import { FilterSchema } from "../../Admin/Base/FilterSchema";
import FiltersViewModel from "../../ViewModel/FiltersViewModel";

/**
 * Component Props definiton
 */
interface ListViewProps extends IBaseProps {
    tableSchema: ITableSchema[];
    filterSchema: FilterSchema;
    rowsPerPageOptions: number[];
    rowsPerPage: number;
    currentPage: number;
    totalItems: number;
    data: any[];
    handleRowsPerPageChange: (rowsPerPage: number) => void;
    handlePageChange: (page: number) => void;
    actions: any[];
    isLoading: boolean;
    addNew: string | false;
    handleFilter: (filter: object) => void;
}

@observer
class ListView extends Component<ListViewProps, any> {
    @observable
    showFilters: boolean = false;

    constructor(props: Readonly<ListViewProps>) {
        super(props);
        this.showFilters = props.filterSchema.subject !== undefined;
    }

    /**
     * Handle function for toggle filter
     */
    protected handleToggleFilter = () => {
        this.showFilters = !this.showFilters;
    }

    /**
     * React Component Render
     */
    render(): React.ReactNode {
        const {
                  t, classes, tableSchema, filterSchema, data, rowsPerPageOptions, rowsPerPage, isLoading, addNew,
                  currentPage, handlePageChange, handleRowsPerPageChange, totalItems, actions, history, handleFilter
              } = this.props;

        const toggleFilter = this.handleToggleFilter;
        return (
            <Paper className={classes.paper}>
                <TableToolbarView title={t("AdminTable:table_list_header")} isLoading={isLoading} addNew={addNew}
                                  toggleFilter={toggleFilter}
                                  hasFilters={filterSchema ? filterSchema.hasFilters() : false}/>
                {this.showFilters ?
                    <FiltersViewModel filterSchema={filterSchema} ajaxWorking={false} handleFilter={(object: any) => { handleFilter(object); }}></FiltersViewModel>
                    : ""
                }
                <TableContainer>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        // size={dense ? "small" : "medium"}
                        aria-label="enhanced table"
                    >
                        <TableHeadView tableSchema={tableSchema}/>
                        <TableBody>
                            {
                                data.map((item, rowIndex: number) => {
                                    return (<TableRow
                                        hover
                                        // onClick={(event) => handleClick(event, row.name)}
                                        role="checkbox"
                                        // aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={rowIndex}
                                        // selected={isItemSelected}
                                    >
                                        {/* @TODO implement checkbox functionality */}
                                        {/*<TableCell key={`row-${rowIndex}-check}`} padding="checkbox">
                                            <Checkbox
                                                checked={false}
                                                inputProps={{"aria-labelledby": "test"}}
                                            />
                                        </TableCell>*/}
                                        {
                                            tableSchema.map((schema: ITableSchema, index: number) => {
                                                let Value: any = String(item[schema.key]);
                                                if (schema.render !== undefined)
                                                    Value = schema.render(schema.key, item, t);
                                                return (
                                                    index === 0 ?
                                                    <TableCell key={`row-${rowIndex}-${schema.key}`} component="th"
                                                               id={schema.key} scope="row" padding="default"> {/* @TODO "none" when checkbox enabled */}
                                                        {typeof Value === "string" ? Value :
                                                         <Value.Component {...Value.props}/>}
                                                    </TableCell>
                                                                :
                                                    <TableCell key={`row-${rowIndex}-${schema.key}`}
                                                               align={item.options?.numeric ? "right" : "left"}>
                                                        {schema.key === "__actions__" ?
                                                         <TableActions actions={actions} data={item}
                                                                       schema={schema}/> : typeof Value === "string" ? Value :
                                                                                           <Value.Component {...Value.props}/>}
                                                    </TableCell>
                                                );
                                            })
                                        }
                                    </TableRow>);
                                })
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
                {
                    isLoading ? <div className={classes.loader}><CircularProgress size={24} thickness={5}/></div>
                              : <TablePagination
                        rowsPerPageOptions={rowsPerPageOptions}
                        component="div"
                        count={totalItems}
                        rowsPerPage={rowsPerPage}
                        page={currentPage}
                        onChangePage={(evt: unknown, newPage: number) => { handlePageChange(newPage); }}
                        onChangeRowsPerPage={(event: React.ChangeEvent<HTMLInputElement>) => {
                            handleRowsPerPageChange(parseInt(event.target.value, 10));
                        }}
                    />
                }
            </Paper>
        );
    }
}

const styles = createStyles((theme: any) => ({
    root          : {
        width: "100%",
    },
    paper         : {
        width       : "100%",
        marginBottom: theme.spacing(2),
    },
    table         : {
        minWidth: 750,
    },
    visuallyHidden: {
        border  : 0,
        clip    : "rect(0 0 0 0)",
        height  : 1,
        margin  : -1,
        overflow: "hidden",
        padding : 0,
        position: "absolute",
        top     : 20,
        width   : 1,
    },
    loader        : {
        textAlign  : "right",
        marginRight: "2.5em",
        paddingTop : "0.8em",
        minHeight  : "52px"
    }
}));

export default withStyles(styles)(withTranslation("")(ListView));