import React, { Component, ReactNode }         from "react";
import IBaseProps                              from "../../ViewModel/Props/IBaseProps";
import { Container, createStyles, withStyles } from "@material-ui/core";
import { withTranslation }                     from "react-i18next";
import Typography                              from "@material-ui/core/Typography";
import Avatar                                  from "@material-ui/core/Avatar";
import LockOutlinedIcon                        from "@material-ui/icons/LockOutlined";
import { ErrorPageTypes }                      from "../../ViewModel/SystemErrorsViewModel";
import SentimentDissatisfiedIcon               from "@material-ui/icons/SentimentDissatisfied";
import VpnKeyIcon                              from "@material-ui/icons/VpnKey";
import ErrorIcon                               from "@material-ui/icons/Error";
import Grid                                    from "@material-ui/core/Grid";
import Link                                    from "@material-ui/core/Link";
import { url_for }                             from "../../Utils/url_generator";

/**
 * View Component props
 */
interface SystemErrorsViewProps extends IBaseProps {
    errorType: ErrorPageTypes;

}

class SystemErrorsView extends Component<SystemErrorsViewProps, any> {

    /**
     * Define the base system icons.
     * @return any
     */
    private iconError() {
        const {classes, errorType} = this.props;
        let icon;

        switch (errorType) {
            case ErrorPageTypes.UNAUTHORIZED:
                icon = <VpnKeyIcon className={classes.icon}/>;
                break;
            case ErrorPageTypes.FORBIDDEN:
                icon = <LockOutlinedIcon className={classes.icon}/>;
                break;
            case ErrorPageTypes.INTERNAL_ERROR:
                icon = <ErrorIcon className={classes.icon}/>;
                break;
            default:
                icon = <SentimentDissatisfiedIcon className={classes.icon}/>;
        }

        return icon;
    }

    /**
     * React Component render.
     */
    render(): ReactNode {
        const {t, classes, errorType} = this.props;

        return (
            <>
                <Container /*component="main"*/ maxWidth="md">
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            {
                                this.iconError()
                            }
                        </Avatar>
                        <Typography /*component="h1"*/ variant="h4" className={classes.errorTitle}>
                            {t(`${errorType}.title`)}
                        </Typography>
                        <Typography /*component="h1"*/ variant="body1" className={classes.errorBody}>
                            {t(`${errorType}.body`)}
                        </Typography>

                        <Grid container>
                            <Grid item>
                                <Link href={url_for("home-page")}
                                      variant="body2">
                                    {t("system:go-home")}
                                </Link>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
            </>
        );
    }

}

const styles = createStyles((theme: any) => ({
    avatar    : {
        backgroundColor: theme.palette.error.main,
        margin         : theme.spacing(2),
        width          : "4em",
        height         : "4em",
    },
    icon      : {
        width : "2em",
        height: "2em",
    },
    errorBody : {
        "text-align"    : "center",
        "font-size"     : "2em",
        "margin-top"    : "2em",
        "padding-bottom": "4em"
    },
    errorTitle: {
        "text-align": "center",
    },
    paper     : {
        alignItems   : "center",
        display      : "flex",
        flexDirection: "column",
        marginTop    : theme.spacing(8)
    }
}));

export default withStyles(styles)(withTranslation("system_errors")(SystemErrorsView));