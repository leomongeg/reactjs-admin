import React, { Component, ReactNode }                        from "react";
import { createStyles, WithStyles }                           from "@material-ui/core/styles";
import { withStyles }                                         from "@material-ui/core";
import { withTranslation }                                    from "react-i18next";
import IBaseProps                                             from "../../ViewModel/Props/IBaseProps";
import Paper                                                  from "@material-ui/core/Paper";
import Typography                                             from "@material-ui/core/CardHeader";
import Grid                                                   from "@material-ui/core/Grid";
import Button                                                 from "@material-ui/core/Button";
import { observer }                                           from "mobx-react";
import WidgetWrapperViewModel, { WidgetWrapperViewModelType } from "../../ViewModel/Base/WidgetWrapperViewModel";
import SimpleReactValidator                                   from "simple-react-validator";
import { IFormSection, IFormWidgetSchema }                    from "../../Admin/Base/FormSchema";
import CircularProgress                                       from "@material-ui/core/CircularProgress";
import { Alert, AlertTitle }                                  from "@material-ui/lab";
import Snackbar                                               from "@material-ui/core/Snackbar";
import { CRUDType }                                           from "../../Store/CreateStore";
import FormSkeletonView                                       from "../Components/FormWidgets/FormSkeletonView";

/**
 * View Component Props
 */
interface CreateViewProps extends IBaseProps, WithStyles {
    ajaxWorking: boolean;
    save: () => void;
    setViewRef: (widget: WidgetWrapperViewModelType) => void;
    validator: SimpleReactValidator;
    sections: Map<String, IFormSection>;
    cancelHandler: (evt: any) => void;
    errors: string;
    snackbarOpen: boolean;
    handleCloseSnackbar: () => void;
    crudType: CRUDType;
    subjectTitle?: string;
    isFetchingData: boolean;
}

@observer
class CreateView extends Component<CreateViewProps, any> {

    /**
     * Component render
     */
    render(): ReactNode {
        const {
                  t, ajaxWorking, classes, save, setViewRef, validator, crudType, isFetchingData,
                  sections, cancelHandler, errors, snackbarOpen, handleCloseSnackbar, subjectTitle
              } = this.props;
        return (
            <div className={classes.root}>
                {/*<form className={classes.form} noValidate>*/}
                    <Grid container spacing={2}>
                        <Typography
                            title={crudType === CRUDType.NEW ? t("AdminCRUD:__add_new__") : t("AdminCRUD:__edit__", {title: subjectTitle || ""})}/>
                    </Grid>
                    {
                        errors ? <Alert severity="error">
                            <AlertTitle>Error</AlertTitle>
                            {errors}
                        </Alert> : undefined
                    }
                    <Grid container spacing={2}>
                        {
                            isFetchingData ? <FormSkeletonView/> : undefined
                        }

                        {
                            Array.from(sections.keys()).map((key: string, index: number) => {
                                const section = sections.get(key);
                                if (section === undefined || section.widgets.length === 0) return undefined;

                                return (
                                    <Grid key={`grid-${key}`} item {...section.breakpoints}>
                                        <Paper className={classes.paper}>
                                            {/*{index === 0 && <Typography title={"Header"}/>}*/}
                                            {
                                                section.widgets.map((widget: IFormWidgetSchema) => {
                                                    return (<WidgetWrapperViewModel
                                                        className={classes.fullWidth}
                                                        key={`widget-${widget.key}`}
                                                        validator={validator}
                                                        objectKey={widget.key}
                                                        widget={widget}
                                                        setViewRef={setViewRef}
                                                    />);
                                                })
                                            }
                                        </Paper>
                                    </Grid>
                                );
                            })
                        }
                        {
                            isFetchingData ? undefined :
                            <Grid item xs={12} sm={12}>
                                <Paper className={classes.buttonContainer}>
                                    <Button variant="contained" color="primary" disabled={ajaxWorking} onClick={save}>
                                        {!ajaxWorking && <span>{t("__save__")}</span>}
                                        {ajaxWorking && <CircularProgress size={24}/>}
                                    </Button>
                                    <Button
                                        onClick={cancelHandler}
                                        variant="outlined">
                                        {t("__cancel__")}</Button>
                                </Paper>
                            </Grid>
                        }
                    </Grid>
                {/*</form>*/}
                <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                    <Alert onClose={handleCloseSnackbar} severity={errors === undefined ? "success" : "error"}>
                        {
                            errors === undefined ? t("AdminCRUD:__success__") : t("AdminCRUD:__error__")
                        }
                    </Alert>
                </Snackbar>
            </div>
        );
    }
}

const styles = createStyles((theme: any) => ({
    root           : {
        flexGrow: 1,
    },
    fullWidth      : {
        marginBottom: "15px",
        width       : "100%"
    },
    paper          : {
        padding  : theme.spacing(2),
        textAlign: "center",
        color    : theme.palette.text.secondary,
    },
    buttonContainer: {
        textAlign: "left",
        "& > *"  : {
            margin: theme.spacing(1),
        }
    }
}));

export default withStyles(styles)(withTranslation("")(CreateView));