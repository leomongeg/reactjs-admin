import React, { Component, ReactNode }                     from "react";
import { WithTranslation, withTranslation }                from "react-i18next";
import SimpleReactValidator                                from "simple-react-validator";
import { Container, createStyles, WithStyles, withStyles } from "@material-ui/core";
import Avatar                                              from "@material-ui/core/Avatar";
import Typography                                          from "@material-ui/core/Typography";
import TextField                                           from "@material-ui/core/TextField";
import Button                                              from "@material-ui/core/Button";
import Grid                                                from "@material-ui/core/Grid";
import LockOutlinedIcon                                    from "@material-ui/icons/LockOutlined";
import Link                                                from "@material-ui/core/Link";
import CircularProgress                                    from "@material-ui/core/CircularProgress";
import { observer }                                        from "mobx-react";
import AuthSettings                                        from "../../../config/AuthSettings";


/**
 * Login Props
 */
interface LoginViewProps extends WithTranslation, WithStyles {
    validator: SimpleReactValidator;
    setPassword: (val: string) => void;
    setUsername: (val: string) => void;
    loginAction: (event: any) => void;
    password: string;
    username: string;
    setViewRef: (ref: Component) => void;
    ajaxWorking: boolean;
}

@observer
class LoginView extends Component <LoginViewProps, any> {

    constructor(props: LoginViewProps) {
        super(props);
        this.props.setViewRef(this);
    }

    /**
     * React Component render.
     */
    public render(): ReactNode {
        const {t, ajaxWorking, classes, setUsername, setPassword, loginAction, username, password, validator} = this.props;
        return (
            <>
                <Container /*component="main"*/ maxWidth="xs">
                    {/*<CssBaseline/>*/}
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography /*component="h1"*/ variant="h5">
                            {t("title")}
                        </Typography>
                        <form className={classes.form} noValidate>
                            <TextField
                                variant="outlined"
                                error={validator.message("username", username, `required|user_credentials_validation${AuthSettings.USER_INPUT_FORM_VALIDATIONS !== "" ? "|" + AuthSettings.USER_INPUT_FORM_VALIDATIONS : ""}`) === "error"}
                                margin="normal"
                                required
                                fullWidth
                                disabled={ajaxWorking}
                                id="username"
                                label={t("user_input")}
                                name="username"
                                autoComplete="username"
                                autoFocus
                                value={username}
                                onChange={(event: any) => {
                                    validator.showMessageFor("username");
                                    setUsername(event.target.value);
                                }}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                error={validator.message("password", password, `required|user_credentials_validation${AuthSettings.PASSWORD_INPUT_FORM_VALIDATIONS !== "" ? "|" + AuthSettings.PASSWORD_INPUT_FORM_VALIDATIONS : ""}`) === "error"}
                                required
                                fullWidth
                                disabled={ajaxWorking}
                                name="password"
                                label={t("password_input")}
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                value={password}
                                onChange={(event: any) => {
                                    validator.showMessageFor("password");
                                    setPassword(event.target.value);
                                }}
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                disabled={ajaxWorking}
                                color="primary"
                                className={classes.submit}
                                onClick={(event: any) => loginAction(event)}
                            >
                                {!ajaxWorking && <span>{t("signIn_btn")}</span>}
                                {ajaxWorking && <CircularProgress size={24}/>}
                            </Button>
                            <Grid container>
                                <Grid item>
                                    <Link href="#"
                                          variant="body2"
                                          onClick={(event: any) => {}}>
                                        {t("pass_forgot_cta")}
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </Container>
            </>
        );
    }
}

const styles = createStyles((theme: any) => ({
    // "@global": {
    //     body: {
    //         backgroundColor: theme.palette.common.white,
    //     },
    // },
    avatar: {
        backgroundColor: theme.palette.secondary.main,
        margin         : theme.spacing(1)
    },
    form  : {
        marginTop: theme.spacing(1),
        width    : "100%" // Fix IE 11 issue.
    },
    paper : {
        alignItems   : "center",
        display      : "flex",
        flexDirection: "column",
        marginTop    : theme.spacing(8)
    },
    submit: {
        margin      : theme.spacing(3, 0, 2),
        "min-height": 40
    },
}));

export default withStyles(styles)(withTranslation("login")(LoginView));