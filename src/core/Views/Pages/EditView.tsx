import React, { Component, ReactNode } from "react";
import { withTranslation }             from "react-i18next";
import IBaseProps                      from "../../ViewModel/Props/IBaseProps";
import { observer }                    from "mobx-react";
import Chip                            from "@material-ui/core/Chip";
import FaceIcon                        from "@material-ui/icons/Face";
import DoneIcon                        from "@material-ui/icons/Done";
import { createStyles }                from "@material-ui/core/styles";
import { withStyles }                  from "@material-ui/core";

/**
 * Component props definition
 */
interface EditViewProps extends IBaseProps {
    counter: number;
}

@observer
class EditView extends Component<EditViewProps, any> {

    constructor(props: EditViewProps) {
        super(props);
    }

    /**
     * Component Render
     */
    render(): ReactNode {
        const {classes, counter} = this.props;

        return (
            <div className={classes.root}>
                <Chip
                    icon={<FaceIcon/>}
                    label={`Counter value: ${counter}`}
                    clickable
                    color="primary"
                    deleteIcon={<DoneIcon/>}
                />
            </div>
        );
    }
}

const styles = createStyles((theme: any) => ({
    root: {
        display       : "flex",
        justifyContent: "center",
        flexWrap      : "wrap",
        "& > *"       : {
            margin: theme.spacing(0.5),
        },
    }
}));

export default withStyles(styles)(withTranslation("")(EditView));