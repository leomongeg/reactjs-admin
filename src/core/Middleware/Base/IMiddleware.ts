import * as React from "react";

/**
 * Interface for middleware usage.
 */
export interface IMiddleware<T> {
    new(): T;
}