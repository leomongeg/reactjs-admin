import * as React           from "react";
import { Container }        from "typedi";
import { ApplicationStore } from "../Store/ApplicationStore";
import { UserStore }        from "../Store/UserStore";
import { Redirect }         from "react-router";
import Middleware           from "../Decorators/Middleware";
import BaseMiddleware from "./Base/BaseMiddleware";
import { url_for }    from "../Utils/url_generator";

@Middleware
export class AuthMiddleware extends BaseMiddleware {
    /**
     * Get the middleware alias
     */
    public static getAlias(): string {
        return "AuthMiddleware";
    }

    /**
     * Validate function
     */
    validate(): boolean {
        const userStore: UserStore = Container.get(ApplicationStore).getStore(UserStore);

        return !!userStore.getUser();
    }

    /**
     * Reject function
     *
     * @param props
     */
    reject(props: any): React.ReactNode {
        return <Redirect to={{
            pathname: url_for("login-page"),
            state   : {from: props.location}
        }}/>;
    }
}