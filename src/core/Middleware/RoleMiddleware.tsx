import * as React           from "react";
import { Container }        from "typedi";
import { ApplicationStore } from "../Store/ApplicationStore";
import { UserStore }        from "../Store/UserStore";
import { Error }            from "tslint/lib/error";
import Middleware           from "../Decorators/Middleware";
import BaseMiddleware       from "./Base/BaseMiddleware";
import { Redirect }         from "react-router";
import User                 from "../../Model/User/User";

@Middleware
export class RoleMiddleware extends BaseMiddleware {
    /**
     * Get alias implementation function
     */
    public static getAlias(): string {
        return "RoleMiddleware";
    }

    /**
     * Validate Implementation
     *
     * @param options
     */
    validate(options: any): boolean {
        const user: User | undefined = Container.get(ApplicationStore).getStore(UserStore).getUser();

        if (!user) throw new Error(`The user need is logged`);
        if (!options.roles) throw new Error(`The Middleware "RoleMiddleware" need a roles on options`);

        for (const role of options.roles) {
            if (user.hasRole(role)) return true;
        }

        return false;
    }

    /**
     * Reject implementation
     */
    reject(): React.ReactNode {
        return <Redirect to={"error-403"}/>;
    }
}