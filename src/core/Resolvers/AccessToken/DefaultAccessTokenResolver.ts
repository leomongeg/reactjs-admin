import { IAccessToken }            from "../../Models/Security/AccessToken";
import { AxiosInstance }           from "axios";
import AuthSettings                from "../../../config/AuthSettings";
import API_CONST                   from "../../../config/API_CONST";
import { CONFIG_GET_ACCESS_TOKEN } from "../../../config/axios.config";
import { IAccessTokenResolver }    from "./IAccessTokenResolver";


export class DefaultAccessTokenResolver implements IAccessTokenResolver {

    /**
     * Return the access token execution
     *
     * @param usernameValue {string}
     * @param passwordValue {string}
     * @param axios {AxiosInstance}
     * @return {Promise<IAccessToken | false>}
     */
    public async getAccessToken(usernameValue: string, passwordValue: string, axios: AxiosInstance): Promise<IAccessToken | false> {
        const params                            = {
            client_id    : process.env.REACT_APP_API_BACKEND_CLIENT_ID,
            client_secret: process.env.REACT_APP_API_BACKEND_CLIENT_SECRET,
            grant_type   : process.env.REACT_APP_API_BACKEND_GRANT_TYPE,
        };
        params[AuthSettings.AUTH_USER_NAME_KEY] = usernameValue;
        params[AuthSettings.AUTH_USER_PASS_KEY] = passwordValue;

        const response = await axios.post(API_CONST.POST_ACCESS_TOKEN, params, CONFIG_GET_ACCESS_TOKEN);
        if (response.data.code && response.data.code === 500) return false;

        return {
            accessToken         : response.data.accessToken,
            accessTokenExpiresAt: response.data.accessTokenExpiresAt
        };
    }
}