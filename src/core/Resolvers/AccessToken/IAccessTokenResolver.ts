import { AxiosInstance } from "axios";
import { IAccessToken }  from "../../Models/Security/AccessToken";

/**
 * Access token call interface
 */
export interface IAccessTokenResolver {
    getAccessToken: (usernameValue: string, passwordValue: string, axios: AxiosInstance) => Promise<IAccessToken | false>;
}