import { BaseViewModel }               from "./Base/BaseViewModel";
import React, { Component, ReactNode } from "react";
import LoginView                       from "../Views/Pages/LoginView";
import IBaseProps                      from "./Props/IBaseProps";
import { withTranslation }             from "react-i18next";
import { observer }                    from "mobx-react";
import { observable }                  from "mobx";
import SimpleReactValidator            from "simple-react-validator";
import AuthService                     from "../Service/AuthService";
import { Container }                   from "typedi";
import { withRouter, generatePath }    from "react-router";
import { url_for }                     from "../Utils/url_generator";
import { UserStore }                   from "../Store/UserStore";

/**
 * Props definition
 */
interface LoginViewModelProps extends IBaseProps {

}

@observer
class LoginViewModel extends BaseViewModel <LoginViewModelProps, any> {

    @observable
    private isLoading: boolean = false;

    @observable
    private readonly validator: SimpleReactValidator;

    @observable
    private loginValid: boolean = true;

    private viewRef: Component;

    private authService: AuthService;

    private userStore: UserStore;

    constructor(props: LoginViewModelProps) {
        super(props);
        const {t}        = this.props;
        this.validator   = new SimpleReactValidator({
                                                        element   : ((message: string) => "error"),
                                                        locale    : "es",
                                                        validators: {
                                                            user_credentials_validation: {
                                                                message: t("errorInvalidCredentials"),
                                                                rule   : (val: any, params?: any) => {
                                                                    return this.loginValid;
                                                                }
                                                            }
                                                        }
                                                    });
        this.authService = Container.get(AuthService);
        this.userStore   = this.getStore(UserStore);
    }

    /**
     * React Component life cycle event
     */
    componentDidMount(): void {
        if (this.userStore.getUser()) {
            this.props.history.push(url_for("home-page"));
        }
    }

    private handleLogin = async (event: any) => {
        event.preventDefault();

        if (this.isLoading) return;

        this.isLoading = true;
        const valid    = this.validator.allValid();

        if (!valid) {
            this.validator.showMessages();
            this.isLoading = false;
            return;
        }

        const status = await this.authService.getAccessToken();

        if (status) {
            window.location.href = generatePath(url_for("home-page"));
            return;
        }

        this.loginValid = false;
        this.isLoading  = false;
        this.validator.showMessages();
    };

    private setPassword = (value: string) => {
        this.loginValid         = true;
        this.userStore.password = value;
    };

    private setUsername = (value: string) => {
        this.loginValid         = true;
        this.userStore.username = value;
    };

    /**
     * Render function
     */
    public render(): ReactNode {
        return <LoginView
            username={this.userStore.username}
            password={this.userStore.password}
            setUsername={this.setUsername}
            setPassword={this.setPassword}
            validator={this.validator}
            loginAction={this.handleLogin}
            ajaxWorking={this.isLoading}
            setViewRef={(component: Component) => this.viewRef = component}
        />;
    }
}

// @ts-ignore Ignore issue with typings in withRouter
export default withRouter(withTranslation("login")(LoginViewModel));