import React from "react";
import { createStyles, WithStyles } from "@material-ui/core/styles";
import SimpleReactValidator from "simple-react-validator";
import { withStyles } from "@material-ui/core";
import { withTranslation } from "react-i18next";
import { observable } from "mobx";
import IBaseProps from "./Props/IBaseProps";
import { WidgetWrapperViewModelType } from "./Base/WidgetWrapperViewModel";
import { FilterSchema } from "../Admin/Base/FilterSchema";
import { BaseViewModel } from "./Base/BaseViewModel";
import { FilterStore } from "../Store/FilterStore";
import TableFiltersView from "../Views/Components/Table/TableFiltersView";

/**
 * Form parameters
 */
interface TableFilterProps extends IBaseProps, WithStyles {
    filterSchema: FilterSchema;
    handleFilter: (filter: object) => void;
    ajaxWorking: boolean;
}

class FiltersViewModel extends BaseViewModel<TableFilterProps, any> {
    @observable
    private readonly validator: SimpleReactValidator;
    private filterStorage: FilterStore;
    private readonly viewRef: WidgetWrapperViewModelType[] = [];
    private readonly widgetDefaultValue: Map<string, any>;


    constructor(props: TableFilterProps, context: any) {
        super(props, context);
        this.viewRef = [];
        this.validator                    = new SimpleReactValidator({
            element: ((message: string) => message),
            locale : "es",
        });
        this.filterStorage = this.getStore(props.filterSchema.storeClass) as FilterStore;
        this.widgetDefaultValue = new Map<string, any>();
    }

    /**
     * Handle Filter button event
     * @private
     */
    private doFilter() {
        const filters = this.filterStorage.buildFilterObject(
            this.viewRef
        );
        this.props.filterSchema.subject = filters;
        // Process object and format it
        if (this.props.filterSchema.processFilter) {
            this.props.filterSchema.processFilter(filters);
        }
        console.log("FiltersViewModel.doFilter", filters);
        // Execute List filter handler
        this.props.handleFilter(filters);
    }

    /**
     * Clean all filters
     * @private
     */
    private cleanFilter() {
        this.filterStorage.clean();
        this.viewRef.forEach(widget => {
            widget.reset(true);
        });
        this.props.handleFilter({});
    }

    /**
     * Form structure
     */
    render() {
        const { t, filterSchema, classes, ajaxWorking } = this.props;
        return (
            <TableFiltersView validator={this.validator}
                              viewRef={(component: WidgetWrapperViewModelType) => {
                                  this.viewRef.push(component);
            }}
                              sections={filterSchema.sections}
                              ajaxWorking={ajaxWorking}
                              handleFilter={() => { this.doFilter(); }}
                              handleClean={() => { this.cleanFilter(); }}
            >
            </TableFiltersView>
        );
    }
}

const styles = createStyles((theme: any) => ({
    root           : {
        flexGrow: 1,
    },
    fullWidth      : {
        marginBottom: "15px",
        width       : "100%"
    },
    paper          : {
        padding  : theme.spacing(2),
        textAlign: "center",
        color    : theme.palette.text.secondary,
    },
    buttonContainer: {
        textAlign: "left",
        "& > *"  : {
            margin: theme.spacing(1),
        }
    }
}));
export default withStyles(styles)(withTranslation("")(FiltersViewModel));