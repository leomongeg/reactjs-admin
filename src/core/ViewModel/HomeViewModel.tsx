import * as React                           from "react";
import { withTranslation, WithTranslation } from "react-i18next";
import HomeView                             from "../Views/Pages/HomeView";
import { observer }                         from "mobx-react";
import { BaseViewModel }                    from "./Base/BaseViewModel";
import { NavBarStore }                      from "../Store/NavBarStore";

/**
 * Props definition interface
 */
interface HomeViewModelProps extends WithTranslation {
}

@observer
class HomeViewModel extends BaseViewModel<HomeViewModelProps, any> {
    constructor(props: HomeViewModelProps) {
        super(props);
    }

    /**
     * Life cycle event
     */
    componentDidMount(): void {
        this.getStore(NavBarStore).title = "Dashboard";
    }

    /**
     * Componente render
     */
    public render(): React.ReactNode {
        return (
            <HomeView/>
        );
    }
}

export default withTranslation("tabs")(HomeViewModel);
