import * as React        from "react";
import Header            from "../../Views/Components/Header/HeaderView";
import { UserStore }     from "../../Store/UserStore";
import { BaseViewModel } from "../Base/BaseViewModel";
import { observer }      from "mobx-react";
import { observable }    from "mobx";
import { NavBarStore }   from "../../Store/NavBarStore";

@observer
class HeaderViewModel extends BaseViewModel<{}, {}> {
    private userStore: UserStore;

    private navBarStore: NavBarStore;

    @observable
    private mobileOpen: boolean = false;

    constructor(props: any) {
        super(props);
        this.userStore   = this.getStore(UserStore);
        this.navBarStore = this.getStore(NavBarStore);
    }

    private handleDrawerToggle = () => {
        this.mobileOpen = !this.mobileOpen;
    };

    /**
     * render
     */
    public render(): React.ReactNode {
        return (
            <>
                {
                    this.userStore.getUser() === undefined ? undefined :
                    <Header
                        title={this.navBarStore.title}
                        mobileOpen={this.mobileOpen}
                        handleDrawerToggle={this.handleDrawerToggle}
                    />
                }
            </>
        );
    }
}

export default HeaderViewModel;
