import React              from "react";
import { BaseViewModel }  from "../Base/BaseViewModel";
import DrawerView         from "../../Views/Components/Header/DrawerView";
import { AdminMenuStore } from "../../Store/AdminMenuStore";
import { UserStore }      from "../../Store/UserStore";
import { TokenStore }     from "../../Store/TokenStore";

/**
 * Component Props definition
 */
interface DrawerViewModelProps {

}

class DrawerViewModel extends BaseViewModel<DrawerViewModelProps, {}> {

    private adminMenuStore: AdminMenuStore;

    constructor(props: DrawerViewModelProps) {
        super(props);
        this.adminMenuStore = this.getStore(AdminMenuStore);
    }

    private handleLogout = () => {
        this.getStore(TokenStore).logout();
    };

    /**
     * React Component Render function
     */
    render(): React.ReactNode {
        return (<DrawerView
            groups={this.adminMenuStore.groups}
            user={this.getStore(UserStore).getUser()}
            handleLogout={this.handleLogout}
        />);
    }
}

export default DrawerViewModel;