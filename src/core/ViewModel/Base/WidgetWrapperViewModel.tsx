import { BaseViewModel }                     from "./BaseViewModel";
import React, { Component, ReactNode }       from "react";
import { observer }                          from "mobx-react";
import { observable }                        from "mobx";
import SimpleReactValidator                  from "simple-react-validator";
import { FormWidgetType, IFormWidgetSchema } from "../../Admin/Base/FormSchema";
import { withTranslation }                   from "react-i18next";
import IBaseProps                            from "../Props/IBaseProps";
import TextField                             from "../../Views/Components/FormWidgets/TextField";
import MaterialUITextField                   from "@material-ui/core/TextField";
import SelectField                           from "../../Views/Components/FormWidgets/SelectField";
import BooleanField                          from "../../Views/Components/FormWidgets/BooleanField";
import DateTimeField                         from "../../Views/Components/FormWidgets/DateTimeField";
import FileUploadField                       from "../../Views/Components/FormWidgets/FileUploadField";
import SelectMultipleField                   from "../../Views/Components/FormWidgets/SelectMultipleField";
import WysiwygField                          from "../../Views/Components/FormWidgets/WysiwygField";
import RepeaterFieldViewModel                from "../../Views/Components/FormWidgets/Repeater/RepeaterFieldViewModel";
import { CleanedAndValue } from "../../Utils/Interfaces";
import ColorPickerFieldViewModel from "../../Views/Components/FormWidgets/ColorPicker/ColorPickerFieldViewModel";

/**
 * View Model Props
 */
interface WidgetWrapperViewModelProps extends IBaseProps {
    className?: any;
    setViewRef: (ref: WidgetWrapperViewModel) => void;
    objectKey: string;
    widget: IFormWidgetSchema;
    validator: SimpleReactValidator;
}

export type WidgetWrapperViewModelType = Component & {
    getValue: () => any;
    objectKey: string;
    reset: (useEmpty: boolean) => void;
    triggerEmbedValidationsHasErrors: () => boolean;
};

@observer
class WidgetWrapperViewModel extends BaseViewModel<WidgetWrapperViewModelProps, any> {

    @observable
    private value: any;

    private cleaned: any;

    private readonly _objectKey: string;

    private elementRef: any | undefined;

    constructor(props: WidgetWrapperViewModelProps) {
        super(props);
        this.value      = props.widget.initialValue;
        this.cleaned    = props.widget.initialValue;
        this._objectKey = this.props.objectKey;
        this.props.setViewRef(this);
    }

    /**
     * Return the value
     */
    getValue(): any {
        const {widget} = this.props;
        return widget.type === FormWidgetType.TYPE_REPEATER ? this.elementRef.getValue() : this.value;
    }

    /**
     * Clean widget using initial value
     */
    reset(useEmpty: boolean = false) {
        if (useEmpty) {
            this.value = this.createValueByType(this.props.widget.initialValue);
        } else {
            this.value = this.props.widget.initialValue;
        }
    }

    get objectKey(): string {
        return this._objectKey;
    }

    /**
     * Triggers the validations for the widget type Repeater child components.
     */
    triggerEmbedValidationsHasErrors(): boolean {
        const {widget} = this.props;
        if (widget.type === FormWidgetType.TYPE_REPEATER && this.elementRef)
            return this.elementRef.triggerEmbedValidationsHasErrors();

        return false;
    }

    /**
     * Choose the element to render.
     *
     * @param widget
     */
    private chooseElement(widget: IFormWidgetSchema): React.ElementType {
        let element = undefined;
        switch (widget.type) {
            case FormWidgetType.TYPE_BOOLEAN:
                element = BooleanField;
                break;
            case FormWidgetType.TYPE_CUSTOM:
                element = widget.element === undefined ? MaterialUITextField : widget.element;
                break;
            case FormWidgetType.TYPE_DATETIME:
                element = DateTimeField;
                break;
            case FormWidgetType.TYPE_FILE:
                element = FileUploadField;
                break;
            case FormWidgetType.TYPE_TEXT:
                element = TextField;
                break;
            case FormWidgetType.TYPE_SELECT:
                element = SelectField;
                break;
            case FormWidgetType.TYPE_SELECT_MULTIPLE:
                element = SelectMultipleField;
                break;
            case FormWidgetType.TYPE_WYSIWYG:
                element = WysiwygField;
                break;
            case FormWidgetType.TYPE_REPEATER:
                element = RepeaterFieldViewModel;
                break;
            case FormWidgetType.TYPE_COLOR_PICKER:
                element = ColorPickerFieldViewModel;
                break;
            default:
                element = MaterialUITextField;
                break;
        }

        return element;
    }

    /**
     * View Model View Render.
     */
    render(): ReactNode {
        const {t, widget, validator, className} = this.props;
        const {element: Element}                = {element: this.chooseElement(widget)};
        let helperText;
        const elementProps                      = {...(widget.elementProps === undefined ? {} : widget.elementProps)};

        if (widget.validations !== undefined) {
            elementProps["error"] = validator.message(widget.key, this.value, widget.validations) !== undefined;
        }

        if (widget.validations !== undefined)
            helperText = validator.message(widget.key, this.value, widget.validations) === undefined ?
                         widget.helperText : validator.message(widget.key, this.value, widget.validations);
        else helperText = widget.helperText !== undefined ? widget.helperText : "";

        elementProps["helperText"]  = t(helperText);
        elementProps["propertyKey"] = widget.key;

        if (widget.type === FormWidgetType.TYPE_REPEATER) {
            if (widget.elementProps?.buildRepeatableFields === undefined) throw Error(`The function buildRepeatableFields is required for widget type ${widget.type} in ${widget.key}`);
            elementProps["buildRepeatableFields"] = widget.elementProps.buildRepeatableFields;
        }

        elementProps["elementProps"] = widget.elementProps;

        if (className !== undefined) elementProps["className"] = className;

        return (<Element {...elementProps}
                         label={t(widget.label)}
                         setViewRef={(widget: any) => this.elementRef = widget}
                         value={this.value}
                         onChange={(value: any | CleanedAndValue) => {
                             const previousValue = this.value;
                             if (widget.validations) {
                                 validator.showMessageFor(widget.key);
                             }

                             this.value = value;

                             if (widget.onChange !== undefined)
                                 widget.onChange(this.value, previousValue);
                         }}
        />);
    }

    /**
     * Creates a empty value using the initialValue's Type
     * @param initialValue
     * @private
     */
    private createValueByType(initialValue: any) {
        if (Array.isArray(initialValue)) {
            return [];
        }
        switch (typeof initialValue) {
            case "object":
                return {};
                break;
            default:
                return "";
        }
    }
}

export default withTranslation("")(WidgetWrapperViewModel);