import { Component }                         from "react";
import { Container }                         from "typedi";
import { ApplicationStore, StoreObjectType } from "../../Store/ApplicationStore";
import { History } from "history";

export class BaseViewModel<P, S> extends Component<P, S> {
    /**
     * Save current component
     * @protected
     */
    protected _currentComponent: string;
    /**
     * Get an Store instance.
     *
     * @param storeType
     */
    protected getStore<T>(store: StoreObjectType<T>) {
        return Container.get(ApplicationStore).getStore(store);
    }

    /**
     * Save current component, is used to compare navigation
     * @param url
     */
    protected setCurrentComponent(match: { path: string }) {
        const currentComponent = match.path.split("/");
        this._currentComponent = currentComponent[1] || "";
    }

    /**
     * Checks if the navigation occurs in the same component
     */
    protected isSameComponent(history: History<any>) {
        const nextComponent = history.location.pathname.split("/");
        return this._currentComponent === nextComponent[1];
    }
}