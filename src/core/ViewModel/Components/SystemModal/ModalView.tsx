import React, { Component, ReactElement, ReactNode } from "react";
import Dialog                                        from "@material-ui/core/Dialog";
import DialogActions                                 from "@material-ui/core/DialogActions";
import DialogContent                                 from "@material-ui/core/DialogContent";
import DialogTitle                                   from "@material-ui/core/DialogTitle";
import { observer }                                  from "mobx-react";
import { ModalType }                                 from "../../../Store/ModalStore";
import { DialogContentText }                         from "@material-ui/core";
import { withTranslation }                           from "react-i18next";
import IBaseProps                                    from "../../Props/IBaseProps";

/**
 * Component Props
 */
interface ModalViewProps extends IBaseProps {
    open: boolean;
    buttons: ReactNode;
    title: string;
    content: string | ReactNode | ReactElement;
    modalType: ModalType;
}

@observer
class ModalView extends Component<ModalViewProps, any> {

    /**
     * Component Render
     */
    render(): ReactNode {
        const {t, open, buttons, content, title, modalType} = this.props;
        return (<div>

            <Dialog
                open={open}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{t(title)}</DialogTitle>
                <DialogContent dividers>
                    {
                        modalType === ModalType.CUSTOM ? content :
                        <DialogContentText id="alert-dialog-description">
                            {t(typeof content === "string" ? content : "")}
                        </DialogContentText>
                    }
                </DialogContent>
                <DialogActions>
                    {buttons}
                </DialogActions>
            </Dialog>
        </div>);
    }
}

export default withTranslation("")(ModalView);