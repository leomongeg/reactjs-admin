import { BaseViewModel }         from "../../Base/BaseViewModel";
import React, { ReactNode }      from "react";
import ModalView                 from "./ModalView";
import { ModalStore, ModalType } from "../../../Store/ModalStore";
import { observer }              from "mobx-react";
import IBaseProps                from "../../Props/IBaseProps";
import { withTranslation }       from "react-i18next";
import Button                    from "@material-ui/core/Button";
import DialogActions             from "@material-ui/core/DialogActions";
import CircularProgress          from "@material-ui/core/CircularProgress";

/**
 * View Model props
 */
interface ModalViewModelProps extends IBaseProps {

}

@observer
class ModalViewModel extends BaseViewModel<ModalViewModelProps, any> {

    private modalStore: ModalStore;

    constructor(props: ModalViewModelProps, context: any) {
        super(props, context);
        this.modalStore = this.getStore(ModalStore);
    }

    /**
     * Build the info modal type buttons.
     */
    private infoButtons(): ReactNode {
        const {t}  = this.props;
        const copy = this.modalStore.modalOptions?.ctaPrimaryText || t("common:__accept__");
        return (
            <Button
                disabled={this.modalStore.primaryButtonLoading}
                onClick={() => this.modalStore.primaryEventHandler()} color="primary">
                {
                    this.modalStore.primaryButtonLoading ? <CircularProgress size={24}/> : copy
                }
            </Button>
        );
    }

    /**
     * Build the confirm modal type buttons.
     */
    private confirmButtons(): ReactNode {
        const {t}           = this.props;
        const secondaryCopy = this.modalStore.modalOptions?.ctaSecondaryText || t("common:__cancel__");
        return (<>
            <Button disabled={this.modalStore.primaryButtonLoading}
                    onClick={() => this.modalStore.secondaryOrCancelEventHandler()} color="secondary">
                {secondaryCopy}
            </Button>
            {
                this.infoButtons()
            }
        </>);
    }

    /**
     * Build the error modal type buttons.
     */
    private errorButtons(): ReactNode {
        const {t}         = this.props;
        const primaryCopy = this.modalStore.modalOptions?.ctaPrimaryText || t("common:__close__");
        return (
            <Button onClick={() => this.modalStore.secondaryOrCancelEventHandler()} color="primary">
                {primaryCopy}
            </Button>
        );
    }

    /**
     * Render the buttons according to the modal type.
     */
    private renderButtons(): ReactNode {
        let buttons;
        switch (this.modalStore.modalOptions?.type) {
            case ModalType.CONFIRM:
                buttons = this.confirmButtons();
                break;
            case ModalType.INFO:
                buttons = this.infoButtons();
                break;
            case ModalType.ERROR:
                buttons = this.errorButtons();
                break;
            default:
                buttons = this.infoButtons();
                break;
        }
        return buttons;
    }

    /**
     * View Render
     */
    render(): ReactNode {
        if (this.modalStore.modalOptions == undefined) {
            return <></>;
        }

        const {body, type, title} = this.modalStore.modalOptions;

        return (<ModalView
            open={this.modalStore.showModal}
            title={title || ""}
            modalType={type}
            content={body}
            buttons={this.renderButtons()}
        />);
    }
}

export default withTranslation("")(ModalViewModel);