import { BaseViewModel }                      from "./Base/BaseViewModel";
import IBaseProps                             from "./Props/IBaseProps";
import React, { ReactNode }                   from "react";
import { AdminInternalRouteTypes, BaseAdmin } from "../Admin/Base/BaseAdmin";
import CreateView                             from "../Views/Pages/CreateView";
import { NavBarStore }                        from "../Store/NavBarStore";
import { observable }                         from "mobx";
import { observer }                           from "mobx-react";
import { WidgetWrapperViewModelType }         from "./Base/WidgetWrapperViewModel";
import SimpleReactValidator                   from "simple-react-validator";
import { CreateStore, CRUDType }              from "../Store/CreateStore";
import { withRouter }                         from "react-router";
import { url_for }                            from "../Utils/url_generator";

/**
 * View model component props
 */
interface CreateViewModelProps extends IBaseProps {
    adminClassInstance: BaseAdmin;
}

@observer
class CreateViewModel extends BaseViewModel<CreateViewModelProps, any> {

    private viewRef: WidgetWrapperViewModelType[] = [];

    private crudStore: CreateStore;

    @observable
    private readonly validator: SimpleReactValidator;

    @observable
    private errors: string | undefined;

    @observable
    private ajaxWorking: boolean = false;

    private readonly intervals: any[];

    @observable
    private snackbarOpen: boolean = false;

    constructor(props: CreateViewModelProps) {
        super(props);
        const {adminClassInstance} = this.props;
        this.intervals             = [];
        this.validator             = new SimpleReactValidator({
                                                                  element: ((message: string) => message),
                                                                  locale : "es",
                                                              });
        this.crudStore             = this.getStore(adminClassInstance.createSchema.storeClass) as CreateStore;
        this.crudStore.configureCrudStore(adminClassInstance);
    }

    /**
     * Life cycle event
     */
    componentDidMount() {
        this.getStore(NavBarStore).title = this.props.adminClassInstance.title;
    }

    /**
     * Life cycle event
     */
    componentWillUnmount(): void {
        this.clearAllIntervals();
        this.crudStore.clean();
    }

    /**
     * Validate the login form inputs
     */
    private isFormValid(): boolean {
        if (!this.validator.allValid()) {
            this.validator.showMessages();
            this.viewRef.map((ref: WidgetWrapperViewModelType) => {
                ref.triggerEmbedValidationsHasErrors();
                ref.forceUpdate();
            });

            return false;
        }

        // Validate embed fields
        let embedErrors = false;
        this.viewRef.map((ref: WidgetWrapperViewModelType) => {
            if (ref.triggerEmbedValidationsHasErrors()) {
                embedErrors = true;
            }
        });

        if (embedErrors) return false;

        return true;
    }

    /**
     * Perform the btn cancel handler
     */
    private cancelHandler = (evt: any) => {
        evt.preventDefault();
        const {history, adminClassInstance} = this.props;
        const route                         = adminClassInstance.internalRoutes.get(AdminInternalRouteTypes.LIST_ROUTE_KEY);
        if (route && route.router?.name) {
            history.push(url_for(route.router?.name));
            return;
        }
        history.push(url_for("home-page"));
    };

    /**
     * Handle the save event.
     */
    private saveHandler = async () => {
        if (this.ajaxWorking) return;
        if (this.intervals.length > 0) {
            this.clearAllIntervals();
            this.errors = undefined;
        }
        if (!this.isFormValid()) return;
        this.ajaxWorking  = true;
        const response    = await this.crudStore.buildObjectAndSave(this.viewRef);
        this.snackbarOpen = true;
        this.ajaxWorking  = false;
        if (!response.status) {
            this.errors = response.errors;
            window.scrollTo(0, 0);
            this.hideErrors();
            return;
        }
        const intervalId = setTimeout(() => {
            clearInterval(intervalId);
            this.successRedirect(response.data);
        }, 3000);
        this.intervals.push(intervalId);
    };

    /**
     * Success redirect
     *
     * @param object
     */
    private successRedirect(object: any) {
        const {history, adminClassInstance} = this.props;
        const route                         = adminClassInstance.internalRoutes.get(AdminInternalRouteTypes.EDIT_ROUTE_KEY);
        if (route && route.router?.name) {
            history.push(url_for(route.router?.name, {_id: object[adminClassInstance.createSchema.entityIdentifierKey]}));
            return;
        }
        history.push(url_for("home-page"));
    }

    /**
     * Hides the errors message automatically
     */
    private hideErrors() {
        const intervalId = setTimeout(() => {
            clearInterval(intervalId);
            this.errors = undefined;
        }, 30000);
        this.intervals.push(intervalId);
    }

    /**
     * Clear current active intervals
     */
    private clearAllIntervals() {
        let currentId;
        while (currentId = this.intervals.pop()) {
            clearInterval(currentId);
        }
    }

    private handleCloseSnackbar = () => {
        this.snackbarOpen = false;
    };

    /**
     * Component Render
     */
    render(): ReactNode {
        return (<CreateView
            ajaxWorking={this.ajaxWorking}
            save={this.saveHandler}
            setViewRef={(widget: WidgetWrapperViewModelType) => this.viewRef.push(widget)}
            validator={this.validator}
            sections={this.crudStore.sections}
            cancelHandler={this.cancelHandler}
            errors={this.errors}
            snackbarOpen={this.snackbarOpen}
            handleCloseSnackbar={this.handleCloseSnackbar}
            isFetchingData={false}
            crudType={CRUDType.NEW}
        />);
    }

}

export default withRouter(CreateViewModel);