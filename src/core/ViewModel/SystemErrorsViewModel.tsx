import React, { Component, ReactNode } from "react";
import { BaseViewModel }               from "./Base/BaseViewModel";
import IBaseProps                      from "./Props/IBaseProps";
import { withRouter }   from "react-router";
import SystemErrorsView from "../Views/Pages/SystemErrorsView";


/**
 * View Model Props
 */
interface SystemErrorsViewModelProps extends IBaseProps {

}

export enum ErrorPageTypes {
    UNAUTHORIZED   = "unauthorized",
    FORBIDDEN      = "forbidden",
    INTERNAL_ERROR = "internal-error",
    NOT_FOUND      = "not-found"
}

class SystemErrorsViewModel extends BaseViewModel<SystemErrorsViewModelProps, any> {

    private errorType: ErrorPageTypes;

    constructor(props: SystemErrorsViewModelProps) {
        super(props);

        const {match}  = props;
        this.errorType = match.params.key;

        this.validateErrorType();
    }

    /**
     * Validates the error and define a default if referred error type not found.
     */
    private validateErrorType() {

        switch (this.errorType) {
            case ErrorPageTypes.FORBIDDEN:
            case ErrorPageTypes.INTERNAL_ERROR:
            case ErrorPageTypes.UNAUTHORIZED:
            case ErrorPageTypes.NOT_FOUND:
                break;
            default:
                this.errorType = ErrorPageTypes.NOT_FOUND;
                break;
        }
    }

    /**
     * Component render
     */
    render(): ReactNode {
        return <SystemErrorsView errorType={this.errorType}/>;
    }
}

export default withRouter(SystemErrorsViewModel);