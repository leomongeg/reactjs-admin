import { BaseViewModel }   from "./Base/BaseViewModel";
import IBaseProps          from "./Props/IBaseProps";
import React               from "react";
import { BaseAdmin }       from "../Admin/Base/BaseAdmin";
import ListView            from "../Views/Pages/ListView";
import { PagerTableStore } from "../Store/PagerTableStore";
import { observer }        from "mobx-react";
import { NavBarStore }     from "../Store/NavBarStore";
import { UserStore }       from "../Store/UserStore";

/**
 * Component Props Definition
 */
interface ListViewModelProps extends IBaseProps {
    adminClassInstance: BaseAdmin;
}

/**
 * Definition of methods required for the list view model.
 */
export interface AdminListViewModel extends React.Component {
    /**
     * Delete item from the list
     * @param item
     */
    deleteListItem: (item: any) => void;
}

@observer
class ListViewModel extends BaseViewModel<ListViewModelProps, any> implements AdminListViewModel {

    private pagerTableStore: PagerTableStore;

    private userStore: UserStore;

    constructor(props: ListViewModelProps) {
        super(props);
        const {history} = this.props;
        this.userStore       = this.getStore(UserStore);
        this.pagerTableStore = this.getStore(PagerTableStore);
        this.pagerTableStore.configurePager(this.props.adminClassInstance);
    }

    /**
     * Life cycle event
     */
    componentDidMount() {
        const {history, adminClassInstance, match} = this.props;
        // Save current component path name
        this.setCurrentComponent(match);
        this.getStore(NavBarStore).title    = adminClassInstance.title;
        // fech information only if navigating from other component
        if (!this.pagerTableStore.sameComponent(history.location.pathname)) {
            // @TODO Call the clean filters
            console.log("[DO clean the filters]");
            this.pagerTableStore.clean();
            this.pagerTableStore.fetchPage();
        }
    }

    /**
     * perform filter information
     * @param object
     */
    handleFilter(object: any) {
        console.log("ListViewModel.handleFilter", object);
        this.pagerTableStore.filters = object;
        this.pagerTableStore.fetchPage();
    }

    /**
     * Life cycle event
     */
    componentWillUnmount() {
        const {match, history}                     = this.props;
        // save in table the previews list
        this.pagerTableStore.previousPage = match.path;
        // check navigation, and clean sore only when exiting component
        if (!this.isSameComponent(history)) {
            this.pagerTableStore.clean();
        }
    }

    /**
     * Delete list item member
     *
     * @param item
     */
    public deleteListItem(item: any): void {
        this.pagerTableStore.removeElementFromList(item);
    }

    /**
     * React Component Render
     */
    render(): React.ReactNode {
        const {adminClassInstance} = this.props;

        return <ListView
            tableSchema={adminClassInstance.tableSchema.schema}
            filterSchema={adminClassInstance.filterSchema}
            data={this.pagerTableStore.data}
            rowsPerPage={this.pagerTableStore.rowsPerPage}
            rowsPerPageOptions={this.pagerTableStore.rowsPerPageOptions}
            totalItems={this.pagerTableStore.totalItems}
            currentPage={this.pagerTableStore.page}
            handlePageChange={this.pagerTableStore.handlePageChange}
            handleRowsPerPageChange={this.pagerTableStore.handleRowsPerPageChange}
            actions={adminClassInstance.tableSchema.actions}
            isLoading={this.pagerTableStore.isLoading}
            addNew={adminClassInstance.addNewURL()}
            handleFilter={(object: any) => { this.handleFilter(object); }}
        />;
    }
}

export default ListViewModel;