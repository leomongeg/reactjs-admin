import { BaseViewModel }    from "./Base/BaseViewModel";
import IBaseProps           from "./Props/IBaseProps";
import React, { ReactNode } from "react";
import { observable } from "mobx";
import EditView       from "../Views/Pages/EditView";
import { observer }   from "mobx-react";

/**
 * Component props definition
 */
interface EditViewModelProps extends IBaseProps {

}

@observer
class EditViewModelTest extends BaseViewModel<EditViewModelProps, any> {

    @observable
    private counter: number = 0;

    constructor(props: EditViewModelProps) {
        super(props);
    }

    /**
     * React life cycle event
     */
    componentDidMount(): void {
        // setInterval(() => {
        //     this.counter ++;
        // }, 1000);
    }

    /**
     * Component render
     */
    render(): ReactNode {
        return (<EditView counter={this.counter}/>);
    }
}

export default EditViewModelTest;