import { BaseViewModel }                      from "./Base/BaseViewModel";
import { AdminInternalRouteTypes, BaseAdmin } from "../Admin/Base/BaseAdmin";
import IBaseProps                             from "./Props/IBaseProps";
import { observer }                           from "mobx-react";
import { WidgetWrapperViewModelType }         from "./Base/WidgetWrapperViewModel";
import { EditStore }                          from "../Store/EditStore";
import { observable }                         from "mobx";
import SimpleReactValidator                   from "simple-react-validator";
import { NavBarStore }                        from "../Store/NavBarStore";
import { url_for }                            from "../Utils/url_generator";
import React, { ReactNode }                   from "react";
import CreateView                             from "../Views/Pages/CreateView";
import { withRouter }                         from "react-router";
import { CRUDType }                           from "../Store/CreateStore";

/**
 * Edit view model props definition
 */
interface EditViewModelProps extends IBaseProps {
    adminClassInstance: BaseAdmin;
}

@observer
class EditViewModel extends BaseViewModel<EditViewModelProps, any> {
    private viewRef: WidgetWrapperViewModelType[] = [];

    private editStore: EditStore;

    @observable
    private readonly validator: SimpleReactValidator;

    @observable
    private errors: string | undefined;

    @observable
    private ajaxWorking: boolean = false;

    private readonly intervals: any[];

    @observable
    private snackbarOpen: boolean = false;

    @observable
    private isFetchingData: boolean = true;

    constructor(props: EditViewModelProps) {
        super(props);
        const {adminClassInstance, match} = this.props;
        this.intervals                    = [];
        this.validator                    = new SimpleReactValidator({
                                                                         element: ((message: string) => message),
                                                                         locale : "es",
                                                                     });
        this.editStore                    = this.getStore(adminClassInstance.editSchema.storeClass) as EditStore;
        this.editStore.configureStore(adminClassInstance);
        this.editStore.entityId          = match.params["_id"];
        this.getStore(NavBarStore).title = this.props.adminClassInstance.title;
    }

    /**
     * Life cycle event
     */
    componentDidMount() {
        this.fetchSubject();
    }

    /**
     * Life cycle event
     */
    componentWillUnmount(): void {
        this.editStore.clean();
        this.clearAllIntervals();
    }

    /**
     * Fetch the subject information
     */
    private async fetchSubject() {
        this.isFetchingData = true;
        const response      = await this.editStore.fetchSubject();
        this.isFetchingData = false;
        if (!response.status) {
            this.errors = response.errors;
            // this.hideErrors();
            return;
        }
    }

    /**
     * Validate the login form inputs
     */
    private isFormValid(): boolean {
        if (!this.validator.allValid()) {
            this.validator.showMessages();
            this.viewRef.map((ref: WidgetWrapperViewModelType) => {
                ref.triggerEmbedValidationsHasErrors();
                ref.forceUpdate();
            });

            return false;
        }

        // Validate embed fields
        let embedErrors = false;
        this.viewRef.map((ref: WidgetWrapperViewModelType) => {
            if (ref.triggerEmbedValidationsHasErrors()) {
                embedErrors = true;
            }
        });

        if (embedErrors) return false;

        return true;
    }

    /**
     * Perform the btn cancel handler
     */
    private cancelHandler = (evt: any) => {
        evt.preventDefault();
        const {history, adminClassInstance} = this.props;
        const route                         = adminClassInstance.internalRoutes.get(AdminInternalRouteTypes.LIST_ROUTE_KEY);
        if (route && route.router?.name) {
            history.push(url_for(route.router?.name));
            return;
        }
        history.push(url_for("home-page"));
    };

    /**
     * Handle the save event.
     */
    private saveHandler = async () => {
        if (this.ajaxWorking) return;
        if (this.intervals.length > 0) {
            this.clearAllIntervals();
            this.errors = undefined;
        }
        if (!this.isFormValid()) return;
        this.ajaxWorking  = true;
        const response    = await this.editStore.buildObjectAndSave(this.viewRef);
        this.snackbarOpen = true;
        this.ajaxWorking  = false;
        if (!response.status) {
            this.errors = response.errors;
            window.scrollTo(0, 0);
            this.hideErrors();
            return;
        }
        const intervalId = setTimeout(() => {
            clearInterval(intervalId);
            if (this.props.adminClassInstance.postPersist !== undefined) {
                this.props.adminClassInstance.postPersist(this.props.history, response);
            }
        }, 3000);
        this.intervals.push(intervalId);
    };

    /**
     * Hides the errors message automatically
     */
    private hideErrors() {
        const intervalId = setTimeout(() => {
            clearInterval(intervalId);
            this.errors = undefined;
        }, 30000);
        this.intervals.push(intervalId);
    }

    /**
     * Clear current active intervals
     */
    private clearAllIntervals() {
        let currentId;
        while (currentId = this.intervals.pop()) {
            clearInterval(currentId);
        }
    }

    private handleCloseSnackbar = () => {
        this.snackbarOpen = false;
    };

    /**
     * Component Render
     */
    render(): ReactNode {
        return (<CreateView
                ajaxWorking={this.ajaxWorking}
                save={this.saveHandler}
                setViewRef={(widget: WidgetWrapperViewModelType) => this.viewRef.push(widget)}
                validator={this.validator}
                sections={this.editStore.sections}
                cancelHandler={this.cancelHandler}
                errors={this.errors}
                snackbarOpen={this.snackbarOpen}
                handleCloseSnackbar={this.handleCloseSnackbar}
                subjectTitle={this.props.adminClassInstance.editSchema.getSubjectTitle()}
                isFetchingData={this.isFetchingData}
                crudType={CRUDType.EDIT}
            />
        );
    }
}

export default withRouter(EditViewModel);