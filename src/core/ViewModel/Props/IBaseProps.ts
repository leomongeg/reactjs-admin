import { WithTranslation }      from "react-i18next";
import { RouteComponentProps } from "react-router";

/**
 * Base interface for the ReactComponent props definition
 */
export default interface IBaseProps extends WithTranslation, RouteComponentProps<any> {
    [key: string]: any;
}