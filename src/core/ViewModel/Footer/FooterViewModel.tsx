import * as React        from "react";
import Footer            from "../../Views/Components/Footer/Footer";
import { BaseViewModel } from "../Base/BaseViewModel";

class FooterViewModel extends BaseViewModel<{}, {}> {
    constructor(props: any) {
        super(props);
    }

    /**
     * render
     */
    public render(): React.ReactNode {
        return (<Footer/>);
    }
}

export default FooterViewModel;
