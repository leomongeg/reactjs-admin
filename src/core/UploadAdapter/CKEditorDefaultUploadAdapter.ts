/**
 * CKEditor upload adapter definition
 */
export interface CKEditorUploadAdapter {

    /**
     * Starts the upload process
     */
    upload(): Promise<any>;

    /**
     * Stops the upload process
     */
    abort(): void;
}

export type CKEditorDefaultOnInit = (xhr: XMLHttpRequest) => Promise<void>;
export type CKEditorDefaultOnProcessBody = (data: FormData) => Promise<void>;
export type CKEditorDefaultOnProcessResponse = (response: any) => Promise<{ default: string; } | string>;

/**
 * Based on this stackoverflow example:
 * @url https://stackoverflow.com/a/46773627
 */
export class CKEditorDefaultUploadAdapter implements CKEditorUploadAdapter {
    protected loader: any;
    private xhr: XMLHttpRequest;
    private readonly onInit: CKEditorDefaultOnInit;
    private readonly onProcessBody: CKEditorDefaultOnProcessBody;
    private readonly onProcessResponse: CKEditorDefaultOnProcessResponse;
    private fileKey: string = "file";


    constructor(loader: any,
                onInit: CKEditorDefaultOnInit,
                onProcessBody: CKEditorDefaultOnProcessBody,
                onProcessResponse: CKEditorDefaultOnProcessResponse) {
        this.loader            = loader;
        this.onInit            = onInit;
        this.onProcessBody     = onProcessBody;
        this.onProcessResponse = onProcessResponse;
    }

    /**
     * Starts the upload process
     */
    upload(): Promise<any> {
        return new Promise(async (resolve, reject) => {
            this.loader.file.then(async (file: any) => {
                await this.initRequest();
                await this.initListeners(resolve, reject, file);
                await this.sendRequest(file);
            });
        });
        return Promise.resolve();
    }

    /**
     * Stops the upload process
     */
    abort(): void {
        if (this.xhr) {
            this.xhr.abort();
        }
    }

    /**
     * Init and configure the image upload request
     * @private
     */
    private async initRequest() {
        const xhr        = (this.xhr = new XMLHttpRequest());
        xhr.responseType = "json";
        // xhr.open("POST", this.url, true);
        if (this.onInit) await this.onInit(xhr);
    }

    /**
     * Initialize the file upload listeners
     *
     * @param resolve
     * @param reject
     * @param file
     * @private
     */
    private initListeners(resolve: any, reject: any, file: any) {
        const xhr              = this.xhr;
        const loader           = this.loader;
        const genericErrorText = "Couldn't upload file:" + ` ${file.name}.`;

        xhr.addEventListener("error", () => reject(genericErrorText));
        xhr.addEventListener("abort", () => reject());

        xhr.addEventListener("load", async () => {
            const response = xhr.response;
            if (this.onProcessResponse) {
                const result = await this.onProcessResponse(response);
                if (typeof result === "string")
                    return reject(result);
                resolve(result);
            }
        });

        if (xhr.upload) {
            xhr.upload.addEventListener("progress", (evt: ProgressEvent<XMLHttpRequestEventTarget>) => {
                if (evt.lengthComputable) {
                    loader.uploadTotal = evt.total;
                    loader.uploaded    = evt.loaded;
                }
            });
        }
    }

    /**
     * Add the file to the request
     * @param file
     * @private
     */
    private async sendRequest(file: any) {
        const data = new FormData();

        data.append(this.fileKey, file);
        if (this.onProcessBody) await this.onProcessBody(data);

        this.xhr.send(data);
    }

}