import * as React from "react";

export class ErrorBoundaryHandler extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {hasError: false};
    }

    /**
     * React Life cycle event.
     * @TODO: Pending send the data to the backend for the error reporting.
     *
     * @param error
     * @param info
     */
    componentDidCatch(error: Error, info: React.ErrorInfo) {
        // Display fallback UI
        // this.setState({hasError: true});
        // You can also log the error to an error reporting service
        // logErrorToMyService(error, info);
        this.setState({hasErrors: true});
        console.log(`[ERROR] => Hello I'm the Error Boundary View Model. Seems like there is an uncaught exceptions:`);
        console.dir(error);
        console.log(error, JSON.stringify(error));
        console.log(info, JSON.stringify(info));
    }

    /**
     * React Component render
     */
    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <h1>Something went wrong.</h1>;
        }
        return this.props.children;
    }
}