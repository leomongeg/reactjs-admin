import * as React            from "react";
import BaseMiddleware        from "../Middleware/Base/BaseMiddleware";
import SystemErrorsViewModel from "../ViewModel/SystemErrorsViewModel";
import { BaseAdmin }         from "../Admin/Base/BaseAdmin";
import PageViewModels        from "../../config/PageViewModels";

/**
 * Base interface for the route object
 */
export interface IRoute {
    auth?: boolean;
    component: React.ComponentType;
    exact?: boolean;
    layout?: React.ComponentType;
    middleware?: BaseMiddleware[];
    options?: any;
    name: string;
    path: string;
    adminClassInstance?: BaseAdmin;
}

const routesList: IRoute[] = [
    {
        auth     : true,
        component: PageViewModels.DashboardPageViewModel,
        exact    : true,
        name     : "home-page",
        path     : "/",
    },
    {
        auth     : false,
        component: PageViewModels.LoginViewModel,
        exact    : true,
        name     : "login-page",
        path     : "/login",
    },
    {
        auth     : false,
        component: SystemErrorsViewModel,
        exact    : true,
        name     : "system-errors",
        path     : "/error/:key",
    }
];

export default routesList;
