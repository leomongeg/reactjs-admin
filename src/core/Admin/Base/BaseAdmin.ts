import React                                 from "react";
import { IRoute }                            from "../../Routes/routes";
import slugify                               from "slugify";
import { SvgIconComponent }                  from "@material-ui/icons";
import AddIcon                               from "@material-ui/icons/Add";
import EditIcon                              from "@material-ui/icons/Edit";
import ListIcon                              from "@material-ui/icons/List";
import { History }                           from "history";
import ListViewModel, { AdminListViewModel } from "../../ViewModel/ListViewModel";
import { TableSchema }                       from "./TableSchema";
import AxiosService                          from "../../Service/AxiosService";
import { Container }                         from "typedi";
import { url_for }                           from "../../Utils/url_generator";
import CreateViewModel                       from "../../ViewModel/CreateViewModel";
import { FormSchema }                        from "./FormSchema";
import EditViewModel                         from "../../ViewModel/EditViewModel";
import { CreateStore }                       from "../../Store/CreateStore";
import { EditStore }                         from "../../Store/EditStore";
import User                                  from "../../../Model/User/User";
import { ApplicationStore, StoreObjectType } from "../../Store/ApplicationStore";
import { ModalStore, ModalType }             from "../../Store/ModalStore";
import { PagerTableStore }                   from "../../Store/PagerTableStore";
import { AxiosResponse }                     from "axios";
import { FilterSchema } from "./FilterSchema";
import { FilterStore } from "../../Store/FilterStore";

/**
 * Schema definition interface
 */
export interface AdminSchema {
    [key: string]: string | number | Array<AdminSchema> | boolean;
}

export type CRUDRoute = {
    method: "POST" | "GET" | "DELETE" | "PUT";
    path: string; // Example: /api/find-by-id/:id/:slug
    preExecute?: (path: string, data?: any) => { params: any, endpoint?: string },
    postExecute?: (response: AxiosResponse | undefined) => { status: boolean, errors: string, data: any | undefined };
};

/**
 * Definition of the Admin Route Schema
 */
export interface AdminRouterSchema {
    router?: IRoute;
    displayText?: string;
    icon?: SvgIconComponent;
    color?: string;
    onClick?: (history?: History) => void;
    roles: string[];
    showInMainMenu?: boolean;
    mainMenuGroup?: string;
    disable?: boolean;
}

export enum AdminInternalRouteTypes {
    LIST_ROUTE_KEY = "route_list",
    EDIT_ROUTE_KEY = "route_edit",
    NEW_ROUTE_KEY  = "rout_new"
}

export abstract class BaseAdmin {
    protected _postPersist: undefined | ((history: any, entity: any) => void);

    private readonly _internalRoutes: Map<AdminInternalRouteTypes | string, AdminRouterSchema>;

    private initialized: boolean = false;

    protected _menuGroup: string;

    private axios: AxiosService;

    private _title: string;

    private readonly _slug: string;

    /**
     * Represents the current logged user.
     */
    private readonly _user: User;

    /**
     * Represents the entity identifier that this field will link to the show/edit page of this particular schema
     */
    protected entityIdentifierKey: string = "_id";

    /**
     * Endpoint to read the list of data for the admin table
     */
    protected _API_GET_ITEMS: CRUDRoute | string;

    /**
     * Endpoint to read the detail for an individual item
     */
    protected _API_GET_ITEM: CRUDRoute | string;

    /**
     * Endpoint to update a specific item
     */
    protected _API_PUT_ITEM: CRUDRoute | string;

    /**
     * Endpoint to remove a specific item
     */
    protected _API_DELETE_ITEM: CRUDRoute | string;

    /**
     * Endpoint to create a new ite
     */
    protected _API_POST_ITEM: CRUDRoute | string;

    /**
     * If implemented handle the behavior once the API return success from the delete process.
     *
     * @deprecated Use CRUDRoute.postExecute Handler
     * @protected
     */
    protected postDeleteElement: (data: any) => void | undefined;

    /**
     * Schema definition for create a new item
     */
    private readonly _createSchema: FormSchema;

    /**
     * Schema definition to edit a new item
     */
    private readonly _editSchema: FormSchema;

    /**
     * Schema definition for the table view
     */
    private readonly _tableSchema: TableSchema;
    /**
     * Schema definition for filters
     * @private
     */
    private readonly _filterSchema: FilterSchema;

    /**
     * Initialize function
     */
    protected abstract initialize(): void;

    /**
     * Configure the list view delegate method
     */
    abstract configureListView(tableSchema: TableSchema): void;

    /**
     * Configure the edit view delegate method
     */
    abstract async configureEditView(formSchema: FormSchema, subject?: any): Promise<void>;

    /**
     * Configure the create view delegate method
     */
    abstract configureCreateView(formSchema: FormSchema): void;

    /**
     * Configure the filters shown in the list view
     * @param filters
     */
    abstract configureFilters(filters: FilterSchema): void;

    protected defaultListViewModel: React.ComponentType = ListViewModel;

    protected defaultEditFormViewModel: React.ComponentType = EditViewModel;

    protected defaultCreateFormViewModel: React.ComponentType = CreateViewModel;

    protected constructor(user: User) {
        if (!this.constructor.name.endsWith("Admin")) {
            throw `Admin classes should end with admin not like ${this.constructor.name}`;
        }
        this._user                    = user;
        this._slug                    = slugify(this.constructor.name.toLowerCase());
        this.axios                    = Container.get(AxiosService);
        this._internalRoutes          = new Map<string, AdminRouterSchema>();
        this._menuGroup               = `menu_group_${this.slug}`;
        this._tableSchema             = new TableSchema();
        this._createSchema            = new FormSchema();
        this._editSchema              = new FormSchema();
        this._createSchema.storeClass = CreateStore;
        this._editSchema.storeClass   = EditStore;
        this._title                   = this.constructor.name.replace("Admin", "");
        this._filterSchema            = new FilterSchema();
        this._filterSchema.storeClass = FilterStore;
    }

    /**
     * Initialize internal routes:
     * - List
     * - Edit
     * - Create
     */
    public init() {
        if (this.initialized) return;
        this.initialized = true;
        const title      = this.constructor.name.replace("Admin", "");
        this._internalRoutes.set(AdminInternalRouteTypes.LIST_ROUTE_KEY, {
            router        : {
                auth     : true,
                component: this.defaultListViewModel,
                exact    : true,
                name     : slugify(`${title.toLowerCase()}-list`),
                path     : `/${slugify(title.toLowerCase())}/list`
            },
            icon          : ListIcon,
            displayText   : slugify(`${title.toLowerCase()}-list`),
            showInMainMenu: true,
            roles         : []
        });
        this._internalRoutes.set(AdminInternalRouteTypes.EDIT_ROUTE_KEY, {
            router     : {
                auth     : true,
                component: this.defaultEditFormViewModel,
                exact    : true,
                name     : slugify(`${title.toLowerCase()}-edit`),
                path     : `/${slugify(title.toLowerCase())}/edit/:${this.entityIdentifierKey}`
            },
            icon       : EditIcon,
            displayText: slugify(`${title.toLowerCase()}-edit`),
            roles      : []
        });
        this._internalRoutes.set(AdminInternalRouteTypes.NEW_ROUTE_KEY, {
            router     : {
                auth     : true,
                component: this.defaultCreateFormViewModel,
                exact    : true,
                name     : slugify(`${title.toLowerCase()}-new`),
                path     : `/${slugify(title.toLowerCase())}/new`
            },
            icon       : AddIcon,
            displayText: slugify(`${title.toLowerCase()}-new`),
            roles      : []
        });

        this.initialize();
        this.tableSchema.entityIdentifierKey = this.entityIdentifierKey;
        this.configureTableActions();
        this.configureListView(this.tableSchema);
        this.configureFilters(this.filterSchema);
        this.configureCreateView(this.createSchema);
        this.tableSchema.add({
                                 key  : "__actions__",
                                 label: "__actions__"
                             });
    }

    get menuGroup(): string {
        return this._menuGroup;
    }

    /**
     * Add new router object to the internal routes
     * @param key
     * @param obj
     */
    protected addNewRoute(key: string, obj: AdminRouterSchema): void {
        this._internalRoutes.set(key, obj);
    }

    /**
     * Create the table actions.
     */
    private configureTableActions(): void {
        const editRoles = this.internalRoutes.get(AdminInternalRouteTypes.EDIT_ROUTE_KEY)?.roles;
        if (this.user.hasRole(editRoles || []))
            this.tableSchema.actions.push({
                                              name   : "__edit__",
                                              handler: (data: any, history: any) => {
                                                  const params                                 = {};
                                                  params[this.tableSchema.entityIdentifierKey] = data[this.tableSchema.entityIdentifierKey];
                                                  history.push(url_for(this.internalRoutes.get(AdminInternalRouteTypes.EDIT_ROUTE_KEY)?.router?.name || "", params));
                                              }
                                          });
        this.tableSchema.actions.push({
                                          name   : "__delete__",
                                          handler: (data: any, history: any) => {
                                              const params                                 = {};
                                              params[this.tableSchema.entityIdentifierKey] = data[this.tableSchema.entityIdentifierKey];
                                              this.getStore(ModalStore).configureModal({
                                                                                           title: "__delete_confirm_title__",
                                                                                           body : "__delete_confirm_body__",

                                                                                           type: ModalType.CONFIRM
                                                                                       })
                                                  .addPrimaryClickHandler(async (modalStore: ModalStore) => {
                                                      await this.getStore(EditStore)
                                                                .handleDeleteElement(this._API_DELETE_ITEM, this.entityIdentifierKey, data);
                                                      this.getStore(PagerTableStore).removeElementFromList(data);
                                                      modalStore.doHideModal();
                                                      if (this.postDeleteElement !== undefined) {
                                                          this.postDeleteElement(data);
                                                          return;
                                                      }
                                                  })
                                                  .doShowModal();
                                          }
                                      });
    }

    /**
     * Ensures if the current user can perform an specific routing action
     * @param action
     */
    public userCan(action: AdminInternalRouteTypes): boolean {
        const route = this._internalRoutes.get(action);
        return this.user.hasRole(route ? route.roles : []);
    }

    /**
     * Build the edit form view
     * @param object
     */
    public async buildEditFormView(subject: any) {
        this.editSchema.subject = subject;
        await this.configureEditView(this.editSchema, subject);
    }

    /**
     * Return the URL String path for the add New route
     * @param params
     */
    public addNewURL(params?: any): string | false {
        if (!this.userCan(AdminInternalRouteTypes.NEW_ROUTE_KEY)) return false;
        const route = this._internalRoutes.get(AdminInternalRouteTypes.NEW_ROUTE_KEY);
        return (!route || route.router === undefined) ? false : url_for(route.router.name, params);
    }

    get internalRoutes(): Map<string, AdminRouterSchema> {
        return this._internalRoutes;
    }

    get user(): User {
        return this._user;
    }

    get tableSchema(): TableSchema {
        return this._tableSchema;
    }

    get createSchema(): FormSchema {
        return this._createSchema;
    }

    get editSchema(): FormSchema {
        return this._editSchema;
    }

    get API_GET_ITEMS(): CRUDRoute | string {
        return this._API_GET_ITEMS;
    }

    get API_GET_ITEM(): CRUDRoute | string {
        return this._API_GET_ITEM;
    }

    get API_PUT_ITEM(): CRUDRoute | string {
        return this._API_PUT_ITEM;
    }

    get API_DELETE_ITEM(): CRUDRoute | string {
        return this._API_DELETE_ITEM;
    }

    get API_POST_ITEM(): CRUDRoute | string {
        return this._API_POST_ITEM;
    }

    get title(): string {
        return this._title;
    }

    get slug(): string {
        return this._slug;
    }

    /**
     * Merge data into a given field
     * @param data
     * @param fields
     * @param container
     * @private
     */
    protected groupFields(data: any, fields: string[], container: string = "content") {
        if (fields.length > 0 && data[container] === undefined) {
            data[container] = {};
        }
        fields.forEach(field => {
            if (data[field] !== undefined) {
                if (data[field] && data[field] !== "") {
                    data[container][field] = data[field];
                }
                delete data[field];
            }
        });
    }

    /**
     * Action to execute after success save
     */
    get postPersist(): ((history: any, response: any) => void) | undefined {
        return this._postPersist;
    }

    /**
     * Get an Store instance.
     *
     * @param storeType
     */
    protected getStore<T>(store: StoreObjectType<T>) {
        return Container.get(ApplicationStore).getStore(store);
    }

    /**
     * Get the filter schame
     */
    get filterSchema(): any {
        return this._filterSchema;
    }
}