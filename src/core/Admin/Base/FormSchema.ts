import TextField                         from "@material-ui/core/TextField";
import React                             from "react";
import { observable }                    from "mobx";
import { ReactTableCellRenderComponent } from "./TableSchema";
import BaseStore                         from "../../Store/Base/BaseStore";
import { StoreObjectType }               from "../../Store/ApplicationStore";
import { v4 }                            from "uuid";

/**
 * Form element definitions.
 */
export interface IFormElementProps {
    [key: string]: any;
}

/**
 * Form Schema Definition
 */
export interface IFormWidgetSchema {
    key: string;
    type: FormWidgetType;
    label: string;
    element?: React.ElementType<JSX.Element> | any;
    validations?: string;
    initialValue: string | any;
    elementProps?: IFormElementProps;
    helperText?: string;
    onChange?: (value: string | any, previousValue?: string | any) => void;
    options?: any[];
    mapper?: (key: string, obj: any) => any; // Used only during the edit schema build
    // buildRepeatableFields?: (schema: FormSchema) => void; // Used only when type is TYPE_REPEATER
}

export enum FormWidgetType {
    TYPE_BOOLEAN         = "TYPE_BOOLEAN",
    TYPE_CUSTOM          = "TYPE_CUSTOM",
    TYPE_DATETIME        = "TYPE_DATETIME",
    TYPE_FILE            = "TYPE_FILE",
    TYPE_TEXT            = "TYPE_TEXT",
    TYPE_SELECT          = "TYPE_SELECT",
    TYPE_SELECT_MULTIPLE = "TYPE_SELECT_MULTIPLE",
    TYPE_WYSIWYG         = "TYPE_WYSIWYG",
    TYPE_REPEATER        = "TYPE_REPEATER",
    TYPE_COLOR_PICKER    = "TYPE_COLOR_PICKER",
}

/**
 * Form section definition
 */
export interface IFormSection {
    key: string;
    breakpoints: { [key: string]: number };
    widgets: IFormWidgetSchema[];
}

export class FormSchema {
    private static readonly DEFAULT_SECTION: string = "default";

    private _sections: Map<String, IFormSection>;

    private section: string = FormSchema.DEFAULT_SECTION;

    private _entityIdentifierKey: string = "_id";

    private _uuid: string;

    /**
     * @deprecated since added CRUDRoute behavior
     * @private
     */
    private _apiRequestMethod: "POST" | "PUT" | undefined;

    /**
     * Entity object to be mapped during the edit form only.
     */
    private _subject: any | undefined;

    private _subjectTitleKey: string;

    private _storeClass: StoreObjectType<BaseStore>;

    /**
     * Event called before call the API to save the object. The returned Object is the data that should be sent to
     * the server.
     */
    private _preSave: (data: any) => void;

    constructor() {
        this._uuid            = v4();
        this._subjectTitleKey = "_id";
        this._sections        = new Map<String, IFormSection>();
        this._sections.set(FormSchema.DEFAULT_SECTION, {
            key        : FormSchema.DEFAULT_SECTION,
            breakpoints: {xs: 12},
            widgets    : []
        });
    }

    /**
     * Add a new widget to build the form
     *
     * @param widget
     */
    public add(widget: IFormWidgetSchema): this {
        const section = this._sections.get(this.section);
        if (this._subject) {
            this.setInitialValue(widget);
        }
        section?.widgets.push(widget);
        return this;
    }

    /**
     * Add a new form section section
     * @param section
     * @param breakpoints
     */
    public with(section: string, breakpoints?: { [key: string]: number }): this {
        this.section = section;
        this._sections.set(section, {
            key        : section,
            breakpoints: breakpoints === undefined ? {xs: 12} : breakpoints,
            widgets    : []
        });
        return this;
    }

    /**
     * End the build of the section build.
     */
    public end(): this {
        this.section = FormSchema.DEFAULT_SECTION;
        return this;
    }

    get sections(): Map<String, IFormSection> {
        return this._sections;
    }


    get preSave(): (data: any) => void {
        return this._preSave;
    }

    set preSave(value: (data: any) => void) {
        this._preSave = value;
    }

    get entityIdentifierKey(): string {
        return this._entityIdentifierKey;
    }

    set entityIdentifierKey(value: string) {
        this._entityIdentifierKey = value;
    }

    get subject(): any {
        return this._subject;
    }

    set subject(value: any) {
        // update fields
        this._subject = value;
        if (this.sections) {
            Array.from(this.sections.values()).forEach(section => {
                section.widgets = section.widgets.map(widget => {
                    this.setInitialValue(widget);
                    return widget;
                });
            });
        }
    }

    /**
     * Set initial value into object
     * @param widget
     * @private
     */
    private setInitialValue(widget: IFormWidgetSchema) {
        if (widget.mapper)
            widget.initialValue = widget.mapper(widget.key, this._subject);
        else
            widget.initialValue = this._subject.hasOwnProperty(widget.key) ? this._subject[widget.key] === undefined ? widget.initialValue : this._subject[widget.key] : widget.initialValue;
        if (widget.initialValue === undefined)
            throw Error(`initialValue can't be undefined for property ${widget.key}`);
    }

    /**
     * @deprecated since added CRUDRoute behavior
     */
    get apiRequestMethod(): "POST" | "PUT" | undefined {
        return this._apiRequestMethod;
    }

    /**
     * @deprecated since added CRUDRoute behavior
     */
    set apiRequestMethod(value: "POST" | "PUT" | undefined) {
        this._apiRequestMethod = value;
    }

    get subjectTitleKey(): string {
        return this._subjectTitleKey;
    }

    set subjectTitleKey(value: string) {
        this._subjectTitleKey = value;
    }

    get storeClass(): StoreObjectType<BaseStore> {
        return this._storeClass;
    }

    set storeClass(value: StoreObjectType<BaseStore>) {
        this._storeClass = value;
    }

    get uuid(): string {
        return this._uuid;
    }

    /**
     * Return the subject title
     */
    public getSubjectTitle(): string {
        if (!this.subject || !this.subject.hasOwnProperty(this.subjectTitleKey)) return "";
        return this.subject[this.subjectTitleKey].toString();
    }

    /**
     * Clean the schema
     */
    public clean() {
        this._sections = new Map<String, IFormSection>();
        this._sections.set(FormSchema.DEFAULT_SECTION, {
            key        : FormSchema.DEFAULT_SECTION,
            breakpoints: {xs: 12},
            widgets    : []
        });
        this.subject = undefined;
    }
}