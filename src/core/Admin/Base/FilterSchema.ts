import { FormSchema, IFormWidgetSchema } from "./FormSchema";

/**
 * Form section definition
 */
export interface IFilterSection {
    key: string;
    breakpoints: { [key: string]: number };
    widgets: IFormWidgetSchema[];
}

export class FilterSchema extends FormSchema {

    /**
     * Allows to create a function to process filters before running the fetchPage
     * @protected
     */
    private _processFilter: (object: any) => void;

    /**
     * Check if there are filters
     */
    public hasFilters() {
        let filters = 0;
        Array.from(this.sections.keys()).forEach(section => {
            const sectionMap = this.sections.get(section);
            if (sectionMap !== undefined) {
                filters = filters + sectionMap.widgets.length;
            }
        });
        return filters > 0;
    }

    get processFilter(): (object: any) => void {
        return this._processFilter;
    }

    set processFilter(value: (object: any) => void) {
        this._processFilter = value;
    }
}