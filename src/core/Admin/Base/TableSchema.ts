/**
 * Table head schema
 */
import { AxiosResponse }   from "axios";
import { PagerTableStore } from "../../Store/PagerTableStore";
import React               from "react";
import { History }         from "history";

/**
 * Table head Options
 */
export interface ITableHeadOptions {
    disablePadding: boolean;
    numeric: boolean;
}

export type ReactTableCellRenderComponent = {
    Component: React.ReactNode,
    props: any
};

/**
 * Table view Schema
 */
export interface ITableSchema {
    key: string;
    label: string;
    options?: ITableHeadOptions;
    render?: (key: string, obj: any, t?: any) => ReactTableCellRenderComponent | string;
}

/**
 * Table actions definition
 */
export interface TableActions {
    name: string;
    icon?: React.ElementType;
    handler: (data: any, history: History) => void;
}

/**
 * @TODO:
 *  - Handle renders for primitive types like boolean
 */
export class TableSchema {
    private readonly _schema: ITableSchema[];

    private _rowsPerPageOptions: number[] = [5, 10, 25, 50, 100];

    private readonly _actions: any[];

    /**
     * Defines in the remote pager, if the first page is 0 or 1
     */
    private _paginationStarts: 0 | 1 = 1;

    /**
     * Use this handler to parse data, apply filter, etc
     */
    private _apiResponseHandler: (response: AxiosResponse, pagerStore?: PagerTableStore) => any[];

    /**
     * Defines the entity Identifier for the built in actions like edit or delete. This value
     * correspond to the Entity identifier like the unique id.
     */
    private _entityIdentifierKey: string = "_id";

    constructor() {
        this._schema  = [];
        this._actions = [];
    }

    /**
     * Add new item to the table definition.
     *
     * @param item
     */
    public add(item: ITableSchema): this {
        if (item.options === undefined) {
            item.options = {
                disablePadding: false,
                numeric       : false
            };
        }

        this._schema.push(item);

        return this;
    }

    get schema(): ITableSchema[] {
        return this._schema;
    }


    get rowsPerPageOptions(): number[] {
        return this._rowsPerPageOptions;
    }

    set rowsPerPageOptions(value: number[]) {
        this._rowsPerPageOptions = value;
    }

    get apiResponseHandler(): (response: AxiosResponse, pagerStore?: PagerTableStore) => any[] {
        return this._apiResponseHandler;
    }

    set apiResponseHandler(value: (response: AxiosResponse, pagerStore?: PagerTableStore) => any[]) {
        this._apiResponseHandler = value;
    }

    get paginationStarts(): 0 | 1 {
        return this._paginationStarts;
    }

    set paginationStarts(value: 0 | 1) {
        this._paginationStarts = value;
    }

    get entityIdentifierKey(): string {
        return this._entityIdentifierKey;
    }

    set entityIdentifierKey(value: string) {
        this._entityIdentifierKey = value;
    }

    get actions(): any[] {
        return this._actions;
    }
}