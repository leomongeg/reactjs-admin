import { Container, Service }                      from "typedi";
import { AxiosError, AxiosInstance, AxiosPromise } from "axios";
import AxiosService                                from "./AxiosService";
import API_CONST                                   from "../../config/API_CONST";

@Service()
export default class AjaxService {

    /**
     * Return the axios object instance
     * @return {AxiosInstance}
     */
    private getAxios(): AxiosInstance {
        return Container.get(AxiosService).axios;
    }

    /**
     * Replace URL
     * @param url
     * @param params
     * @return {string}
     */
    protected replaceURL(url: string, params: object = {}): string {
        for (const key in params) {
            url = url.replace(`:${key}`, params[key]);
        }

        return url;
    }

    /**
     * Return web service me action call
     * @return {AxiosPromise<any>}
     */
    public getMe(): AxiosPromise<any> {
        return this.getAxios().get(API_CONST.GET_ME);
    }
}
