import AccessToken                 from "../Models/Security/AccessToken";
import API_CONST                   from "../../config/API_CONST";
import { deserialize }             from "serializr";
import { Container, Service }      from "typedi";
import AxiosService                from "./AxiosService";
import { UserStore }               from "../Store/UserStore";
import { TokenStore }              from "../Store/TokenStore";
import { ApplicationStore }        from "../Store/ApplicationStore";
import { generatePath }            from "react-router";
import AjaxService                 from "./AjaxService";
import JwtDecode                   from "jwt-decode";
import { url_for }                 from "../Utils/url_generator";
import { ErrorPageTypes }          from "../ViewModel/SystemErrorsViewModel";
import { AdminService }            from "./AdminService";
import { CONFIG_GET_ACCESS_TOKEN } from "../../config/axios.config";
import AuthSettings                from "../../config/AuthSettings";
import User                        from "../../Model/User/User";

@Service()
export default class AuthService {
    /**
     * Return the axios service instance
     * @return {AxiosService}
     */
    private getAxiosService(): AxiosService {
        return Container.get(AxiosService);
    }

    /**
     * Return ajax service instance
     * @return {AjaxService}
     */
    private getAjaxService(): AjaxService {
        return Container.get(AjaxService);
    }

    /**
     * Return application store instance
     * @return {ApplicationStore}
     */
    private getApplicationStore(): ApplicationStore {
        return Container.get(ApplicationStore);
    }

    /**
     * Return the user store instance
     * @return {UserStore}
     */
    private getUserStore(): UserStore {
        return this.getApplicationStore().getStore(UserStore);
    }

    /**
     * Return the token store instance
     * @return {TokenStore}
     */
    private getTokenStore(): TokenStore {
        return this.getApplicationStore().getStore(TokenStore);
    }

    /**
     * Execute the process to obtain the access token
     * @param tokenSaml
     * @param cb
     * @return {void}
     */
    public async getAccessToken(cb?: (err: boolean, user: User | undefined) => void): Promise<boolean> {
        let accessToken;
        try {
            accessToken = await this.getAxiosService().getAccessToken(this.getUserStore().username, this.getUserStore().password);
        } catch (e) {
            accessToken = undefined;
        }

        // Access Token Invalid
        if (!accessToken) {
            return false;
        }

        const token = new AccessToken(accessToken);

        if (this.getTokenStore())
            this.getTokenStore().setAccessToken(token);

        await this.getUserStore().bindSignedInUser();
        const user = this.getUserStore().getUser();

        if (!this.getUserStore() || !user || user.id === undefined) {
            if (cb)
                cb(true, undefined);

            return false;
        }

        if (cb)
            cb(false, user);

        this.getUserStore().username = "";
        this.getUserStore().password = "";

        if (!user.hasRole(AuthSettings.ADMIN_ROLE)) {
            try {
                await this.getTokenStore().logout();
            } catch (e) {

            }

            window.location.href = generatePath(url_for("system-errors", {key: ErrorPageTypes.UNAUTHORIZED}));
            return false;
        }

        return true;
    }

    /**
     * Redirect user to login page
     */
    public redirectToLogin(): void {
        window.location.href = "/login";
    }

    /**
     * Perform logout process and remove all user data.
     */
    public async logout() {
        try {
            await this.getTokenStore().logout();
        } catch (e) {
            // @TODO: Fix the issue with the logout to the BE
        }

        window.location.href = generatePath(url_for("home-page"));
    }

    /**
     * Check if user is authenticated
     */
    public isAuthenticate(): boolean {
        return !!this.getUserStore().getUser();
    }

    /**
     * Return the current logged in user object
     */
    public getUser(): User | undefined {
        return this.getUserStore().getUser();
    }
}
