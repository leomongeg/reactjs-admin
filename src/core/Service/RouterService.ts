import { Service } from "typedi";
import routesList  from "../Routes/routes";

@Service()
export class RouterService {
    private static routes: { [key: string]: string };

    constructor() {
        this.generateRoutes();
    }

    /**
     * Generate the routes paths by the route name.
     */
    private generateRoutes() {
        if (RouterService.routes) return;

        const routes                           = routesList;
        const paths: { [key: string]: string } = {};

        for (const route of routes) {
            paths[route.name] = route.path;
        }

        RouterService.routes = paths;
    }

    /**
     * Returns the path for the given route name.
     *
     * @param routeName
     */
    public getRoutePathByName(routeName: string): string {
        if (!RouterService.routes.hasOwnProperty(routeName))
            throw Error(`Route with name: ${routeName} doesn't exist in routesList`);
        return RouterService.routes[routeName];
    }
}