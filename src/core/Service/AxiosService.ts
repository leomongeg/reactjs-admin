import Axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import {
    CONFIG_CALL_GET_API,
    CONFIG_CALL_POST_API,
    CONFIG_GET_ACCESS_TOKEN
}                                                   from "../../config/axios.config";
import { Container, Service }                       from "typedi";
import { stringify }                                from "qs";
import { ApplicationStore }                         from "../Store/ApplicationStore";
import AccessToken, { IAccessToken }                from "../Models/Security/AccessToken";
import API_CONST                                    from "../../config/API_CONST";
import { TokenStore }                               from "../Store/TokenStore";
import { generatePath }                             from "react-router";
import { UserStore }                                from "../Store/UserStore";
import { url_for }                                  from "../Utils/url_generator";
import { DefaultAccessTokenResolver }               from "../Resolvers/AccessToken/DefaultAccessTokenResolver";

const DATA_TOKEN_INVALIDATE: string[] = ["Route error: Invalid token: access token is invalid", "Route error: Invalid token: access token has expired", "expired_token", "Token Expirado"];

@Service()
class AxiosService {
    private _axios: AxiosInstance;
    private callToken: boolean = false;

    constructor() {
        this.createAxios();
    }

    /**
     * Return token store instance
     * @return {TokenStore}
     */
    private getTokenStore(): TokenStore {
        return Container.get(ApplicationStore).getStore(TokenStore);
    }

    /**
     * Return the user store instance
     * @return {UserStore}
     */
    private getUserStore(): UserStore {
        return Container.get(ApplicationStore).getStore(UserStore);
    }

    /**
     * Create the axios object and starts the interceptor for the token inject
     */
    private createAxios() {
        this._axios = Axios.create(CONFIG_CALL_GET_API);
        this.axiosInterceptor();
    }

    /**
     * Injects and verify the access token in the request information.
     */
    private axiosInterceptor() {
        this._axios.interceptors.request.use((config: AxiosRequestConfig): AxiosRequestConfig => {
            // call a new token
            if (this.callToken) {
                if (config.headers["Content-Type"] === "application/x-www-form-urlencoded") config.data = stringify(config.data);
                return config;
            }

            // call deleted api, append token
            const token = this.getTokenStore().getAccessToken();

            switch (config.method) {
                case "post":
                case "put": {
                    config.headers = CONFIG_CALL_POST_API.headers;
                    break;
                }

                case "get": {
                    config.headers = CONFIG_CALL_GET_API.headers;
                    break;
                }
            }

            if (token && config.headers.authorization)
                config.headers.authorization = config.headers.authorization.replace("%s", token.accessToken);

            if (config.headers["Content-Type"] === "application/x-www-form-urlencoded") config.data = stringify(config.data);

            return config;
        }, function (error) {
            // @TODO handle error to report
            // Do something with request error
            console.log("error", error);
        });

        this._axios.interceptors.response.use(response => response, this.rejectResponse);
    }

    /**
     * Reject the response if the user is unauthorized or the token is expired or invalid.
     * @param error
     */
    public rejectResponse = (error: any): any => {
        const response = error.response;

        if (process.env.ENV === "dev") {
            return error; // show error on dev
        }

        if (typeof response === "undefined" || (response.status === 401)) {
            this.getUserStore().setUser(undefined);
            this.getTokenStore().setAccessToken(undefined);
            window.location.href = generatePath(url_for("login-page"));
        }

        return error;
    };

    /**
     * Getter for axios instance
     */
    get axios(): AxiosInstance {
        return this._axios;
    }

    /**
     * Logout the user from the server
     * @param accessToken
     */
    public async logout(accessToken: AccessToken): Promise<AccessToken | false> {
        const params = {
            invalidate_access_token: accessToken.accessToken
        };

        await this.axios.post(API_CONST.POST_LOGOUT, params);

        return false;
    }

    /**
     * Retrieve the access token
     *
     * @param tokenSaml
     */
    public async getAccessToken(usernameValue: string, passwordValue: string): Promise<IAccessToken | false> {
        this.callToken            = true;
        const accessTokenResolver = new DefaultAccessTokenResolver();
        const accessToken         = await accessTokenResolver.getAccessToken(usernameValue, passwordValue, this.axios);
        this.callToken            = false;

        return accessToken;
    }
}

export default AxiosService;
