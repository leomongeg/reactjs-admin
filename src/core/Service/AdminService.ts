import { Inject, Service }              from "typedi";
import { adminSettings, AdminSettings } from "../../config/Admin";
import { IRoute }                       from "../Routes/routes";
import { AdminRouterSchema, BaseAdmin } from "../Admin/Base/BaseAdmin";
import { ApplicationStore }             from "../Store/ApplicationStore";
import { AdminMenuStore }               from "../Store/AdminMenuStore";
import { UserStore }                    from "../Store/UserStore";

@Service()
export class AdminService {
    private readonly admins: AdminSettings[] = adminSettings;

    private readonly adminClasses: BaseAdmin[];

    private initialized: boolean = false;

    @Inject()
    private applicationStore: ApplicationStore;

    constructor() {
        this.adminClasses = [];
    }

    /**
     * Initialize the admin classes and bootstrapping
     */
    initializeAdmins(systemRoutes: IRoute[]) {
        if (!this.applicationStore.getStore(UserStore).getUser()) return;
        if (this.initialized) return;

        this.initInternalAdminRoutes(systemRoutes);
    }

    /**
     * Initialize the internal admin routes for every admin class:
     * - List
     * - Edit
     * - New
     */
    private initInternalAdminRoutes(systemRoutes: IRoute[]): void {
        const userStore = this.applicationStore.getStore(UserStore);
        for (const setting of this.admins) {
            const instance: BaseAdmin = <BaseAdmin>new setting.adminClass(userStore.getUser());
            instance.init();
            instance.internalRoutes.forEach((route: AdminRouterSchema) => {
                if (route.router === undefined && route.onClick === undefined) throw Error(`At least one the following properties "router" or "onClick" should be declared in your admin internal routes class ${instance.constructor.name}`);

                if (route.router !== undefined) {
                    route.router.adminClassInstance = instance;
                    if (userStore.getUser()?.hasRole(route.roles))
                        systemRoutes.push(route.router);
                }
            });
            this.applicationStore.getStore(AdminMenuStore).addMenuRoute(instance);

        }
    }

}