import { serializable } from "serializr";

/**
 * Raw Access Token Definition
 */
export interface IAccessToken {
    accessToken: string;
    accessTokenExpiresAt: string;

    [key: string]: any;
}

class AccessToken {
    @serializable
    public accessToken: string;

    @serializable
    public accessTokenExpiresAt: Date;

    public constructor(accessToken?: any) {
        if (!accessToken)
            return;

        const accessTokenObj = {
            accessToken: accessToken.accessToken
        };

        Object.assign(this, accessTokenObj);
    }
}

export default AccessToken;
