import { observable }                        from "mobx";
import { alias, list, object, serializable } from "serializr";
import { Role }                              from "./Role";

export default class BaseUser {
    @serializable
    @observable
    protected _id: string;

    @serializable(alias("name", true))
    @observable
    protected _name: string;

    @serializable(alias("identification", true))
    @observable
    protected _identification: string;

    @serializable(alias("email", true))
    @observable
    protected _email: string;

    @serializable(alias("password", true))
    @observable
    protected _password?: string;

    @serializable(alias("roles", list(object(Role))))
    protected _roles: Role[] = [];

    // @serializable(alias("image", true))
    @observable
    protected _photo?: any;


    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get identification(): string {
        return this._identification;
    }

    set identification(value: string) {
        this._identification = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get password(): string {
        return this._password || "";
    }

    set password(value: string) {
        this._password = value;
    }

    get roles(): Role[] {
        return this._roles;
    }

    set roles(value: Role[]) {
        this._roles = value;
    }

    get photo(): any {
        return this._photo;
    }

    set photo(value: any) {
        this._photo = value;
    }

    /**
     * Setter
     * @param value
     */
    public setRoles(value: Role[]): this {
        this._roles = value;
        return this;
    }

    /**
     * Getter
     */
    public getRoles(): Role[] {
        return this._roles;
    }

    /**
     * Check if User has role or almost one role in a list of roles
     * @param role
     */
    hasRole(roles: string | string[]): boolean {
        if (roles === "" || roles.length === 0) return true;
        if (!Array.isArray(roles)) {
            roles = [roles];
        }

        return this._roles.some((role: any) => roles.includes(role._name));
    }
}
