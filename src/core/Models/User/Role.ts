import { alias, serializable } from "serializr";

export class Role {

    @serializable(alias("description", true))
    private _description: string;

    @serializable(alias("name", true))
    private _name: string;

    @serializable(alias("displayName", true))
    private _displayName: string;

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get displayName(): string {
        return this._displayName;
    }

    set displayName(value: string) {
        this._displayName = value;
    }
}