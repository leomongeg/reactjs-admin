import { observable } from "mobx";
import BaseStore from "./Base/BaseStore";
import { FilterSchema, IFilterSection } from "../Admin/Base/FilterSchema";
import { WidgetWrapperViewModelType } from "../ViewModel/Base/WidgetWrapperViewModel";

export class FilterStore extends BaseStore {
    public static readonly NAME_STORE: string = "FilterStore";

    @observable
    private _sections: Map<String, IFilterSection> | undefined;

    @observable
    private _filter: any = {};

    /**
     * Store initializer
     */
    protected init() {
        this.needPersistData = false;
    }

    /**
     * Configure fields used in filter using a FormSchema
     * @param filterSchema
     */
    public configureFilterSchema(filterSchema: FilterSchema) {
        this._sections = filterSchema.sections;
    }

    /**
     * Clean storage
     */
    clean() {
        this._filter = {};
    }

    /**
     * Build the object
     */
    buildFilterObject(viewRefs: WidgetWrapperViewModelType[]): any {
        const object = {};
        viewRefs.forEach((widget: WidgetWrapperViewModelType, index: number) => {
            Reflect.set(object, widget.objectKey, widget.getValue());
        });
        // Keep information in Storage
        this._filter = object;

        return object;
    }

    get filter(): any {
        return this._filter;
    }
}