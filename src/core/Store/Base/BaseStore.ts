import { ApplicationStore } from "../ApplicationStore";
import { Error }            from "tslint/lib/error";

export default abstract class BaseStore {
    public static readonly NAME_STORE: string;

    protected needPersistData: boolean;
    private _applicationStore: ApplicationStore;

    /**
     * Getter
     */
    get applicationStore(): ApplicationStore {
        return this._applicationStore;
    }

    /**
     * Setter
     * @param value
     */
    set applicationStore(value: ApplicationStore) {
        this._applicationStore = value;
    }

    /**
     * Init function
     */
    protected init() {}

    public constructor(applicationStore: ApplicationStore) {
        this.applicationStore = applicationStore;
        this.init();

        if (typeof this.needPersistData === "undefined")
            throw `The storage: ${this.constructor.name} need set property this.needPersistData to false / true`;
    }

    /**
     * Return the needs persist data value.
     * @return {boolean}
     */
    public getNeedPersistData(): boolean {
        return this.needPersistData;
    }
}