import BaseStore            from "./Base/BaseStore";
import { observable, toJS } from "mobx";

/**
 * Breadcrumb definition
 */
export interface BreadcrumbItem {
    title: string;
    routeName: string;
    callback?: (item: BreadcrumbItem, navBarStore: NavBarStore) => void;

}

export class NavBarStore extends BaseStore {
    public static readonly NAME_STORE: string = "NavBarStore";

    @observable
    private _title: string = "";

    @observable
    private _items: BreadcrumbItem[];

    /**
     * Store initialize
     */
    protected init() {
        this.needPersistData = false;
        this._items          = [];
    }


    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    /**
     * Get the breadcrumb items
     */
    get items(): BreadcrumbItem[] {
        return this._items;
    }

    /**
     * Sets the breadcrumb items
     *
     * @param value
     */
    set items(value: BreadcrumbItem[]) {
        this._items = value;
    }

    /**
     * Add new breadcrumb item
     *
     * @param item
     */
    public addItem(item: BreadcrumbItem): this {
        if (this.items.length > 1) this.popItem();
        this.items.push(item);
        // this.pageTitle = item.title;
        return this;
    }

    /**
     * Remove the last item.
     */
    public popItem(): BreadcrumbItem | undefined {
        const item = this.items.pop();
        // const current  = this.items[this.items.length - 1];
        // this.pageTitle = current ? current.title : "";

        return toJS(item);
    }

    /**
     * Clean the breadcrumb
     */
    public cleanItems(): this {
        this.items = [];
        return this;
    }
}