import { ConfigurationStore }           from "../ConfigurationStore";
import { TokenStore }                   from "../TokenStore";
import { UserStore }                    from "../UserStore";
import { AdminMenuStore }               from "../AdminMenuStore";
import { PagerTableStore }              from "../PagerTableStore";
import { NavBarStore }                  from "../NavBarStore";
import { CreateStore }                  from "../CreateStore";
import { EditStore }                    from "../EditStore";
import { DefaultSelectDataSourceStore } from "../MultipleSelect/DefaultSelectDataSourceStore";
import LoadStore                        from "../../../Store/LoadStore/LoadStore";
import { ModalStore }                   from "../ModalStore";
import { FilterStore } from "../FilterStore";

export default [
    ConfigurationStore,
    TokenStore,
    UserStore,
    AdminMenuStore,
    PagerTableStore,
    CreateStore,
    EditStore,
    ModalStore,
    NavBarStore,
    DefaultSelectDataSourceStore,
    FilterStore
].concat(LoadStore as any[]);