import BaseStore                               from "./Base/BaseStore";
import { BaseAdmin }                           from "../Admin/Base/BaseAdmin";
import { FormSchema, IFormSection }            from "../Admin/Base/FormSchema";
import { observable }                          from "mobx";
import { WidgetWrapperViewModelType }          from "../ViewModel/Base/WidgetWrapperViewModel";
import { AxiosInstance, AxiosResponse }        from "axios";
import { Container }                           from "typedi";
import AxiosService                            from "../Service/AxiosService";
import { buildRequestOptionsObj, makeRequest } from "../Utils/common";
import { api_url_generator }                   from "../Utils/url_generator";

export enum CRUDType {
    NEW  = "NEW",
    EDIT = "EDIT"
}

export class CreateStore extends BaseStore {
    public static readonly NAME_STORE: string = "CrudStore";

    private adminClass: BaseAdmin;

    private schema: FormSchema;

    @observable
    private _sections: Map<String, IFormSection> | undefined;

    /**
     * Store initializer
     */
    protected init() {
        this.needPersistData = false;
    }

    /**
     * Configure crud store
     */
    configureCrudStore(adminClass: BaseAdmin): void {
        this.adminClass = adminClass;
        this._sections  = adminClass.createSchema.sections;
        this.schema     = adminClass.createSchema;
    }

    /**
     * Clean the store
     */
    clean(): void {
        this._sections = undefined;
    }

    /**
     * Build the object
     */
    async buildObjectAndSave(viewRefs: WidgetWrapperViewModelType[]): Promise<{ status: boolean, errors: string, data: any }> {
        const object = {};
        viewRefs.forEach((widget: WidgetWrapperViewModelType, index: number) => {
            object[widget.objectKey] = widget.getValue();
        });

        return this.handleSave(object);
    }

    get sections(): Map<String, IFormSection> | undefined {
        return this._sections;
    }

    /**
     * Return the axios service instance
     */
    private getAxios(): AxiosInstance {
        return Container.get(AxiosService).axios;
    }

    /**
     * Handle the save event
     */
    private async handleSave(object: any): Promise<{ status: boolean, errors: string, data: any }> {
        if (this.schema.preSave !== undefined)
            this.schema.preSave(object);

        const options  = buildRequestOptionsObj(this.adminClass.API_POST_ITEM, "POST");
        const endpoint = api_url_generator(options.path);
        let requestOptions;
        if (options.preExecute) {
            requestOptions = options.preExecute(options.path, object);
        }

        if (!requestOptions) {
            requestOptions = {
                endpoint: endpoint,
                params  : object
            };
        }

        let response: AxiosResponse | Error | undefined;

        try {
            response = await makeRequest({
                                             endpoint: requestOptions.endpoint || "",
                                             method  : options.method,
                                             params  : requestOptions.params
                                         }, this.getAxios());
        } catch (e) {
            console.error(e);
        }

        if (response === undefined) return {status: false, errors: "Internal server error", data: undefined};
        if (options.postExecute) {
            const customResponse = options.postExecute(response as AxiosResponse | undefined);
            return customResponse;
        }
        if (response instanceof Error) return {status: false, errors: response.message, data: undefined};
        if (response.status < 200 || response.status > 299 || response instanceof Error) {
            return {status: false, errors: response.statusText, data: undefined};
        }
        if (!response.data.status) {
            return {status: false, errors: response.data.error, data: undefined};
        }

        return {
            status: true,
            errors: "",
            data  : response.data.data
        };
    }
}