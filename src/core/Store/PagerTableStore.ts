import BaseStore                               from "./Base/BaseStore";
import { observable }                          from "mobx";
import { BaseAdmin }                           from "../Admin/Base/BaseAdmin";
import AxiosService                            from "../Service/AxiosService";
import { Container }                           from "typedi";
import { AxiosInstance }                       from "axios";
import { api_url_generator }                   from "../Utils/url_generator";
import { buildRequestOptionsObj, makeRequest } from "../Utils/common";

export class PagerTableStore extends BaseStore {
    public static readonly NAME_STORE: string = "PagerTableStore";

    @observable
    private _data: any[] = [];

    @observable
    private _page: number = 0;

    @observable
    private _rowsPerPage: number = 10;

    @observable
    private _totalItems: number = 0;

    @observable
    private _isLoading: boolean = false;

    @observable
    private _filters: any;

    private _rowsPerPageOptions: number[];

    private adminClass: BaseAdmin;

    private _previousPage: { path: string, sections: string[] };

    /**
     * Store initializer
     */
    protected init() {
        this.needPersistData = false;
    }

    /**
     * Configure the pager
     * @param adminClass
     */
    configurePager(adminClass: BaseAdmin): void {
        this.adminClass          = adminClass;
        this._rowsPerPageOptions = this.adminClass.tableSchema.rowsPerPageOptions;
    }

    /**
     * Clean the pager
     */
    clean() {
        this._data               = [];
        this._page               = 0;
        this._rowsPerPageOptions = this.adminClass.tableSchema.rowsPerPageOptions;
        // this._rowsPerPage = 10;
    }

    /**
     * Return the axios service instance
     */
    private getAxios(): AxiosInstance {
        return Container.get(AxiosService).axios;
    }

    /**
     * Fetch the next Page of information
     */
    async fetchPage(page: number = 0) {
        if (this._isLoading) return;
        this._isLoading = true;
        if (page < 0) page = 0;
        this._page = page;

        const options    = buildRequestOptionsObj(this.adminClass.API_GET_ITEMS, "GET");
        const pathParams = {
            take: this._rowsPerPage,
            skip: this.adminClass.tableSchema.paginationStarts === 1 ? this._page + 1 : this._page
        };
        // add filters to parameters
        if (this._filters) {
            Object.assign(pathParams, this._filters);
        }
        const endpoint   = api_url_generator(options.path, pathParams);
        let requestOptions;
        if (options.preExecute) {
            requestOptions = options.preExecute(options.path, pathParams);
        }
        if (!requestOptions) {
            requestOptions = {
                endpoint: endpoint,
                params  : pathParams
            };
        }

        const response = await makeRequest({
                                               endpoint: requestOptions.endpoint || "",
                                               method  : options.method,
                                               params  : requestOptions.params
                                           }, this.getAxios());

        if (response === undefined || response.status != 200) {
            this._isLoading = false;
            this._data      = [];
            return;
        }

        if (this.adminClass.tableSchema.apiResponseHandler === undefined)
            throw Error(`The apiResponseHandler needs to be implemented in ${this.adminClass.constructor.name} class`);

        if (options.postExecute) {
            throw new Error("The postExecute method is not allowed for the List View, use the apiResponseHandler instead");
        }

        this._isLoading = false;
        this._data      = this.adminClass.tableSchema.apiResponseHandler(response, this);
    }


    get data(): any[] {
        return this._data;
    }

    get rowsPerPageOptions(): number[] {
        return this._rowsPerPageOptions;
    }

    get rowsPerPage(): number {
        return this._rowsPerPage;
    }

    set rowsPerPage(value: number) {
        this._rowsPerPage = value;
    }

    get page(): number {
        return this._page;
    }

    get totalItems(): number {
        return this._totalItems;
    }

    set totalItems(value: number) {
        this._totalItems = value;
    }

    get isLoading(): boolean {
        return this._isLoading;
    }

    get filters(): any {
        return this._filters;
    }

    set filters(value: any) {
        this._filters = value;
    }

    public handlePageChange = (page: number) => {
        this._page = page;
        this.fetchPage(page);
    };

    public handleRowsPerPageChange = (rowsPerPage: number) => {
        this.rowsPerPage = rowsPerPage;
        this._page       = 0;
        this.fetchPage();
    };

    /**
     * Remove referenced item
     *
     * @param item
     */
    public removeElementFromList(item: any): void {
        const index = this._data.indexOf(item, 0);
        if (index > -1) {
            this._data.splice(index, 1);
        }
    }

    get previousPage(): any {
        return this._previousPage;
    }


    set previousPage(path: any) {
        this._previousPage = {
            path    : path,
            sections: path.split("/")
        };
    }

    /**
     * Validate if require clean the filters
     *
     * @param currentPath
     */
    public sameComponent(currentPath: string): boolean {
        if (!this._previousPage) return false;
        const sections = currentPath.split("/");
        return (this._previousPage.sections[1] === sections[1]);
    }
}