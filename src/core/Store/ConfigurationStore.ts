import { action, observable } from "mobx";
import { configuration }      from "../mockdata/configuration";
import { deserialize } from "serializr";
import Configuration   from "../Models/Configuration/Configuration";
import BaseStore       from "./Base/BaseStore";
import { persist }            from "mobx-persist";

export class ConfigurationStore extends BaseStore {
    public static readonly NAME_STORE: string = "ConfigurationStore";

    @observable
    private currentRolePage: string;

    @observable
    private currentRouteName: string;

    @persist("object", Configuration)
    @observable
    private configuration: Configuration;

    /**
     * Implementation of the init function
     */
    protected init() {
        this.needPersistData = false;
        const config         = deserialize(Configuration, configuration).setShowHeader(true).setShowFooter(true);

        this.setConfiguration(config);
    }

    /**
     * Get Configuration instance
     * @return {Configuration}
     */
    @action
    public getConfiguration(): Configuration {
        return this.configuration;
    }

    /**
     * Set configuration instance actions
     * @param {Configuration} value
     */
    @action
    public setConfiguration(value: Configuration) {
        this.configuration = value;

        return this;
    }

    /**
     * Get current page role
     * @return {string}
     */
    public getCurrentRolePage(): string {
        return this.currentRolePage || "";
    }

    /**
     * Set current page role
     * @param {string} val
     */
    public setCurrentRolePage(val: string): this {
        this.currentRolePage = val;

        return this;
    }

    /**
     * Gets the current route name
     * @return {string}
     */
    public getCurrentRouteName(): string {
        return this.currentRouteName;
    }

    /**
     * Sets the current route name
     * @param {string} routeName
     * @return {ConfigurationStore}
     */
    public setCurrentRouteName(routeName: string): this {
        this.currentRouteName = routeName;
        return this;
    }
}
