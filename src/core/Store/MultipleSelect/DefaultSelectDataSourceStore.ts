import BaseStore                        from "../Base/BaseStore";
import { AxiosInstance, AxiosResponse } from "axios";
import { Container }                    from "typedi";
import AxiosService                     from "../../Service/AxiosService";
import { clone } from "../../Utils/merge";

export type MultiSelectItem = {
    label: string;
    value: string;
    fullObject: any;
};

export type MultiSelectModel = {
    label: string;
    value: string;
    customData?: { [key: string]: any };
};

export type MultiSelectDefaultDataSourceOptions = {
    apiEndpoint: string;
    method: "post" | "get";
    params: any;
    responseParser: (axiosResponse: AxiosResponse) => any[];
    search: "local" | "remote";
    remoteParameter: string;
};

/**
 * Multiple Select Store/Data Interface
 */
export interface IMultiSelectDataSourceStore extends IDefaultMultiSelectStore {
    /**
     * Return the data fetched from the remote service.
     *
     * @param inputValue the user input text info to filter data
     */
    getItems(inputValue?: string): MultiSelectItem[];

    /**
     * Fetch the items for the input
     *
     * @param inputValue
     * @param model
     */
    fetchItems(inputValue: string, model?: MultiSelectModel): Promise<void>;

    /**
     * Clean the store data when the method is called. Be nice with the performance!
     */
    cleanData(): void;
}

/**
 * Test
 */
export interface IDefaultMultiSelectStore {
    /**
     * Fetch the items for the input using the default data source.
     *
     * @param inputValue
     * @param model
     * @param dataSourceOptions
     */
    fetchItemsUsingDefaultDataSource?(inputValue: string, model?: MultiSelectModel, dataSourceOptions?: MultiSelectDefaultDataSourceOptions): Promise<void>;
}

export class DefaultSelectDataSourceStore extends BaseStore implements IMultiSelectDataSourceStore {
    public static readonly NAME_STORE: string = "DefaultSelectStore";

    private selectItems: MultiSelectItem[] = [];

    private data: any[] = [];

    /**
     * Store initializer
     */
    protected init() {
        this.needPersistData = false;
    }

    /**
     * Return the default Axios Instance.
     *
     * @private
     */
    private getAxios(): AxiosInstance {
        return Container.get(AxiosService).axios;
    }

    /**
     * Fetch the items for the input using the default data source.
     *
     * @param inputValue
     * @param model
     * @param dataSourceOptions
     */
    public async fetchItemsUsingDefaultDataSource(inputValue: string, model?: MultiSelectModel, dataSourceOptions?: MultiSelectDefaultDataSourceOptions): Promise<void> {
        if (!model) throw new Error("The Property Model is required when using SelectMultipleField with dataSourceStore option");
        if (!(dataSourceOptions)) throw new Error("The Property dataSourceOptions is required when using SelectMultipleField with DefaultSelectDataSourceStore option");
        let response;
        try {
            const params = clone(dataSourceOptions?.params);
            if (dataSourceOptions.search == "remote" && inputValue.trim().length >= 3) {
                // get the remote parameter (q) by default
                const remoteParameter = (dataSourceOptions.remoteParameter === undefined ||
                    (dataSourceOptions.remoteParameter as string).trim() === "")
                    ? "q" : dataSourceOptions.remoteParameter;
                // add parameter to query
                params[remoteParameter] = inputValue.trim();
            }

            if (dataSourceOptions?.method === "get") {
                response = await this.getAxios().get(dataSourceOptions?.apiEndpoint, {
                    params: params
                });

            } else {
                response = await this.getAxios().post(dataSourceOptions?.apiEndpoint, params);
            }
        } catch (e) {
            console.error(e);
        }

        if (response === undefined) {
            this.data        = [];
            this.selectItems = [];
            return;
        }

        this.data = dataSourceOptions.responseParser(response);

        if (dataSourceOptions.search == "local") {
            if (inputValue.replace(/\W/g, "").length > 0) {
                this.selectItems = (this.data.map(obj => {
                    return {label: obj[model.label], value: obj[model.value], fullObject: obj};
                })).filter(item => item.label.toLowerCase().includes(inputValue.toLowerCase()));
                return;
            }
        }

        this.selectItems = this.data;
    }

    /**
     * Fetch the items for the input
     * @param inputValue
     */
    public async fetchItems(inputValue: string, model?: MultiSelectModel): Promise<void> {
        if (!model) throw new Error("The Property Model is required when using SelectMultipleField with dataSourceStore option");
        return new Promise(resolve => {
            resolve();
        });
    }

    /**
     * Return the data fetched from the remote service.
     */
    public getItems(): any[] {
        return this.selectItems;
    }

    /**
     * Clean the store data
     */
    public cleanData() {
        this.data        = [];
        this.selectItems = [];
    }


}