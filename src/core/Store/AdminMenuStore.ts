import BaseStore                        from "./Base/BaseStore";
import { AdminRouterSchema, BaseAdmin } from "../Admin/Base/BaseAdmin";
import { History }   from "history";
import { url_for }   from "../Utils/url_generator";
import { UserStore } from "./UserStore";


/**
 * Define the list of menu groups
 */
export interface IMenuGroups {
    [key: string]: any[];
}

export class AdminMenuStore extends BaseStore {

    private _groups: IMenuGroups = {};

    /**
     * Store initialization
     */
    protected init() {
        this.needPersistData = false;
    }


    get groups(): IMenuGroups {
        return this._groups;
    }

    /**
     * Add new menu item to the group
     * @param admin
     */
    addMenuRoute(admin: BaseAdmin): this {
        if (!this._groups[admin.menuGroup]) {
            this._groups[admin.menuGroup] = [];
        }
        const userStore = this.applicationStore.getStore(UserStore);

        admin.internalRoutes.forEach((route: AdminRouterSchema) => {
            const groupKey = route.mainMenuGroup ? route.mainMenuGroup : admin.menuGroup;
            if (!this._groups[groupKey]) {
                this._groups[groupKey] = [];
            }

            if (route.disable || !route.showInMainMenu || !userStore.getUser()?.hasRole(route.roles)) return;

            this._groups[groupKey].push({
                                            route      : route,
                                            handleClick: this.handleMenuItemNavigation
                                        });
        });


        return this;
    }

    private handleMenuItemNavigation = (history: History, route: AdminRouterSchema) => {
        if (route.onClick !== undefined) {
            route.onClick(history);
            return;
        }

        if (route.router !== undefined) {
            history.push(url_for(route.router.name));
            return;
        }

        throw Error(`[handleMenuItemNavigation] no defined "router" or "onClick" event to handle the main menu navigation`);
    };
}