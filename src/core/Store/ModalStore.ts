import BaseStore                   from "./Base/BaseStore";
import { observable }              from "mobx";
import { ReactElement, ReactNode } from "react";

export enum ModalType {
    INFO    = "INFO", // Display only one button, to accept the info acknowledge.
    CONFIRM = "CONFIRM", // Display two buttons, primary and secondary.
    ERROR   = "ERROR", // Display only one button, to accept the error acknowledge.
    CUSTOM  = "CUSTOM", // @TODO: Display custom modal.
}

/**
 * Media Details Interface
 */
export interface ModalMedia {
    mediaType: string;
    src: string;
    description?: string;
}

/**
 * Modal definition
 */
export interface ModalOptions {
    // icon?: string | ReactNode;
    title?: string;
    body?: string | ReactNode | ReactElement;
    type: ModalType;
    hideDefaultCloseCTA?: boolean;
    ctaPrimaryText?: string;
    ctaSecondaryText?: string;
    ctaPrimaryClickHandler?: () => void;
    onCancelClickHandler?: () => void;
}

export class ModalStore extends BaseStore {
    public static readonly NAME_STORE: string = "ModalStore";

    @observable
    private _showModal: boolean = false;

    @observable
    private _modalOptions: ModalOptions | undefined;

    private primaryOnClickHandler: (modalStore: ModalStore) => void | undefined;

    private secondaryOrCancelClickHandler: () => void | undefined;

    @observable
    private _primaryButtonLoading: boolean;

    /**
     * Initialize the store
     */
    protected init() {
        this.needPersistData = false;
    }

    /**
     * Configure the modal display
     *
     * @return {ModalStore}
     */
    public configureModal(options: ModalOptions | undefined): this {
        this._primaryButtonLoading = false;
        this._modalOptions         = options;
        return this;
    }

    /**
     * Add the primary click event handler.
     *
     * @param handler
     */
    public addPrimaryClickHandler(handler: (modalStore: ModalStore) => void): this {
        this.primaryOnClickHandler = handler;
        return this;
    }

    /**
     * Add the secondary or cancel click event handler.
     *
     * @param handler
     */
    public addSecondaryOrCancelHandler(handler: () => void): this {
        this.secondaryOrCancelClickHandler = handler;
        return this;
    }

    public primaryEventHandler = () => {
        this._primaryButtonLoading = true;
        if (this.primaryOnClickHandler !== undefined) this.primaryOnClickHandler(this);
    };

    public secondaryOrCancelEventHandler = () => {
        if (this.secondaryOrCancelClickHandler !== undefined) this.secondaryOrCancelClickHandler();
        this.doHideModal();
    };

    /**
     * Show the modal component.
     *
     * @return {ModalStore}
     */
    public doShowModal(): this {
        this._showModal = true;
        return this;
    }

    /**
     * Hide the modal component.
     *
     * @return {ModalStore}
     */
    public doHideModal(): this {
        this._showModal            = false;
        this._primaryButtonLoading = false;
        setTimeout(() => { this.configureModal(undefined); }, 500);
        return this;
    }

    get showModal(): boolean {
        return this._showModal;
    }

    get modalOptions(): ModalOptions | undefined {
        return this._modalOptions;
    }

    get primaryButtonLoading(): boolean {
        return this._primaryButtonLoading;
    }
}