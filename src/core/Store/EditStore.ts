import BaseStore                               from "./Base/BaseStore";
import { BaseAdmin, CRUDRoute }                from "../Admin/Base/BaseAdmin";
import { FormSchema, IFormSection }            from "../Admin/Base/FormSchema";
import { observable }                          from "mobx";
import { WidgetWrapperViewModelType }          from "../ViewModel/Base/WidgetWrapperViewModel";
import { AxiosInstance, AxiosResponse }        from "axios";
import { Container }                           from "typedi";
import AxiosService                            from "../Service/AxiosService";
import { api_url_generator }                   from "../Utils/url_generator";
import { buildRequestOptionsObj, makeRequest } from "../Utils/common";

export class EditStore extends BaseStore {
    public static readonly NAME_STORE: string = "EditStore";

    private adminClass: BaseAdmin;

    private schema: FormSchema;

    @observable
    private _sections: Map<String, IFormSection> | undefined;

    private _entityId: string;

    /**
     * Init the Store
     */
    protected init() {
        this.needPersistData = false;
    }

    /**
     * Configure crud store
     */
    configureStore(adminClass: BaseAdmin): void {
        this.clean();
        this.adminClass = adminClass;
        this._sections  = new Map<String, IFormSection>();
        this.schema     = adminClass.editSchema;
    }

    /**
     * Clean the store
     */
    clean(): void {
        this._sections = undefined;
        this.entityId  = "";
        this.schema?.clean();
    }

    /**
     * Build the object
     */
    async buildObjectAndSave(viewRefs: WidgetWrapperViewModelType[]): Promise<{ status: boolean, errors: string, data: any }> {
        const object = {};
        viewRefs.forEach((widget: WidgetWrapperViewModelType, index: number) => {
            object[widget.objectKey] = widget.getValue();
        });

        return this.handleSave(object);
    }

    get sections(): Map<String, IFormSection> | undefined {
        return this._sections;
    }

    get entityId(): string {
        return this._entityId;
    }

    set entityId(value: string) {
        this._entityId = value;
    }

    /**
     * Return the axios service instance
     */
    private getAxios(): AxiosInstance {
        return Container.get(AxiosService).axios;
    }

    /**
     * Fetch the subject entity for the edit process
     */
    public async fetchSubject(): Promise<{ status: boolean, errors: string }> {
        const options                               = buildRequestOptionsObj(this.adminClass.API_GET_ITEM, "GET");
        const pathParams                            = {};
        pathParams[this.schema.entityIdentifierKey] = this.entityId;
        const endpoint                              = api_url_generator(options.path, pathParams);
        let requestOptions;
        if (options.preExecute) {
            requestOptions = options.preExecute(options.path, pathParams);
        }

        if (!requestOptions) {
            requestOptions = {
                endpoint: endpoint,
                params  : {}
            };
        }

        let response;

        try {
            response = await makeRequest({
                                             endpoint: requestOptions.endpoint || "",
                                             method  : options.method,
                                             params  : requestOptions.params
                                         }, this.getAxios());
            console.log(response);
        } catch (e) {
            console.error(e);
        }

        if (response === undefined) return {status: false, errors: "Internal server error"};

        if (response.status < 200 || response.status > 299) {
            return {status: false, errors: response.statusText};
        }

        if (!response.data.status) {
            return {status: false, errors: response.data.error};
        }

        let customResponse;
        if (options.postExecute) {
            customResponse = options.postExecute(response);
            if (!customResponse.status) {
                return customResponse;
            }
        }

        await this.adminClass.buildEditFormView(customResponse ? customResponse.data : response.data.data);
        this._sections = this.adminClass.editSchema.sections;

        return {
            status: true,
            errors: ""
        };
    }

    /**
     * Handle the save event
     */
    private async handleSave(object: any): Promise<{ status: boolean, errors: string, data: any }> {
        const options                               = buildRequestOptionsObj(this.adminClass.API_PUT_ITEM, "PUT");
        const pathParams                            = {};
        pathParams[this.schema.entityIdentifierKey] = this.entityId;
        const endpoint                              = api_url_generator(options.path, pathParams);
        let requestOptions;
        if (options.preExecute) {
            requestOptions = options.preExecute(options.path, pathParams);
        }

        object[this.schema.entityIdentifierKey] = this.entityId;

        if (this.schema.preSave !== undefined)
            this.schema.preSave(object);

        if (!requestOptions) {
            requestOptions = {
                endpoint: endpoint,
                params  : object
            };
        }

        let response: AxiosResponse | Error | undefined;

        try {
            response = await makeRequest({
                                             endpoint: requestOptions.endpoint || "",
                                             method  : options.method,
                                             params  : requestOptions.params
                                         }, this.getAxios());
        } catch (e) {
            console.error(e);
        }

        if (response === undefined) return {status: false, errors: "Internal server error", data: undefined};
        if (options.postExecute) {
            const customResponse = options.postExecute(response as AxiosResponse | undefined);
            return customResponse;
        }
        if (response instanceof Error) return {status: false, errors: response.message, data: undefined};
        if (response.status < 200 || response.status > 299 || response instanceof Error) {
            return {status: false, errors: response.statusText, data: undefined};
        }
        if (!response.data.status) {
            return {status: false, errors: response.data.error, data: undefined};
        }

        return {
            status: true,
            errors: "",
            data  : response.data.data
        };
    }

    /**
     * Handle delete element
     *
     * @param entityIdentifier
     * @param modelObject
     */
    public async handleDeleteElement(options: CRUDRoute | string, entityIdentifier: string, modelObject: any) {
        options = buildRequestOptionsObj(options, "DELETE");

        const params             = {};
        params[entityIdentifier] = modelObject[entityIdentifier];
        const endpoint           = api_url_generator(options.path, params);
        let requestOptions;
        if (options.preExecute) {
            requestOptions = options.preExecute(options.path, modelObject);
        }

        if (!requestOptions) {
            requestOptions = {
                endpoint: endpoint,
                params  : {}
            };
        }

        const response = await makeRequest({
                                               endpoint: requestOptions.endpoint || "",
                                               method  : options.method,
                                               params  : requestOptions.params
                                           }, this.getAxios());
        if (options.postExecute) {
            const {data} = options.postExecute(response);
        }
    }
}