import { observable }                      from "mobx";
import { persist }                         from "mobx-persist";
import BaseStore                           from "./Base/BaseStore";
import AjaxService                         from "../Service/AjaxService";
import Container                           from "typedi";
import { date, deserialize, serializable } from "serializr";
import { TokenStore }                      from "./TokenStore";
import JwtDecode                           from "jwt-decode";
import User                                from "../../Model/User/User";

const moment = require("moment");

export class UserStore extends BaseStore {
    public static readonly NAME_STORE: string = "UserStore";

    @persist("object", User)
    @observable
    private user: User | undefined;

    @persist("object")
    @serializable(date())
    private previousCallAccess: Date;

    @observable
    private _password: string = "";

    @observable
    private _username: string = "";

    private callMe: boolean = false;

    /**
     * Implementation of the init function
     */
    protected init() {
        this.needPersistData = true;
    }

    /**
     * Ajax Service instance
     * @return {AjaxService}
     */
    public getAjaxService(): AjaxService {
        return Container.get(AjaxService);
    }

    /**
     * Previous access
     */
    public getPreviousAccess(): Date {
        return this.previousCallAccess;
    }

    /**
     * setter previous access
     *
     * @param currentDate
     */
    public setPreviousAccess(currentDate: Date) {
        this.previousCallAccess = currentDate;
    }

    /**
     * set user
     *
     * @param user
     */
    public setUser(user: User | undefined): this {
        this.user = user;

        return this;
    }

    /**
     * Return the token store instance
     * @return {TokenStore}
     */
    private getTokenStore(): TokenStore {
        return this.applicationStore.getStore(TokenStore);
    }

    /**
     * get user
     */
    public getUser(): User | undefined {
        const now = moment().add(2, "h");

        // if (!this.getPreviousAccess() || now.isBefore(moment(this.getPreviousAccess()))) {
        //     const token = this.getTokenStore().getAccessToken();
        //     if (!token) return undefined;
        //     const payload: any = JwtDecode(token.accessToken);
        //     this.getUserMeApi(payload.id);
        // }

        return this.user;
    }

    /**
     * Bind the authenticated user by access token to the UserStore to identified the current user.
     */
    public async bindSignedInUser(): Promise<boolean> {
        if (!this.applicationStore.getStore(TokenStore).getAccessToken()) return true; // Prevent if the app don't have
                                                                                       // accessToken to call api.me
        if (this.callMe) return true; // Prevent call unlimited times api.me

        this.callMe    = true;
        const response = await this.getAjaxService().getMe();

        if (!response.data.status) {
            return false;
        }

        const user: User = deserialize(User, response.data.data);

        this.setUser(user)
            .setPreviousAccess(moment());

        this.callMe = false;

        return true;
    }

    /**
     * Getter
     */
    get password(): string {
        return this._password;
    }

    /**
     * Setter
     *
     * @param value
     */
    set password(value: string) {
        this._password = value;
    }

    /**
     * Getter
     */
    get username(): string {
        return this._username;
    }

    /**
     * Setter
     *
     * @param value
     */
    set username(value: string) {
        this._username = value;
    }
}