import React, { Component, ReactNode } from "react";
import { BaseViewModel }               from "../core/ViewModel/Base/BaseViewModel";
import AcmeCustomView                  from "../View/Pages/AcmeCustomView";
import { NavBarStore }                 from "../core/Store/NavBarStore";

class AcmeCustomViewModel extends BaseViewModel<any, any> {

    /**
     * React Life Cycle Event
     */
    componentDidMount() {
        this.getStore(NavBarStore).title = this.props.adminClassInstance.title;
    }

    /**
     * ViewModel View Render
     */
    render(): ReactNode {
        return (<AcmeCustomView/>);
    }
}

export default AcmeCustomViewModel;