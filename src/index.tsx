import "reflect-metadata";
import "./index.scss";
import * as React               from "react";
import * as ReactDOM            from "react-dom";
import { BrowserRouter }        from "react-router-dom";
import { I18nextProvider }      from "react-i18next";
import { Provider }             from "mobx-react";
import { Container } from "typedi";
import Layout        from "./core/Views/Components/Layout/Layout";
import i18next       from "./core/Utils/i18n/i18n";
import { ApplicationStore } from "./core/Store/ApplicationStore";
import { ErrorBoundaryHandler } from "./core/Routes/ErrorBoundaryHandler";

const applicationStore = Container.get(ApplicationStore);

(async () => {
    await applicationStore.initStorage();

    const app = (
        <ErrorBoundaryHandler>
            <Provider {...applicationStore.getStores()}>
                <I18nextProvider i18n={i18next}>
                    <BrowserRouter>
                        <Layout/>
                    </BrowserRouter>
                </I18nextProvider>
            </Provider>
        </ErrorBoundaryHandler>
    );

    ReactDOM.render(app, document.getElementById("root-react-admin") as HTMLElement);
})();

ReactDOM.render((<div> Loading... </div>), document.getElementById("root-react-admin") as HTMLElement);