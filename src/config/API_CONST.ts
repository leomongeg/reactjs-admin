const prefix = `api`;

export default {
    GET_ME               : `${prefix}/me`,
    POST_ACCESS_TOKEN    : `/oauth/token`,
    POST_CHANGE_PASS     : ``,
    POST_FORGOT_PASS     : ``,
    POST_LOGOUT          : `/auth/logout`,
};
