/**
 * Define the roles available.
 * The ADMIN Role define the minimum role required by the user to login in the React Admin App.
 */
export default {
    ADMIN: "admin"
};

