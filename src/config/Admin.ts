import { AcmeAdmin }                  from "../Admin/AcmeAdmin";
import { UserAdmin }                  from "../Admin/UserAdmin";
import { NotificationAdmin }          from "../Admin/NotificationAdmin";
import { CMSAdmin }                   from "../Admin/CMSAdmin";
import { DefaultAccessTokenResolver } from "../core/Resolvers/AccessToken/DefaultAccessTokenResolver";

/**
 * Admin Settings
 */
export interface AdminSettings {
    adminClass: any;
}


export const adminSettings: AdminSettings[] = [
    {
        adminClass: NotificationAdmin,
    },
    {
        adminClass: CMSAdmin
    },
    {
        adminClass: AcmeAdmin
    },
    {
        adminClass: UserAdmin
    }
];

/**
 * Define your custom behavior here overriding the defined class and implementing the related interface
 * as the documentations describes.
 */
export const CustomResolvers: any = {

    AccessTokenResolver: DefaultAccessTokenResolver
};