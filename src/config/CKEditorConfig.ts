export const defaultConfig = {

    toolbar: {
        items: [
            "heading",
            "|",
            "bold",
            "italic",
            "link",
            "bulletedList",
            "numberedList",
            "|",
            "alignment",
            "indent",
            "outdent",
            "|",
            "imageUpload",
            "blockQuote",
            "insertTable",
            "mediaEmbed",
            "undo",
            "redo"
        ]
    },

    // https://ckeditor.com/docs/ckeditor5/latest/features/image.html
    image: {
        toolbar: [
            "imageStyle:alignLeft", "imageStyle:alignCenter", "imageStyle:alignRight",
            "|",
            "imageResize",
            "|",
            "imageTextAlternative",
            "|",
            "linkImage"
        ],

        upload: {
            panel: {
                items: ["insertImageViaUrl"]
            }
        },

        resizeOptions: [
            {
                name : "imageResize:original",
                value: undefined,
                label: "Original"
            },
            {
                name : "imageResize:50",
                value: "50",
                label: "50%"
            },
            {
                name : "imageResize:75",
                value: "75",
                label: "75%"
            }
        ],
        // The default value.
        styles       : [
            "full",
            "alignLeft", "alignCenter", "alignRight"
        ]
    },

    // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html
    heading: {
        options: [
            {model: "paragraph", title: "Paragraph", class: "ck-heading_paragraph"},
            {model: "heading2", view: "h2", title: "Heading 2", class: "ck-heading_heading2"},
            {model: "heading3", view: "h3", title: "Heading 3", class: "ck-heading_heading3"},
            {model: "heading4", view: "h4", title: "Heading 4", class: "ck-heading_heading4"},
            {model: "heading5", view: "h5", title: "Heading 5", class: "ck-heading_heading5"},
        ]
    }
};