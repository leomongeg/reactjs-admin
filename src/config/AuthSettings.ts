/**
 * This configuration file contains all the configuration settings for the login form and login process:
 *  - ADMIN_ROLE: The base role required for the users to login into the Admin Application.
 *  - USER_INPUT_FORM_VALIDATIONS: Form validations for the form username input.
 *  - PASSWORD_INPUT_FORM_VALIDATIONS: Form validations for the form password input.
 *  - AUTH_USER_NAME_KEY: Your API username key to send in the request.
 *  - AUTH_USER_PASS_KEY: Your API password key to send in the request.
 *
 *  For more options in the form validations please see https://www.npmjs.com/package/simple-react-validator#rules
 */
export default {
    ADMIN_ROLE                     : "admin",
    USER_INPUT_FORM_VALIDATIONS    : "min:8",
    PASSWORD_INPUT_FORM_VALIDATIONS: "min:5",
    AUTH_USER_NAME_KEY             : "identification",
    AUTH_USER_PASS_KEY             : "password"
};