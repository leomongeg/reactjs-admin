import HomeViewModel  from "../core/ViewModel/HomeViewModel";
import LoginViewModel from "../core/ViewModel/LoginViewModel";

export default {
    /**
     * Override the default Dashboard home page view model.
     */
    DashboardPageViewModel: HomeViewModel,

    /**
     * Override the default Login View Model page.
     */
    LoginViewModel: LoginViewModel
};