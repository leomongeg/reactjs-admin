import React, { Component, ReactNode } from "react";

class AcmeCustomView extends Component<any, any> {

    /**
     * React View Render
     */
    render(): ReactNode {
        return (<>
            <h1>Hello World</h1>
        </>);
    }
}

export default AcmeCustomView;