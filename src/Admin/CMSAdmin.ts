import { BaseAdmin }                  from "../core/Admin/Base/BaseAdmin";
import { FormSchema, FormWidgetType } from "../core/Admin/Base/FormSchema";
import { TableSchema }                from "../core/Admin/Base/TableSchema";
import { AxiosResponse }              from "axios";
import { PagerTableStore }            from "../core/Store/PagerTableStore";
import { Container }                  from "typedi";
import { ApplicationStore }           from "../core/Store/ApplicationStore";
import { TokenStore }                 from "../core/Store/TokenStore";
import {
    DefaultSelectDataSourceStore,
    MultiSelectItem
}                                     from "../core/Store/MultipleSelect/DefaultSelectDataSourceStore";
import { api_url_generator }          from "../core/Utils/url_generator";
import { FilterSchema }               from "../core/Admin/Base/FilterSchema";

export class CMSAdmin extends BaseAdmin {
    /**
     * Configure the filters shown in the list view
     * @param filters
     */
    configureFilters(filters: FilterSchema): void {
        filters.add({
                        key         : "title",
                        type        : FormWidgetType.TYPE_TEXT,
                        label       : "Titulo",
                        initialValue: "",
                        elementProps: {variant: "outlined"},
                        helperText  : "Define the title of your post entry",
                        onChange    : (value: any) => {
                            console.log(`Value changed ${value}`);
                        }
                    }).add({
                               key          : "categories",
                               type         : FormWidgetType.TYPE_SELECT_MULTIPLE,
                               label        : "Categories",
                               initialValue : [],
                               elementProps : {
                                   isMulti          : false,
                                   // staticData: [{label: "test1", value: "test1", fullObject: {label: "test1", value:
                                   // "test1"}}, {label: "test2", value: "test2", fullObject: {label: "test2", name:
                                   // "test2", value: "test2"}}],
                                   dataSourceStore  : Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore),
                                   dataSourceOptions: {
                                       apiEndpoint   : `${process.env.REACT_APP_ENDPOINT_BACKEND}microservice-cms/api/category/list`,
                                       method        : "get",
                                       params        : {take: 10, skip: 1, type: "tip"},
                                       search        : "remote",
                                       responseParser: (axiosResponse: AxiosResponse) => {
                                           return axiosResponse.data.data.items;
                                       }
                                   },
                                   model            : {label: "name", value: "_id"},
                               }, helperText: "Select the categories",
                           });
    }

    /**
     * Configure create view
     *
     * @param formSchema
     */
    configureCreateView(formSchema: FormSchema): void {
        formSchema.add({
                           key         : "title",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Titulo",
                           validations : "required",
                           initialValue: "",
                           elementProps: {variant: "outlined"},
                           helperText  : "Define the title of your post entry",
                           onChange    : (value: any) => {
                               console.log(`Value changed ${value}`);
                           }
                       })
                  .add({
                           key         : "slugEditable",
                           type        : FormWidgetType.TYPE_BOOLEAN,
                           label       : "Slug Editable",
                           initialValue: true,
                           elementProps: {variant: "outlined"},
                           helperText  : "The slug value should be unique, if the system detects repeated slug try to assign other, other wise an error be throw.",
                       })
                  .add({
                           key         : "content",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Content",
                           validations : "required",
                           initialValue: "",
                           helperText  : "Add the content for your post entry",
                           elementProps: {variant: "outlined", multiline: true}
                       })
                  .add({
                           key         : "contentType",
                           type        : FormWidgetType.TYPE_SELECT,
                           label       : "Content Type",
                           validations : "required",
                           initialValue: "article",
                           helperText  : "Select the content type",
                           elementProps: {
                               options: [
                                   {value: "article", displayText: "Article"},
                                   {value: "tips", displayText: "Tips"}
                               ]
                           }
                       })
                  .add({
                           key          : "categories",
                           type         : FormWidgetType.TYPE_SELECT_MULTIPLE,
                           label        : "Categories",
                           validations  : "required",
                           initialValue : [],
                           elementProps : {
                               isMulti          : true,
                               dataSourceStore  : Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore),
                               dataSourceOptions: {
                                   apiEndpoint:
                                       "https://c4e7ec30-b929-4913-a9ec-4b30c5a57e5a.mock.pstmn.io/microservice-cms/api/category/list?take=10&skip=1",
                                   method     : "get", params: {}, responseParser: (axiosResponse: AxiosResponse) => {
                                       return axiosResponse.data.data.items;
                                   }
                               }, model         : {label: "name", value: "_id"},
                           }, helperText:
                          "Select the categories",
                       })
                  .add({
                           key         : "contentText",
                           type        : FormWidgetType.TYPE_WYSIWYG,
                           label       : "Content",
                           validations : "required",
                           initialValue: "",
                           elementProps: {
                               placeholder                 : "Add your text here!",
                               onProcessImageUploadResponse: (response: any) => {
                                   if (!response || !response.status) {
                                       return "Error trying to upload your image.";
                                   }
                                   return {default: response.data.secure_url};
                               },
                               onInitImageUploadRequest    : async (xhr: XMLHttpRequest) => {
                                   xhr.open("POST", "https://gateway-toyotacr.devdoubledigit.com/file-handler/file-upload", true);

                                   // change "Authorization" header with your header
                                   xhr.setRequestHeader("authorization", `Bearer ${Container.get(ApplicationStore).getStore(TokenStore).getAccessToken()?.accessToken}`);
                                   xhr.responseType = "json";
                               }, onProcessBodyImageUpload : async (data: FormData) => {
                                   data.append("namespace", "cms");
                                   data.append("subPath", "ckeditor");
                               }
                           },
                           helperText  : "Create your content",
                       })
                  .add({
                           key         : "repeater",
                           type        : FormWidgetType.TYPE_REPEATER,
                           label       : "Content",
                           validations : undefined,
                           initialValue: [],
                           // initialValue: [{
                           //     title: "prueba", categories: [
                           //         {_id: "prueba", name: "Prueba"}
                           //     ]
                           // }],
                           elementProps: {
                               allowEmpty           : false,
                               confirmDeleteItem    : true,
                               maxEmbeds            : 4,
                               buildRepeatableFields: (schema: FormSchema) => {
                                   let categories = [];
                                   if (schema.subject && schema.subject.categories) {
                                       categories = schema.subject.categories.map((category: any) => {
                                           return {
                                               fullObject: category,
                                               label     : category.name,
                                               value     : category._id
                                           };
                                       });
                                   }
                                   schema
                                       .add({
                                                key         : "title",
                                                type        : FormWidgetType.TYPE_TEXT,
                                                label       : "Titulo",
                                                validations : "required",
                                                initialValue: "",
                                                elementProps: {variant: "outlined"},
                                                helperText  : "Define the title of your post entry",
                                                onChange    : (value: any) => {
                                                    console.log(`Value changed ${value}`);
                                                }
                                            }).add({
                                                       key          : "categories",
                                                       type         : FormWidgetType.TYPE_SELECT_MULTIPLE,
                                                       label        : "Categories",
                                                       validations  : "required",
                                                       initialValue : categories,
                                                       elementProps : {
                                                           isMulti          : true,
                                                           dataSourceStore  : Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore),
                                                           dataSourceOptions: {
                                                               apiEndpoint   : `${process.env.REACT_APP_ENDPOINT_BACKEND}microservice-cms/api/category/list`,
                                                               method        : "get",
                                                               params        : {take: 10, skip: 1, type: "tip"},
                                                               search        : "remote",
                                                               responseParser: (axiosResponse: AxiosResponse) => {
                                                                   return axiosResponse.data.data.items;
                                                               }
                                                           },
                                                           model            : {label: "name", value: "_id"},
                                                       }, helperText: "Select the categories",
                                                   });
                               }
                           },
                           helperText  : "Create your repeatable content here, adding or removing items, according to your needs.",
                       })
            .preSave = (data: any) => {
            console.log(data);
        };
    }

    /**
     * Demo to load the subject categories
     *
     * @private
     */
    private async loadSubjectCategories(): Promise<MultiSelectItem[]> {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve([{
                    label     : "Accessorios", value: "5f344d0d35391d0e844f41d9",
                    fullObject: {
                        name: "Accessorios", _id: "5f344d0d35391d0e844f41d9"
                    }
                }]);
            }, 500);
        });
    }

    /**
     * Configure edit view
     *
     * @param formSchema
     */
    async configureEditView(formSchema: FormSchema, subject: any) {

        console.log("Configure Edit wait");
        const subjectCategories = await this.loadSubjectCategories();
        console.log("Configure Edit ready");

        formSchema.add({
                           key         : "title",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Title",
                           validations : "required",
                           initialValue: "",
                           elementProps: {variant: "outlined"},
                           helperText  : "Define the title of your post entry",
                           onChange    : (value: any) => {
                               console.log(`Value changed ${value}`);
                           }
                       })
                  .add({
                           key         : "slugEditable",
                           type        : FormWidgetType.TYPE_BOOLEAN,
                           label       : "Slug Editable",
                           initialValue: true,
                           elementProps: {variant: "outlined"},
                           helperText  : "The slug value should be unique, if the system detects repeated slug try to assign other, other wise an error be throw.",
                       })
                  .add({
                           key         : "contentType",
                           type        : FormWidgetType.TYPE_SELECT,
                           label       : "Content Type",
                           validations : "required",
                           initialValue: "article",
                           helperText  : "Select the content type",
                           elementProps: {
                               options: [{value: "article", displayText: "Article"}]
                           }
                       })
                  .add({
                           key         : "content",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Content",
                           validations : "required",
                           initialValue: "",
                           helperText  : "Add the content for your post entry",
                           elementProps: {variant: "outlined", multiline: true}
                       })
                  .add({
                           key         : "categories",
                           type        : FormWidgetType.TYPE_SELECT_MULTIPLE,
                           label       : "Categories",
                           initialValue: subjectCategories,
                           elementProps: {
                               isMulti          : true,
                               dataSourceStore  : Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore),
                               dataSourceOptions: {
                                   apiEndpoint   : "https://c4e7ec30-b929-4913-a9ec-4b30c5a57e5a.mock.pstmn.io/microservice-cms/api/category/list?take=10&skip=1",
                                   method        : "get",
                                   params        : {},
                                   responseParser: (axiosResponse: AxiosResponse) => {
                                       return axiosResponse.data.data.items;
                                   }
                               },
                               model            : {label: "name", value: "_id"},
                           },
                           helperText  : "Select the categories",
                       })
            .preSave               = (data: any) => {
            console.log(data);
            data.slug = subject.slug;
        };
        formSchema.subjectTitleKey = "title";
    }

    /**
     * Configure list view
     *
     * @param tableSchema
     */
    configureListView(tableSchema: TableSchema): void {
        tableSchema
            .add({
                     key  : "_id",
                     label: "id"
                 })
            .add({
                     key  : "title",
                     label: "Title"
                 })
            .add({
                     key  : "slug",
                     label: "Slug"
                 })
            .add({
                     key  : "type",
                     label: "Content Type"
                 })
            .apiResponseHandler = this.listResponseHandler
        ;
    }

    /**
     * Initialize function
     */
    protected initialize(): void {
        // this._API_GET_ITEMS   = "microservice-cms/api/article/article/list";
        this._API_GET_ITEMS   = {
            method    : "GET",
            path      : "microservice-cms/api/article/article/list",
            preExecute: (path: string, data: any) => {
                console.log(path);
                console.log(data);
                return {
                    endpoint: "microservice-cms/api/article/article/list",
                    params  : data
                };
            }
        };
        this._API_POST_ITEM   = "microservice-cms/api/new";
        this._API_GET_ITEM    = "microservice-cms/api/article/get/:_id";
        this._API_PUT_ITEM    = "microservice-cms/api/update/:_id";
        // this._API_DELETE_ITEM = "microservice-cms/api/rule/delete/:_id";
        this._API_DELETE_ITEM = {
            method     : "DELETE",
            path       : "microservice-cms/api/rule/delete/:_id",
            postExecute: (response: AxiosResponse) => {
                console.log(response);
                return {data: undefined, status: true, errors: ""};
            }
        };
    }

    private listResponseHandler = (response: AxiosResponse, pager: PagerTableStore): any[] => {
        if (response.data.status) {
            pager.totalItems = response.data.data.count;
            return response.data.data.items;
        }

        return [];
    };

}