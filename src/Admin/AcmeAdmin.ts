import { BaseAdmin }       from "../core/Admin/Base/BaseAdmin";
import { TableSchema }     from "../core/Admin/Base/TableSchema";
import { AxiosResponse }   from "axios";
import { PagerTableStore } from "../core/Store/PagerTableStore";
import { url_for }         from "../core/Utils/url_generator";
import AcmeCustomViewModel from "../ViewModel/AcmeCustomViewModel";
import { FilterSchema } from "../core/Admin/Base/FilterSchema";

export class AcmeAdmin extends BaseAdmin {

    protected defaultListViewModel = AcmeCustomViewModel;

    /**
     * Initialize func
     */
    protected initialize() {
        this.addNewRoute("custom_something", {
            displayText   : "Edit View",
            onClick       : (history => {
                console.log("Handler");
                history?.push(url_for("edit-page"));
            }),
            roles         : [],
            showInMainMenu: true,
            mainMenuGroup : "Edit"
            // router        : {
            //     auth     : true,
            //     component: HomeViewModel,
            //     exact    : true,
            //     name     : "my-custom-view-model",
            //     path     : `/my-custom-action/test`
            // }
        });
        this._API_GET_ITEMS = "/microservice-notification/api/list";
    }

    /**
     * Configure the list view delegate method
     */
    configureCreateView(): void {
        console.log(this.defaultListViewModel);
    }

    /**
     * Configure the list view delegate method
     */
    async configureEditView(): Promise<void> {
    }

    /**
     * Configure the list view delegate method
     */
    configureListView(table: TableSchema): void {
        table.apiResponseHandler = this.listResponseHandler;
    }

    private listResponseHandler = (response: AxiosResponse, pager: PagerTableStore): any[] => {
        return [];
    };

    /**
     * Configure filters used in table list
     * @param filters
     */
    configureFilters(filters: FilterSchema): void {
    }
}