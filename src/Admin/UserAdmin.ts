import { BaseAdmin } from "../core/Admin/Base/BaseAdmin";
import HomeViewModel from "../core/ViewModel/HomeViewModel";
import { FilterSchema } from "../core/Admin/Base/FilterSchema";

export class UserAdmin extends BaseAdmin {

    protected defaultListViewModel = HomeViewModel;

    protected _menuGroup = "menu_group_acmeadmin";


    /**
     * Implementation
     */
    configureCreateView(): void {
    }

    /**
     * Implementation
     */
    async configureEditView(): Promise<void> {
    }

    /**
     * Implementation
     */
    configureListView(): void {
    }

    /**
     * Initialize func
     */
    initialize(): void {
    }

    /**
     * Configure filters for List View
     * @param filters
     */
    configureFilters(filters: FilterSchema): void {
    }

}