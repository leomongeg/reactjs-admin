import { AdminInternalRouteTypes, BaseAdmin } from "../core/Admin/Base/BaseAdmin";
import { TableSchema }                        from "../core/Admin/Base/TableSchema";
import { AxiosResponse }                      from "axios";
import { PagerTableStore }                    from "../core/Store/PagerTableStore";
import { History }                            from "history";
import { url_for }                            from "../core/Utils/url_generator";
import { FormSchema, FormWidgetType }         from "../core/Admin/Base/FormSchema";
import { DateTimePickerFieldType }            from "../core/Views/Components/FormWidgets/DateTimeField";
import { FilterSchema } from "../core/Admin/Base/FilterSchema";

export class NotificationAdmin extends BaseAdmin {

    /**
     * Initialize admin
     */
    protected initialize(): void {
        this._API_GET_ITEMS = "/microservice-notification/api/list";
        this._API_POST_ITEM = "microservice-cms/api/new";
        const listRoute     = this.internalRoutes.get(AdminInternalRouteTypes.LIST_ROUTE_KEY);
        // listRoute?.roles.push("editor");
    }

    /**
     * create view config
     */
    configureCreateView(formSchema: FormSchema): void {
        formSchema.add({
                           key         : "title",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Title",
                           validations : "required|min:5",
                           initialValue: "",
                           elementProps: {variant: "outlined"},
                           helperText  : "Use a name  valid",
                           onChange    : (value: any) => {
                               console.log(`Value changed ${value}`);
                           }
                       })
                  .add({
                           key         : "email",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Email",
                           validations : "required|min:4|email",
                           initialValue: "",
                           elementProps: {variant: "outlined"},
                           helperText  : "Primary email address",
                       })
                  .add({
                           key         : "dniType",
                           type        : FormWidgetType.TYPE_SELECT,
                           label       : "DNI Type",
                           validations : "required",
                           initialValue: "",
                           helperText  : "Select the dni type",
                           elementProps: {
                               options: [{value: "fisica", displayText: "Fisica"}, {
                                   value      : "juridica",
                                   displayText: "Juridica"
                               }]
                           }
                       })
                  .with("SEO")
                  .add({
                           key         : "description",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Description",
                           initialValue: "",
                           // element     : TextField,
                           elementProps: {variant: "outlined", multiline: true},
                           helperText  : "SEO meta description",
                       })
                  .add({
                           key         : "enabled",
                           type        : FormWidgetType.TYPE_BOOLEAN,
                           label       : "Enabled",
                           initialValue: true,
                           elementProps: {},
                           helperText  : "Defines item  as enabled",
                       })
                  .add({
                           key         : "isFlagged",
                           type        : FormWidgetType.TYPE_BOOLEAN,
                           label       : "Flagged",
                           initialValue: false,
                           elementProps: {},
                           helperText  : "Defines item  as flagged",
                           // onChange?: (value: any) => void;
                       })
                  .add({
                           key         : "when",
                           type        : FormWidgetType.TYPE_DATETIME,
                           label       : "Whe you what your date",
                           initialValue: new Date(),
                           elementProps: {
                               pickerType: DateTimePickerFieldType.KEYBOARD_PICKER_DATETIME,
                               format: "YYYY/MM/DD HH:mm:ss"
                           },
                           helperText  : "Choose your date and time",
                       })
                  .end()
            .preSave = (data: any) => {

        }

        ;
    }

    /**
     * Edit view config
     * @TODO: Para el edit hay que agregar un mapper por propiedad para tomar el valor correcto
     * de la respuesta del server hacia el schema, parecido a como funciona el list view.
     */
    async configureEditView(formSchema: FormSchema): Promise<void> {
    }

    /**
     * Configure view config
     * @param tableSchema
     */
    configureListView(tableSchema: TableSchema): void {
        tableSchema.add({
                            key  : "_id",
                            label: "Id"
                        })
                   .add({
                            key  : "title",
                            label: "Titulo"
                        })
                   .add({
                            key  : "description",
                            label: "Descripción"
                        })
                   .add({
                            key   : "readed",
                            label : "Leída",
                            render: ((key, obj, t) => obj[key] === true ? "Si" : "No")
                        })
            .apiResponseHandler = this.listResponseHandler;
        tableSchema.actions.push({
                                     name   : "Ver",
                                     handler: (data: any, history: History) => {
                                         console.log(data);
                                         history.push(url_for("home-page"));
                                     }
                                 });
    }

    private listResponseHandler = (response: AxiosResponse, pager: PagerTableStore): any[] => {
        if (response.data.status) {
            pager.totalItems = response.data.data.count;
            return response.data.data.items;
        }

        return [];
    };

    /**
     * Add filter schema
     * @param filters
     */
    configureFilters(filters: FilterSchema): void {
        // @todo implement filters
    }
}