const skillStatus = {
    approved   : "APPROVED",
    deleted    : "DELETED", // only memory
    draft      : "DRAFT", // Pending submission, saved as Draft
    pending    : "PENDING",
    undo_delete: "UNDO_DELETE", // only memory
};

export default skillStatus;