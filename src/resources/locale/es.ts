/**
 * Default Language
 */

const es = {
    validator    : {
        numeric : "Por favor ingrese únicamente números",
        min     : "Por favor ingrese al menos :min caracteres",
        max     : "Por favor no ingrese más de :max caracteres",
        required: "Este campo es requerido"
    },
    common       : {
        "Notification"                     : "Notifications",
        "notification-list"                : "Notifications",
        "cms-list"                         : "CMS",
        "acme-list"                        : "Acme",
        "menu_group_acmeadmin"             : "Gestion de recursos",
        "user-list"                        : "Gestion de Usuarios",
        "__actions__"                      : "Acciones",
        "__edit__"                         : "Editar",
        "__delete__"                       : "Eliminar",
        "__save__"                         : "Guardar",
        "__cancel__"                       : "Cancelar",
        "__upload__"                       : "Upload",
        "__close__"                        : "Cerrar",
        "__ok__"                           : "Ok",
        "__accept__"                       : "Acceptar",
        "__delete_confirm_title__"         : "Está seguro que desea continuar?",
        "__delete_confirm_body__"          : "Al eliminar este elemento perderá la información relacionada. Está  seguro que desea cotinuar?",
        "__repeater_item_label_index__"    : "Item {{index}}",
        "__repeater_add_other_btn__"       : "Add other",
        "__repeater_delete_confirm_title__": "Está seguro que desea continuar?",
        "__repeater_delete_confirm_body__" : "Al eliminar este elemento perderá la información relacionada. Está  seguro que desea cotinuar?"
    },
    AdminCRUD    : {
        "__error__"  : "Ha ocurrido un error!",
        "__success__": "Datos procesados exitosamente!",
        "__add_new__": "Agregar nuevo",
        "__edit__"   : "Editar {{title}}"
    },
    AdminTable   : {
        table_list_header  : "List",
        filter_list_tooltip: "Filtrar",
        add_new_item       : "Agregar nuevo"
    },
    login        : {
        user_input     : "Identificación",
        pass_forgot_cta: "¿Olvido su contraseña?",
        navBarTitle    : "Login",
        password_input : "Contraseña",
        signIn_btn     : "Iniciar Sesión",
        title          : "Inicio de Sesión"
    },
    system_errors: {
        unauthorized    : {
            title: "Acceso Denegado",
            body : "Acceso denegado debido a credenciales inválidas"
        },
        forbidden       : {
            title: "Forbidden",
            body : "No cuenta con el nivel de acceso requerido para ver el recurso"
        },
        "internal-error": {
            title: "Error interno",
            body : "Se ha producido un error, favor intentar mas tarde o pongase en contacto con el administrador del sistema"
        },
        "not-found"     : {
            title: "Recurso No Encontrado",
            body : "El recurso solicitado no ha sido encontrado por el sistema"
        }
    },
    system       : {
        "go-home": "Volver al inicio"
    },
};

export default es;
