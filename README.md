# React Admin Dashboard

React Admin Dashboard is a project oriented to the fast development of the admin dashboard systems. The main objective 
is to build using configuration over programming, but keeping in main the extensibility and customization.

## Requirements:

 - NodeJS 11.11.0 (npm v6.7.0)
 - [Yarn](https://yarnpkg.com/en/)


## Instalación

1- Clone/Download the Repository: 

```bash
 $ git clone https://bitbucket.org/leomongeg/reactjs-admin.git
```

2- Install dependencies:

```bash
$ nvm install
$ nvm use
$ yarn install` (or `npm install` for npm)
````
 
3- Create an environment file with the basic configuration of the project for references see [.env.example](.env.example)

```bash
 $ cp .env.example .env
```

## Usage & Docs

 - [Quick Start](docs/quick_start.md)
 - [Configuration Files](docs/config_files.md)
 - [Admin Classes](docs/admin_classes.md)

## Available Form Widgets

 - TYPE_BOOLEAN
 - [TYPE_DATETIME](docs/widgets/DateTimeField.md)
 - TYPE_FILE
 - TYPE_TEXT
 - TYPE_SELECT
 - [TYPE_SELECT_MULTIPLE](docs/widgets/SelectMultipleField.md)
 - [TYPE_WYSIWYG](docs/widgets/WysiwygField.md)
 - [TYPE_REPEATER](docs/widgets/RepeaterField.md)

## Use

**Development**

For develoment use the following command, that enables the hot reload and tslint
 
```bash
 $ yarn start
``` 

* Build app continuously (HMR enabled)
* App served @ `http://localhost:8080`

**Production**

In order to create a production build:

```bash
 $ yarn run build
```

* Build app once (HMR disabled) to `/dist/`
* App served @ `http://localhost:3000`

---

**All commands**

Command | Description
--- | ---
`yarn run setup-start` | Check the .nvmrc, install, setup, configure and start the development runtime,
`yarn run start-dev` | Build app continuously (HMR enabled) and serve @ `http://localhost:8080`
`yarn run start-prod` | Build app once (HMR disabled) to `/dist/` and serve @ `http://localhost:3000`
`yarn run build` | Build app to `/dist/`
`yarn run test` | Run tests
`yarn run lint` | Run Typescript linter
`yarn run start` | (alias of `yarn run start-dev`)

**Note**: replace `yarn` with `npm` if you use npm.

## See also
* [React Webpack Babel Starter](https://github.com/vikpe/react-webpack-babel-starter)
* [Isomorphic Webapp Starter](https://github.com/vikpe/isomorphic-webapp-starter)