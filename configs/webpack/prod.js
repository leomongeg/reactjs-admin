// production config
const merge = require('webpack-merge');
const {resolve} = require('path');
const {appNodeModules} = require('../paths');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopySources = require('./plugins/CopySources')
const commonConfig = require('./common');
const CopyPlugin = require('copy-webpack-plugin');

commonConfig.module.rules.push({
                                   test: /\.tsx?$/,
                                   loaders: ['babel-loader', 'ts-loader'],
                                   exclude: [/node_modules/, appNodeModules]
                               })

module.exports = merge(commonConfig, {
    mode: 'production',
    entry: './index.tsx',
    output: {
        filename: 'js/bundle.[hash].min.js',
        path: resolve(__dirname, '../../dist'),
        publicPath: '/',
    },
    devtool: 'source-map',
    plugins: [
        new HtmlWebpackPlugin({
                                  template: 'index_prod.html.ejs',
                                  inject: false,
                                  minify: {html5: false},
                                  cache: true,
                                  hash: true
                              }),
        new CopyPlugin([
                           {
                               from: resolve(__dirname, '../../src/resources'),
                               to: resolve(__dirname, '../../dist/src/resources/')
                           }
                       ])
    ],
});
