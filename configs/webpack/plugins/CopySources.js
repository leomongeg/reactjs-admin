const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');

class CopySources {

    constructor(options) {
        this.options = options;
        if (!options.delay)
            this.options.delay = 5000;
    }

    apply(compiler) {
        compiler.hooks.compilation.tap('MyPlugin', (compilation) => {
            console.log('The compiler is starting a new compilation...')
            // Static Plugin interface |compilation |HOOK NAME | register listener
            HtmlWebpackPlugin.getHooks(compilation).afterEmit.tapAsync(
                'CopySources', // <-- Set a meaningful name here for stacktraces
                (data, cb) => {
                    // Manipulate the content
                    const timerId = setTimeout(() => {
                        fs.copyFile(
                            this.options.from,
                            this.options.to,
                            (err) => {
                                if (err) throw err;
                                console.log('Files copied successfully');
                                clearTimeout(timerId);
                            });
                    }, this.options.delay);
                    // Tell webpack to move on
                    cb(null, data)
                }
            )
        });
    }
}

module.exports = CopySources;