// development config
const {appNodeModules} = require('../paths');
const merge = require('webpack-merge');
const webpack = require('webpack');
const commonConfig = require('./common');
const HtmlWebpackPlugin = require('html-webpack-plugin');

commonConfig.module.rules.push({
                                   // @TODO: Under testing Option 2 for development compilation seems works better, improvement from 3s to 500ms
                                   test: /\.tsx?$/,
                                   loader: 'ts-loader',
                                   exclude: [/node_modules/, appNodeModules],
                                   options: {
                                       // disable type checker - we will use it in fork plugin
                                       transpileOnly: true
                                   }
                               });

module.exports = merge(commonConfig, {
    mode: 'development',
    entry: [
        'react-hot-loader/patch', // activate HMR for React
        'webpack-dev-server/client?http://localhost:8080',// bundle the client for webpack-dev-server and connect to the provided endpoint
        'webpack/hot/only-dev-server', // bundle the client for hot reloading, only- means to only hot reload for successful updates
        './index.tsx' // the entry point of our app
    ],
    devServer: {
        hot: true, // enable HMR on the server
        historyApiFallback: true,
        index: 'index.html',
    },
    devtool: 'cheap-module-eval-source-map',
    plugins: [
        new webpack.HotModuleReplacementPlugin(), // enable HMR globally
        new webpack.NamedModulesPlugin(), // prints more readable module names in the browser console on HMR updates
        new HtmlWebpackPlugin({template: 'index.html.ejs', inject: false})
    ],
});
