# Widget DateTimeField

This widget allows you to create a flexible Date Time Pickers Input control for ReactJS. 
This widget is being based on https://material-ui-pickers.dev.

Usage Example:

```typescript
formSchema.add({
       key         : "when",
       type        : FormWidgetType.TYPE_DATETIME,
       label       : "Whe you what your date",
       initialValue: new Date(),
       elementProps: {
           type: DateTimePickerFieldType.KEYBOARD_PICKER_DATETIME,
           format: "YYYY/MM/DD HH:mm:ss"
       },
       helperText  : "Choose your date and time",
    })
```

## Component Properties

Those properties are defined under elementProps:

### dateUtilsProvider?: any

Material-UI pickers relies on the date management library when the date should be parsed. Any other prop-types that accepts dates (e.g. minDate, maxDate) it can take string, number, Date object and so on.

Find more information about parsing dates in the documentation for your library:

 - [date-fns](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#Several_ways_to_create_a_Date_object)
 - [dayjs](https://github.com/iamkun/dayjs/blob/dev/docs/en/API-reference.md#constructor-dayjsexisting-string--number--date--dayjs)
 - [luxon](https://moment.github.io/luxon/docs/manual/parsing.html)
 - [moment](https://momentjs.com/docs/#/parsing/)
 
By default, this use moment.
___

### pickerType?: DateTimePickerFieldType

Defines the kind of picker to render. The available options are:

    - KEYBOARD_PICKER_DATE
    - KEYBOARD_PICKER_TIME
    - KEYBOARD_PICKER_DATETIME
    - PICKER_DATETIME
    - PICKER_DATE
    - PICKER_TIME
    
All the Keyboard types allow to inline edit the date value by the user, using the keyboard.
___

### format?: string

Format string
___


### inputVariant?: string "standard" | "outlined" | "filled"

Pass material-ui text field variant down, bypass internal variant prop.
___

### maxDate?: ParsableDate

Max selectable date
___

### minDate?: ParsableDate

Min selectable date

___

To see the full list of properties, please reach out this docs:

 - https://material-ui-pickers.dev/api/DatePicker
 - https://material-ui-pickers.dev/api/KeyboardDatePicker


