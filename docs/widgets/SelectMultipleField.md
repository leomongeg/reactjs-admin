# Widget SelectMultipleField

This widget allows you to create a flexible and beautiful Select Input control for ReactJS with multiselect, autocomplete.

This widget is being based on https://react-select.com, the main idea is to use this widget to map model elements
with a relation of one to many in the admin classes, but is not limited to that, because this widget allows
static and single selection.

Usage example:

```typescript
formSchema.add({
   key         : "categories",
   type        : FormWidgetType.TYPE_SELECT_MULTIPLE,
   label       : "Categories",
   validations : "required",
   initialValue: [],
   elementProps: {
       isMulti          : true,
       dataSourceStore  : Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore),
       dataSourceOptions: {
           apiEndpoint   : "https://c4e7ec30-b929-4913-a9ec-4b30c5a57e5a.mock.pstmn.io/microservice-cms/api/category/list?take=10&skip=1",
           method        : "get",
           params        : {},
           responseParser: (axiosResponse: AxiosResponse) => {
               return axiosResponse.data.data.items;
           }
       },
       model            : {label: "name", value: "_id"},
   },
   helperText  : "Select the categories related to the article",
})
```

## Component Properties

### closeMenuOnSelect?: boolean

Optional property default `false`. Close the option menu once an item is selected.
___

### isMulti?: boolean

Optional property default `false`. Enable multiple selection option.
___
   
### widgetStyles?: { [key: string]: (styles: any, options: any) => any }

Allow to modify the widget styles, for more information reach out the React Select docs:  https://react-select.com/styles
___


### dataSourceStore?: IMultiSelectDataSourceStore

When defined, allows you to define the Data Source store to fetch the data to fill the widget options. Please reach out 
the custom SelectMultipleField Data Source Store docs to explain how to create custom data source stores.

There is a built-in basic dataSourceStore called [DefaultSelectDataSourceStore](../../src/core/Store/MultipleSelect/DefaultSelectDataSourceStore.ts)
that uses the current axios context to perform API Calls to fetch the data to fill the widget options. 
When this store is defined the `model` and the `dataSourceOptions` properties are required.

In order to load an `IMultiSelectStore` you need to use the DI Container to load the Store. For example to load the 
DefaultSelectDataSourceStore:

```typescript
dataSourceStore: Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore)
```
___

### dataSourceOptions?: MultiSelectDefaultDataSourceOptions

Define the options required by `DefaultSelectDataSourceStore` to fetch and parse the remote data to fill the widget 
options. 

The options are:

```typescript
apiEndpoint: string;
method: "post" | "get";
params: any;
responseParser: (axiosResponse: AxiosResponse) => any[];
```

Usage example:

```typescript
dataSourceStore  : Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore),
dataSourceOptions: {
   apiEndpoint   : "https://c4e7ec30-b929-4913-a9ec-4b30c5a57e5a.mock.pstmn.io/microservice-cms/api/category/list?take=10&skip=1",
   method        : "get",
   params        : {},
   responseParser: (axiosResponse: AxiosResponse) => {
       return axiosResponse.data.data.items;
   }
}
```
___

### model?: MultiSelectModel

Define the model required by `DefaultSelectDataSourceStore` to map the remote data to the structure allowed by the
widget options.

The properties are:

 - `label: string` the string representation of the property to be use as a display text in the widget options.
 - `value: string` the string representation of the property to be use as a value the widget options.
 
Usage Example:

```typescript
dataSourceStore  : Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore),
dataSourceOptions: {
   apiEndpoint   : "https://c4e7ec30-b929-4913-a9ec-4b30c5a57e5a.mock.pstmn.io/microservice-cms/api/category/list?take=10&skip=1",
   method        : "get",
   params        : {},
   responseParser: (axiosResponse: AxiosResponse) => {
       return axiosResponse.data.data.items;
   }
},
model            : {label: "name", value: "_id"}
```
___

### staticData?: { label: string, value: string }[]

If the `dataSourceOptions` is no used yo can define an array of items to fill the widget options.


## MultiSelectDataSourceStore

Out of the box the `DefaultSelectDataSourceStore` allows you to connect with a remote API using the current axios 
context, but if you need to create a more custom and specific integration you can create a custom Data Source Store.

There are two requirements:

 1. Extends from [BaseStore](../../src/core/Store/Base/BaseStore.ts) and implements the required properties and methods
 2. Implements the interface [IMultiSelectDataSourceStore](../../src/core/Store/MultipleSelect/DefaultSelectDataSourceStore.ts)
 
The `IMultiSelectDataSourceStore` defines three required methods:

 - `fetchItems(inputValue: string, model?: MultiSelectModel): Promise<void>;` method called by the widget to start the
 fetching data process. This method is called during the widget initialization process to fill the options with default
 data and is called when the user types to filter the data.
 - `getItems` method used to return the getter of the property used to store the items processed during the `fethItems`
 - `cleanData` method called in the widget componentWillUnmount life cycle event to clean the data.
 
This is a full example of the implementation of a custom MultiSelect Data Source Store: 

```typescript
import BaseStore         from "../../core/Store/Base/BaseStore";
import {
    IMultiSelectDataSourceStore,
    MultiSelectItem,
    MultiSelectModel
}                        from "../../core/Store/MultipleSelect/DefaultSelectDataSourceStore";
import { AxiosInstance } from "axios";
import { Container }     from "typedi";
import AxiosService      from "../../core/Service/AxiosService";

export class CMSArticleFiltersStore extends BaseStore implements IMultiSelectDataSourceStore {
    public static readonly NAME_STORE: string = "CMSArticleFiltersStore";

    private selectItems: MultiSelectItem[] = [];

    private data: any[] = [];

    /**
     * Store initializer
     */
    protected init() {
        this.needPersistData = false;
    }

    /**
     * Clean Data
     */
    cleanData(): void {
        this.data        = [];
        this.selectItems = [];
    }

    /**
     * Axios instance
     * @private
     */
    private getAxios(): AxiosInstance {
        return Container.get(AxiosService).axios;
    }

    /**
     * Fetch items
     * @param inputValue
     * @param model
     */
    async fetchItems(inputValue: string, model?: MultiSelectModel): Promise<void> {
        let response;
        try {
            response = await this.getAxios().get("https://c4e7ec30-b929-4913-a9ec-4b30c5a57e5a.mock.pstmn.io/microservice-cms/api/filter/list?take=10&skip=1");
        } catch (e) {
            console.error(e);
        }

        if (response === undefined) {
            this.data        = [];
            this.selectItems = [];
            return;
        }

        this.data = response.data.data.items;

        if (inputValue.replace(/\W/g, "").length > 0) {
            this.selectItems = (this.data.map(obj => {
                return {label: obj.name, value: obj._id, fullObject: obj};
            })).filter(item => item.label.toLowerCase().includes(inputValue.toLowerCase()));
            return;
        }

        this.selectItems = this.data.map(obj => {
            return {label: obj.name, value: obj._id, fullObject: obj};
        });
        return Promise.resolve(undefined);
    }

    /**
     * Get Items
     * @param inputValue
     */
    getItems(inputValue?: string): MultiSelectItem[] {
        return this.selectItems;
    }

}
```

As the `CMSArticleFiltersStore` is a Store class needs to be loaded in the [LoadStore.ts](../../src/Store/LoadStore) file:

```typescript
import { CMSArticleFiltersStore } from "../MultipleSelectDataSources/CMSArticleFiltersStore";

export default [
    ... other stores
    CMSArticleFiltersStore
];
```

Finally, the usage:

```typescript
formSchema.add({
   key         : "filters",
   type        : FormWidgetType.TYPE_SELECT_MULTIPLE,
   label       : "Filters",
   initialValue: [],
   elementProps: {
       isMulti        : true,
       dataSourceStore: Container.get(ApplicationStore).getStore(CMSArticleFiltersStore),
   },
   helperText  : "Select the filters",
})
```

## Edit usage

For the usage in the `configureEditView`, to fill the data provided by the `subject` object and you need to request 
or map data for external resource:

 - Implement your data fetch:
 
```typescript
/**
 * Asume that the subject object has a property categories as an array of ids: article.categories -> ["5f344d0d35391d0e844f41d9", "5f3c76d5747550c109e913a1"]
 * and you need to fetch the full objects to get the display text value
 * currentIds = ["5f344d0d35391d0e844f41d9"] 
 */
private async fetchObjectCategories(currentIds: string[]): MultiSelectItem[] {
    // .. some async data fetching pasing the currentIds for filtering by the server side.
    return listItems;
}
``` 

 - In the `configureEditView` before of the form schema construction call your data fetching with the `await` keyword, 
 and pass the returned items to the widget `initialValue` prop:
 
```typescript
/**
 * Configure edit view
 *
 * @param formSchema
 */
async configureEditView(formSchema: FormSchema, subject: any) {
    const filteredCategories = this.fetchObjectCategories();
    formSchema.add({
                  key         : "categories",
                  type        : FormWidgetType.TYPE_SELECT_MULTIPLE,
                  label       : "Categories",
                  initialValue: filteredCategories,
                  elementProps: {
                      isMulti          : true,
                      dataSourceStore  : Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore),
                      dataSourceOptions: {
                          apiEndpoint   : "https://c4e7ec30-b929-4913-a9ec-4b30c5a57e5a.mock.pstmn.io/microservice-cms/api/category/list?take=10&skip=1",
                          method        : "get",
                          params        : {},
                          responseParser: (axiosResponse: AxiosResponse) => {
                              return axiosResponse.data.data.items;
                          }
                      },
                      model            : {label: "name", value: "_id"},
                  },
                  mapper: (key: string, obj: any) => { return filteredCategories; },  
                  helperText  : "Select the categories",
              });
}
```

and that's it this should be the result:

![selectmultiplefield-edit.png](../assets/selectmultiplefield-edit.png)