# Widget ColorPicker Field

The idea of this widget is to allow users to set in an interactive and simple way colors in hex string or rgb format object.
How to use it?
```
        .add({
            key: "color",
            type: FormWidgetType.TYPE_COLOR_PICKER,
            label: "MODULE:general.color",
            helperText: "MODULE:help.color",
            validations: "required",
            initialValue: "",
            elementProps: { 
                variant: "outlined",
                propertyColorValue: "hex (default)|rgb" 
                defaultValue: {
                    r: "52",
                    g: "51",
                    b: "148",
                    a: "100"
                }
            },
        })
```

## Component Properties

This component can use 2 properties to manipulate it:

### propertyColorValue?: string

Use this property to define which kind of value you need rgb or hex (Default) value. In case of rgb it is going to send to schema an object like this:
```
    {
        r: "241",
        g: "112",
        b: "19",
        a: "100",
    }
```

### defaultValue?: {r: string, g: string, b: string, a: string} | string

This property can be used to change the default selected color when form view start. It only works for create view, because in edit value is already set.
Also it is important to know that you can set an hexadecimal default value with or without '#' (for example "343394" or "#343394") or use traditional way in rgb object format like this:

```
    {
        r: "241",
        g: "112",
        b: "19",
        a: "100",
    }
```