# Widget WysiwygField

This widget allows you to create a flexible Wysiwyg Input control for ReactJS with image upload and multimedia support. 
This widget is being based on https://github.com/ckeditor/ckeditor5-react.

Usage Example:

```typescript
add({
   key         : "contentText",
   type        : FormWidgetType.TYPE_WYSIWYG,
   label       : "Content",
   validations : "required",
   initialValue: "",
   elementProps: {
       placeholder                 : "Add your text here!",
       onInitImageUploadRequest    : async (xhr: XMLHttpRequest) => {
           xhr.open("POST", "https://gateway-toyotacr.devdoubledigit.com/file-handler/file-upload", true);

           // change "Authorization" header with your header
           xhr.setRequestHeader("authorization", `Bearer ${Container.get(ApplicationStore).getStore(TokenStore).getAccessToken()?.accessToken}`);
           xhr.responseType = "json";
       }, onProcessBodyImageUpload : async (data: FormData) => {
           data.append("namespace", "cms");
           data.append("subPath", "ckeditor");
       }, onProcessImageUploadResponse: (response: any) => {
           if (!response || !response.status) {
             return "Error trying to upload your image.";
           }
           return {default: response.data.secure_url};
       }
   },
   helperText  : "Create your content",
})
```

## Component Properties

### placeholder?: string;

Optional, display placeholder of the component. Example, `placeholder={"Your content here"}`
___


### editorConfig?: any;

Optional, configuration for the editor, the default config is [defaultConfig](../../src/config/CKEditorConfig.ts).
___


## Image Handling

One of the built in plugins that comes with the CKEditor is the Image plugin that allows, upload, resize and positioning 
images in the content body.

In order to upload images the widget provides a built in image upload adapter with three hooks to handle that process.

Please, be noticed that those hooks are optional but if you use the image component you need to implement those in order 
to work properly.

### onInitImageUploadRequest?: CKEditorDefaultOnInit;

This hook or callback allows you to configure the XHR object with the information required to upload the image to the 
server. Example:

```typescript
onProcessImageUploadResponse: (response: any) => {
   xhr.open("POST", "https://gateway-toyotacr.devdoubledigit.com/file-handler/file-upload", true);
   
  // change "Authorization" header with your header
  xhr.setRequestHeader("authorization", `Bearer ${Container.get(ApplicationStore).getStore(TokenStore).getAccessToken()?.accessToken}`);
  xhr.responseType = "json";
}
```
___


### onProcessBodyImageUpload?: CKEditorDefaultOnProcessBody;

This hook or callback allows you to configure your request body and send additional information to the server. Example:

```typescript
onProcessBodyImageUpload : async (data: FormData) => {
   data.append("namespace", "cms");
   data.append("subPath", "ckeditor");
}
```
___


### onProcessImageUploadResponse?: any;

This hook or callback allows you to define to the value of the final URL to retrieve the image to render in the ckeditor.

**Important**
 
 - if there is an error the callback should return a string with the error message.
 - the structure to return the url is: `{default: response.data.imageURL}`
 
Example:

```typescript
onProcessImageUploadResponse: (response: any) => {
   if (!response || !response.status) {
     return "Error trying to upload your image.";
   }
   return {default: response.data.secure_url};
}
```

## Preview:

![wysiwygfield-edit.png](../assets/wysiwygfield-edit.png)



**Disclaimer**: The ClassicEditor is compiled with all the plugins. If you want to add more plugins
you need to recompile and link your own editor in the package.json. The current project use is:

```json
"ckeditor5-build-classic": "https://leomongeg@bitbucket.org/leomongeg/ckeditor5-build-classic.git",
"@ckeditor/ckeditor5-react": "2.1.0",
```

If you install the plugins directly, the installation is thru the sources and that produces an error. So you
need to create your own instance or fork from the editor, install, add, and configure your plugins, recompile and
upload to a version system or to npm.