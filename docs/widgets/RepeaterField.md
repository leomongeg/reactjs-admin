# Widget RepeaterField

This widget allows you to create a flexible repeater elements, building the embeddable content with form schemas.

Usage example:

```typescript
formSchema.add({
      key         : "repeater",
      type        : FormWidgetType.TYPE_REPEATER,
      label       : "Content",
      validations : undefined,
      initialValue: [],
      elementProps: {
          allowEmpty           : true,
          buildRepeatableFields: (schema: FormSchema) => {
              let categories = [];
              if (schema.subject && schema.subject.categories) {
                  categories = schema.subject.categories.map((category: any) => {
                      return {
                          fullObject: category,
                          label     : category.name,
                          value     : category._id
                      };
                  });
              }
              schema
                  .add({
                           key         : "title",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Titulo",
                           validations : "required",
                           initialValue: "",
                           elementProps: {variant: "outlined"},
                           helperText  : "Define the title of your post entry",
                           onChange    : (value: any) => {
                               console.log(`Value changed ${value}`);
                           }
                       }).add({
                                  key          : "categories",
                                  type         : FormWidgetType.TYPE_SELECT_MULTIPLE,
                                  label        : "Categories",
                                  validations  : "required",
                                  initialValue : categories,
                                  elementProps : {
                                      isMulti          : true,
                                      dataSourceStore  : Container.get(ApplicationStore).getStore(DefaultSelectDataSourceStore),
                                      dataSourceOptions: {
                                          apiEndpoint   : `${process.env.REACT_APP_ENDPOINT_BACKEND}microservice-cms/api/category/list`,
                                          method        : "get",
                                          params        : {take: 10, skip: 1, type: "tip"},
                                          search        : "remote",
                                          responseParser: (axiosResponse: AxiosResponse) => {
                                              return axiosResponse.data.data.items;
                                          }
                                      },
                                      model            : {label: "name", value: "_id"},
                                  }, helperText: "Select the categories",
                              });
          }
      },
      helperText  : "Create your repeatable content here, adding or removing items, according to your needs.",
  })
```

**Important:** 
 - The validations always should be disabled for the repeater element, all the validations are handle by every 
 embed element. If any embedded element has errors, the accordion automatically is opened to let the user know that are 
 errors.
 - Initial Value should be an `Array` or `undefined` any other value raise an `Error`. 

## Component Properties

### allowEmpty?: boolean;

Defines if the component allows empty values.
___

### repeaterItemsLabel?: (t: TFunction, index: number) => string;

A handler function to create the string label for every inserted item. By default, the i18n key `__repeater_item_label_index__` 
is used as label.
___

### defaultExpanded?: boolean;

All embed elements are inside of a Accordion panel, this property defines if the accordion is open or close when the
component load. By default, the behavior is close.
___

### alwaysOpen?: boolean;

Disables the accordion behavior and set the component as always open.
___

### confirmDeleteItem?: boolean;

If true, show a confirmation modal to ask to remove or cancel. Default value, false.

The modal content is handle by the i18n file, under the common namespace, by the keys:
 - `__repeater_delete_confirm_title__`
 - `__repeater_delete_confirm_body__`
 
___

### maxEmbeds?: number;

Limit the number of embed elements.
___ 

### buildRepeatableFields: (schema: FormSchema) => any;

A handler function to build the embeddable form schema. Use the same behavior that you use to, build a form.
___


## Limitations:

 - Not support multiple levels of embeds, only one. 

## Preview:

![repeaterfield.png](../assets/repeaterfield.png)


