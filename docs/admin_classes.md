# Admin Classes

ReactJS Admin Boilerplate helps you manage your data using a graphical interface that will let you create, update 
or search your API model endpoints. The Boilerplate relies on Admin classes to know which schemas will be managed and 
how these actions will look like.

An Admin class decides which fields to show on a listing, which fields are used to find entries and how the create 
form will look like. Each API model endpoints will have its own Admin class.

## 1- Create your Admin Class

The first step is to create your Admin Class, all the admin classes should be created under the [src/Admin](../src/Admin)
directory.

**Requirements:** 

 - Each AdminClass needs to extend from [BaseAdmin](../src/core/Admin/Base/BaseAdmin.ts) class
 - The AdminClass name needs to end with the word Admin. For example `UserAdmin` or `AcmeAdmin`
 - The AdminClass needs to implement all the abstract members defined in the BaseAdmin class.
 
```typescript
export class AcmeAdmin extends BaseAdmin {

    protected defaultListViewModel = AcmeCustomViewModel;

    /**
     * Initialize func configure here your admin class general information, like the 
     * menu display name, the menu group, roles required by the user
     */
    protected initialize() {
    }

    /**
     * Configure Create delegate method. Configure the Create view form schema 
     */
    configureCreateView(formSchema: FormSchema): void {
        formSchema.add({
                           key         : "title",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Title",
                           validations : "required",
                           initialValue: "",
                           elementProps: {variant: "outlined"},
                           helperText  : "Define the title of your post entry",
                           onChange    : (value: any) => {
                               console.log(`Value changed do something ${value}`);
                           }
                       });
    }

    /**
     * Configure Edit delegate method. Configure the edit view form schema 
     */
    configureEditView(formSchema: FormSchema, subject: any): void {
        formSchema.add({
                           key         : "title",
                           type        : FormWidgetType.TYPE_TEXT,
                           label       : "Title",
                           validations : "required",
                           initialValue: "",
                           elementProps: {variant: "outlined"},
                           helperText  : "Define the title of your post entry",
                           onChange    : (value: any) => {
                               console.log(`Value changed ${value}`);
                           },
                           mapper      : (key: string, obj: any) => { // Used only during the edit schema build
                                return obj.innerData[key];
                          } 
                       })
    }

    /**
    * Configure Filters for list view
    */
    configureFilters(filters: FilterSchema): void {
        // Add a filter called q, It use FormSchema format
        filters.add({
            initialValue: "",
            key: "q",
            label: "Name",
            type: FormWidgetType.TYPE_TEXT
        });
        // Process filter information and reformat it in a way list can use it. Mutes the object parameter
        filters.processFilter = (object: any) => {
            // remove empty fields
            Object.keys(object).forEach(key => {
                if (!object[key]) {
                    Reflect.deleteProperty(object, key);
                }
            });
        };
    }


    /**
     * Configure List delegate Method. Configure the list view delegate method
     */
    configureListView(tableSchema: TableSchema): void {
        tableSchema
            .add({
                     key  : "_id",
                     label: "id"
                 })
            .add({
                     key  : "title",
                     label: "Title"
                 })
            .add({
                     key  : "slug",
                     label: "Slug"
                 })
            .add({
                     key  : "contentType",
                     label: "Content Type"
                 })
            .apiResponseHandler = this.listResponseHandler;
        tableSchema.entityIdentifierKey = "slug";
    }
    
    /**
     * From the response of the list API call, defines the response object and configure the pager according to 
     * your api response structure. 
     */    
    private listResponseHandler = (response: AxiosResponse, pager: PagerTableStore): any[] => {
        if (response.data.status) {
            pager.totalItems = response.data.data.count;
            return response.data.data.items;
        }

        return [];
    };

}
```

So, what does this code do?

 - configureCreateView(): This method configures which fields are displayed on the create actions. The FormSchema allows you to configure the form field widgets.
 - configureEditView(): This method configures which fields are displayed on the edit actions. The FormSchema allows you to configure the form field widgets.
 - configureListView(): Here you specify which fields are shown when all API models are listed (the tableSchema.entityIdentifierKey property means that this field will link to the show/edit page of this particular schema).
 - configureFilters(): This method configures a list of filters to show in the view list. The filter button appears in the top lists and when clicked the filters show. 
This is the most basic example of the Admin class. You can configure a lot more with the Admin class. This will be covered by docs.