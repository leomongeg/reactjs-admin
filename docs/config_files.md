# Configuration Files

One of the key features of the ReactJS Admin Boilerplate is the capability of customization through configuration files.
In this doc file you can find all the configuration settings available for use.

All the configuration settings are stored in the [src/config](../src/config) directory:

## [Admin.ts](../src/config/Admin.ts) Configuration File

### AdminSettings

The way in that you define your admin pages is through something that we call AdminClasses. An Admin Class is an special
class that defines several methods and configuration patters that allow the ReactJS Admin Boilerplate to generate
your Admin CRUDs or your custom Admin Sections. For more information please reach out the Admin Class Docs @TODO: Implement docs

The way that you load the Admin Classes in order to be read by the ReactJS Admin Core to generate the Admin CRUDS is
registering the Classes in this file. Just open the file and add your Admin Class to the Array:

```typescript
export const adminSettings: AdminSettings[] = [
    {
        adminClass: UserAdmin
    },
    {
        adminClass: AcmeAdmin
    }
];
```  
In the previous example suppose that you already created an Admin Class called AcmeAdmin, just import your class here
and the core instantiate and initialize it during the system bootstrapping.



### CustomResolvers

The custom resolvers are component classes that allow the system to delegate you to implement internal system functions 
or behaviors. Please Reach out the CustomResolvers Docs: @TODO: Pending implement this documentation.

```typescript
import { DefaultAccessTokenResolver } from "../src/core/Resolvers/AccessToken/DefaultAccessTokenResolver";

/**
 * Define your custom behavior here overriding the defined class and implementing the related interface
 * as the documentations describes.
 */
export const CustomResolvers: any = {
    AccessTokenResolver: DefaultAccessTokenResolver
};
```


## [API_CONST.ts](../src/config/API_CONST.ts) Configuration File

This configuration file contains the API Call constants endpoints and allow you to add your owns or override the existing
according to your API needs.

```typescript
export default {
    GET_ME               : `/api/me`,
    POST_ACCESS_TOKEN    : `/oauth/token`,
    POST_CHANGE_PASS     : `/api/auth/change-password`,
    POST_FORGOT_PASS     : `/api/auth/forgot-password`,
    POST_LOGOUT          : `/auth/logout`,
};
```


## [AuthSettings.ts](../src/config/AuthSettings.ts) Configuration File

This configuration file contains the configuration settings for the authentication process and form:

```typescript
/**
 * This configuration file contains all the configuration settings for the login form and login process:
 *  - ADMIN_ROLE: The base role required for the users to login into the Admin Application.
 *  - USER_INPUT_FORM_VALIDATIONS: Form validations for the form username input.
 *  - PASSWORD_INPUT_FORM_VALIDATIONS: Form validations for the form password input.
 *  - AUTH_USER_NAME_KEY: Your API username key to send in the request.
 *  - AUTH_USER_PASS_KEY: Your API password key to send in the request.
 *
 *  For more options in the form validations please see https://www.npmjs.com/package/simple-react-validator#rules
 */
export default {
    ADMIN_ROLE                     : "admin",
    USER_INPUT_FORM_VALIDATIONS    : "min:8",
    PASSWORD_INPUT_FORM_VALIDATIONS: "min:5",
    AUTH_USER_NAME_KEY             : "identification",
    AUTH_USER_PASS_KEY             : "password"
};
```


## [axios.config.ts](../src/config/axios.config.ts) Configuration File

Contains all the configurations required to the [AxiosService](../src/core/Service/AxiosService.ts).

### CONFIG_GET_ACCESS_TOKEN

Contains all AxiosRequestConfig settings for the authentication, custom headers and the default API base url.
For more information: https://github.com/axios/axios#request-config 

```typescript
export const CONFIG_GET_ACCESS_TOKEN = <AxiosRequestConfig> {
   baseURL    : process.env.REACT_APP_ENDPOINT_BACKEND,
   headers    : {
       "Content-Type": "application/x-www-form-urlencoded"
   }
};
```

### PARAMS_GET_ACCESS_TOKEN

Contains the Authentication parameters according to the OAuth 2.0 protocol:

```typescript
export const PARAMS_GET_ACCESS_TOKEN = {
    client_id    : process.env.REACT_APP_API_BACKEND_CLIENT_ID,
    client_secret: process.env.REACT_APP_API_BACKEND_CLIENT_SECRET,
    grant_type   : process.env.REACT_APP_API_BACKEND_GRANT_TYPE,
};
```

### CONFIG_CALL_GET_API

Defines the configuration for the GET HTTP verb API Calls:

```typescript
export const CONFIG_CALL_GET_API = <AxiosRequestConfig> {
    baseURL    : process.env.REACT_APP_ENDPOINT_BACKEND,
    headers    : {
        "Content-Type": "application/x-www-form-urlencoded",
        "authorization" : "Bearer %s"
    }
};
```

### CONFIG_CALL_POST_API

Defines the configuration for the POST HTTP verb API Calls:

```typescript
export const CONFIG_CALL_POST_API = <AxiosRequestConfig> {
    baseURL    : process.env.REACT_APP_ENDPOINT_BACKEND,
    headers    : {
        "authorization" : "Bearer %s",
        "content-type": "application/json"
    }
};
```

## [ThemeConfig.ts](../src/config/ThemeConfig.ts) Configuration File

Contains all the look an feel configurations:

```typescript
export default {
    /**
     * Default Material UI theme options
     * @url https://material-ui.com/customization/theming/
     * @url https://material-ui.com/customization/default-theme/#default-theme
     * @property {ThemeOptions}
     */
    themeOptions: {
        palette: {
            error  : {
                main: "#f44336"
            },
            type   : "dark",
            primary: {
                contrastText: "#fff",
                main        : "#4dabf5"
            }
        }
    },
    /**
     * Side menu options
     */
    menuOptions : {
        // Enable the tree view in the side menu, default disable
        treeMode: false
    }
};
```

## [PageViewModels.ts](../src/config/PageViewModels.ts)

Override the default view models handler for the internal system pages and global components. Override any View Model
if you want to change the default behavior or add your custom implementation:

```typescript
export default {
    /**
     * Override the default Dashboard home page view model.
     */
    DashboardPageViewModel: HomeViewModel,

    /**
     * Override the default Login View Model page.
     */
    LoginViewModel: LoginViewModel
};
```

