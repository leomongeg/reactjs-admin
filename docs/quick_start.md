# Quick Start

ReactJS Admin Boilerplate is a project-based in the idea of the Sonata Project to build Admin Dashboards in an easy
way, using configuration over programing, fully customized and extensible. 

The base project is built on top of MaterialUI and structured with the MVVM Software design pattern.

First clone or download the project:

```bash
 $ git clone https://bitbucket.org/leomongeg/reactjs-admin.git
```

Install dependencies:
 ```bash
 $ nvm install
 $ nvm use
 $ yarn install` (or `npm install` for npm)
 ````

Create an environment file with the basic configuration of the project for references see [.env.example](.env.example)

```bash
 $ cp .env.example .env
```

```dotenv
# Application environment
ENV=development
REACT_APP_ENV=dev
REACT_APP_BASE_URL=http://localhost:8080

REACT_APP_ENDPOINT_BACKEND=https://your-awesome-api.com/
REACT_APP_API_BACKEND_CLIENT_ID=74bfcc16-b7ea-11ea-b3de-0242ac130004
REACT_APP_API_BACKEND_CLIENT_SECRET=7c3f036c-b7ea-11ea-b3de-0242ac130004
REACT_APP_API_BACKEND_GRANT_TYPE=user_credentials

REACT_APP_ROUTING_PREFIX=''
```

As your can see the env file contains all your API configuration. You can add more env vars to your env file but
please be noticed that you need to add the `REACT_APP_` prefix otherwise the variables cannot be loaded into your code.  

In order to start the project just execute the `yarn start` script to enable the hot reload:

```bash
yarn start
```

In your favorite web browser open new tab with the url: http://localhost:8080 and you should see the following screen:

![Login View](./assets/login-view.png)

The nex step is make this login form working. To do that, you should open the AuthSettings config file located in 
[src/config/AuthSettings.ts](../src/config/AuthSettings.ts), the configure your login form using the following settings:

```typescript
/**
 * This configuration file contains all the configuration settings for the login form and login process:
 *  - ADMIN_ROLE: The base role required for the users to login into the Admin Application.
 *  - USER_INPUT_FORM_VALIDATIONS: Form validations for the form username input.
 *  - PASSWORD_INPUT_FORM_VALIDATIONS: Form validations for the form password input.
 *  - AUTH_USER_NAME_KEY: Your API username key to send in the request.
 *  - AUTH_USER_PASS_KEY: Your API password key to send in the request.
 *
 *  For more options in the form validations please see https://www.npmjs.com/package/simple-react-validator#rules
 */
export default {
    ADMIN_ROLE                     : "admin",
    USER_INPUT_FORM_VALIDATIONS    : "min:8",
    PASSWORD_INPUT_FORM_VALIDATIONS: "min:5",
    AUTH_USER_NAME_KEY             : "identification",
    AUTH_USER_PASS_KEY             : "password"
};
```

In the demo, RestFul API that I am using for this example the login is through the identification number, and the API 
is waiting for that parameter, and the password key remains equals. For example, if you API the login is through 
username or email, just change the `AUTH_USER_NAME_KEY` for the appropriate value.

Now you need to define the API endpoint to perform the login process (retrieve the access token), and fetch the user info
in the [src/config/API_CONST.ts](../src/config/API_CONST.ts) you can setup the API endpoints or add more as you need:

```typescript
export default {
    GET_ME               : `api/me`,
    POST_ACCESS_TOKEN    : `/oauth/token`,
    POST_CHANGE_PASS     : ``,
    POST_FORGOT_PASS     : ``,
    POST_LOGOUT          : `/auth/logout`,
};
```

At this point the important once are `POST_ACCESS_TOKEN` and the `GET_ME`. The `GET_ME` API Call should return the user
roles required for the validation. Once the endpoints are properly configured you can hit the login button and view the 
two API calls:

![Login View](./assets/login-call.png)

The Access Token Response for this call looks like this:

```json
{
  "_id": "5ef665558c88d5001257ef74",
  "accessToken": "8ba441ee-ce3a-4857-b311-bd00e904dab7",
  "accessTokenExpiresAt": "2020-06-27T01:15:01.848Z",
}
```

If you need to change the login behavior, you can create a custom resolver for the login process. First inside the 
[CustomReslovers](../src/CustomResolvers) Directory create a new class; for example, `LoginResolver` and implement the 
`IAccessTokenResolver` interface as following:

```typescript
import { IAccessToken }              from "../../Models/Security/AccessToken";
import { AxiosInstance }             from "axios";
import AuthSettings                  from "../../../config/AuthSettings";
import API_CONST                     from "../../../config/API_CONST";
import { CONFIG_GET_ACCESS_TOKEN }   from "../../../config/axios.config";
import { IAccessTokenResolver }      from "../core/Resolvers/AccessToken/IAccessTokenResolver";

export class LoginResolver implements IAccessTokenResolver {

    /**
     * Return the access token execution
     *
     * @param usernameValue {string}
     * @param passwordValue {string}
     * @param axios {AxiosInstance}
     * @return {Promise<IAccessToken | false>}
     */
    public async getAccessToken(usernameValue: string, passwordValue: string, axios: AxiosInstance): Promise<IAccessToken | false> {
        const params                            = {
            client_id    : process.env.REACT_APP_API_BACKEND_CLIENT_ID,
            client_secret: process.env.REACT_APP_API_BACKEND_CLIENT_SECRET,
            grant_type   : process.env.REACT_APP_API_BACKEND_GRANT_TYPE,
        };
        params[AuthSettings.AUTH_USER_NAME_KEY] = usernameValue;
        params[AuthSettings.AUTH_USER_PASS_KEY] = passwordValue;

        const response = await axios.post(API_CONST.POST_ACCESS_TOKEN, params, CONFIG_GET_ACCESS_TOKEN);
        if (response.data.code && response.data.code === 500) return false;

        return {
            accessToken         : response.data.accessToken,
            accessTokenExpiresAt: response.data.accessTokenExpiresAt
        };
    }
}
```

Please be noticed that the `IAccessTokenResolver` requires you to implement the `getAccessToken` that provides you all
the parameters required to customize the request access token process. 

Finally, edit the config file [src/config/Admin.ts](../src/config/Admin.ts) in the `CustomResolvers` section to 
change the class in charge to resolve the access token by yours class instead of the Default class:

```typescript
import { LoginResolver } from "../CustomResolvers/LoginResolver";

/**
 * Define your custom behavior here overriding the defined class and implementing the related interface
 * as the documentations describes.
 */
export const CustomResolvers: any = {

    AccessTokenResolver: LoginResolver
};
```

With all the configuration provided, the login process is completed an you should be able to see the Dashboard 
landing page:

![Login View](./assets/empty-dashboard-view.png)

As you can see the Dashboard is empty so the next step is to configure and Admin Class. To do that please create a 
new class under the [src/Admin](../src/Admin) directory for example `CMSAdmin.ts` Then create a new class, extends 
from [BaseAdmin](../src/core/Admin/Base/BaseAdmin.ts) and implement all the abstract methods:

```typescript
import { BaseAdmin }                  from "../core/Admin/Base/BaseAdmin";
import { FormSchema, FormWidgetType } from "../core/Admin/Base/FormSchema";
import { TableSchema }                from "../core/Admin/Base/TableSchema";
import { AxiosResponse }              from "axios";
import { PagerTableStore }            from "../core/Store/PagerTableStore";
import { Container }                  from "typedi";

export class CMSAdmin extends BaseAdmin {
    /**
     * Initialize func configure here your admin class general information, like the 
     * menu display name, the menu group, roles required by the user
     */
    protected initialize() {
    }

    /**
     * Configure Create delegate method. Configure the Create view form schema 
     */
    configureCreateView(formSchema: FormSchema): void {
    }

    /**
     * Configure Edit delegate method. Configure the edit view form schema 
     */
    configureEditView(formSchema: FormSchema, subject: any): void {
    }

    /**
     * Configure List delegate Method. Configure the list view delegate method
     */
    configureListView(tableSchema: TableSchema): void {
    }
}
```

Now you are able to show the CMS List option in the main menu: 

![Login View](./assets/admin-cms-list.png)

If you want to change the menu text just open the default translations file under the resource's directory: 
[src/resources/locale/es.ts](../src/resources/locale/es.ts) and search for the `common` key and add the exact same
name displayed in the list view as key of the `common` object and for the value the desired text for example CMS List:

```typescript
    common : {
        "cms-list"            : "CMS List",
        "__actions__"         : "Acciones",
        "__edit__"            : "Editar",
        "__delete__"          : "Eliminar",
        "__save__"            : "Guardar",
        "__cancel__"          : "Cerrar",
        "__upload__"          : "Upload"
    },
```

The expected result should be something like this:

![Login View](./assets/admin-cms-list-text.png)

Much more better! Know lets configure the list view. Go back to our CMSAdmin Class and takes do the following 
modifications to the `initialize` and `configureListView` methods:

```typescript
    /**
     * Initialize func configure here your admin class general information, like the 
     * menu display name, the menu group, roles required by the user
     */
    protected initialize(): void {
        this._API_GET_ITEMS = "microservice-cms/api/list"; // This is my API list items endpoint
    }

    /**
     * Configure list view
     *
     * @param tableSchema
     */
    configureListView(tableSchema: TableSchema): void {
        tableSchema
            .add({
                     key  : "_id",
                     label: "id"
                 })
            .add({
                     key  : "title",
                     label: "Title"
                 })
            .add({
                     key  : "slug",
                     label: "Slug"
                 })
            .add({
                     key  : "contentType",
                     label: "Content Type"
                 })
            .apiResponseHandler = this.listResponseHandler;
    }

    /**
     * From the response of the list API call, defines the response object and configure the pager according to 
     * your api response structure. 
     */    
    private listResponseHandler = (response: AxiosResponse, pager: PagerTableStore): any[] => {
        if (response.data.status) {
            pager.totalItems = response.data.data.count;
            return response.data.data.items;
        }

        return [];
    };
```

Fist, be noticed that we added a value for the property `_API_GET_ITEMS` this is my Demo API endpoint to fetch the list
of elements by default the pages add two parameters for the pagination take: the amount of items to retrieve and skip, 
the page number.

The next important element is that we added some stuff to the `configureListView` method. These properties declared in the
`add` methods, members of the `TableSchema` parameter are the way that we can introduce which elements we like to display 
in the table list view. The `key` defines the name of the property returned by my API structure and the label is the Title
of the table list column.

Finally we need to add a mandatory handler for the response, the `apiResponseHandler`, is a callback function tar contains
the axios api call response and the pager object, this allows my to tell the pager the number of elements to calculate the
pages to display and return the array of items to be used by the table schema to build the list view.

In this case this is my API response structure:

```json
{
  "data": {
    "count": 26,
    "first": true,
    "items": [
      {
        "slugEditable": true,
        "_id": "5ebee43fc80d1a00110a4394",
        "slug": "terms-and-conditions",
        "title": "Terms and Conditions",
        "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "contentType": "article",
        "createdAt": "2020-05-15T18:49:35.388Z",
        "updatedAt": "2020-05-15T20:29:22.270Z"
      },
      ...,
    ],
    "last": false,
    "pages": 3
  },
  "error": {
    
  },
  "status": true
}
```

The result:

![admin-list-view](./assets/admin-list-view.png)

The pager and the actions are automatically generated by the table schema builder. The actions roles and permissions
are fully customizable, for example you can define that the delete action only should be displayed for certain roles, and 
you can add your custom actions.

The pager has been built with [Material UI Pagination](https://material-ui.com/components/pagination/) see the docs for
more options.

Let's configure the create new CMS Entry view:

```typescript
    /**
     * Initialize func configure here your admin class general information, like the 
     * menu display name, the menu group, roles required by the user
     */
    protected initialize(): void {
        this._API_GET_ITEMS = "microservice-cms/api/list"; // This is my API list items endpoint
        this._API_POST_ITEM = "microservice-cms/api/new"; // This is my API end point to create a new CMS Entry
    }

    /**
         * Configure create view
         *
         * @param formSchema
         */
        configureCreateView(formSchema: FormSchema): void {
            formSchema.add({
                               key         : "title",
                               type        : FormWidgetType.TYPE_TEXT,
                               label       : "Title",
                               validations : "required",
                               initialValue: "",
                               elementProps: {variant: "outlined"},
                               helperText  : "Define the title of your post entry",
                               onChange    : (value: any) => {
                                   console.log(`Value changed ${value}`);
                               }
                           })
                      .add({
                               key         : "slugEditable",
                               type        : FormWidgetType.TYPE_BOOLEAN,
                               label       : "Slug Editable",
                               initialValue: true,
                               elementProps: {variant: "outlined"},
                               helperText  : "The slug value should be unique, if the system detects repeated slug try to assign other, other wise an error be throw.",
                           })
                      .add({
                               key         : "content",
                               type        : FormWidgetType.TYPE_TEXT,
                               label       : "Content",
                               validations : "required",
                               initialValue: "",
                               helperText  : "Add the content for your post entry",
                               elementProps: {variant: "outlined", multiline: true}
                           })
                      .add({
                               key         : "contentType",
                               type        : FormWidgetType.TYPE_SELECT,
                               label       : "Content Type",
                               validations : "required",
                               initialValue: "article",
                               helperText  : "Select the content type",
                               elementProps: {
                                   options: [{value: "article", displayText: "Article"}]
                               }
                           })
                .preSave = (data: any) => {
                    // Do some stuff with the data structure or add some static properties to the body object
            };
        }
```

And the result:

![admin-cms-new](./assets/admin-cms-new.png)


The structure of API request made: 

![admin-cms-new-request](./assets/admin-cms-new-request.png)

During the initialization process, the api routes can be defined as a `CRUDRoute` object or string.
The `CRUDRoute`, allows to create a more detail and control over the API request.

This is the `CRUDRoute` object definition:

```typescript
export type CRUDRoute = {
    method: "POST" | "GET" | "DELETE" | "PUT";
    path: string; // Example: /api/find-by-id/:id/:slug
    preExecute?: (path: string, data?: any) => { params: any, endpoint?: string }, // Allows override or mutate some values for the request like the endpoint and the parameters
    postExecute?: (response: AxiosResponse | undefined) => { status: boolean, errors: string, data: any | undefined }; // Allows change or mutate the response data
};
```

For example:

```typescript
    /**
     * Initialize func configure here your admin class general information, like the 
     * menu display name, the menu group, roles required by the user
     */
    protected initialize(): void {
        this._API_GET_ITEMS = "microservice-cms/api/list"; // This is my API list items endpoint
        this._API_POST_ITEM = "microservice-cms/api/new"; // This is my API end point to create a new CMS Entry
        this._API_GET_ITEM = {
            method    : "GET",
            path      : "microservice-cms/api/article/get/:_id",
            preExecute: (path: string, data: any) => {
                console.log(path);
                console.log(data); // Data is an object with the entityIdentifierKey and the value for the entityIdentifierKey: {"_id": 1}
                return {
                    // The endpoint use to perform the API request
                    endpoint: api_url_generator("microservice-cms/api/article/get/:_id", data),
                    // Additional parameters
                    params  : {}
                };
            },
            postExecute: (response: AxiosResponse) => {
                return {data: response.data.data, status: true, errors: ""};
            }
        };
    }
```