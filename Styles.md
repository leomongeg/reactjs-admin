# Purdy Go Styles Guideline

Purdy Go is made to be fully modular application. This meaning that each component can be used by itself.

## Folder Structure

The main components are divided in two Main Folders:



## Styling

The main library selecte for the styling of the app is [Styled Components](https://styled-components.com/) as it allows us to hold the styling inside the component itself without specificty problems, as well as theming to keep consistency around the whole application.


### Theming

Theming is declared in the Layout File it provides the `theme` object used by styled components accross all the components

### Building the components 

The components are built in their own sepparate file each holding the styles relevants for them and their inmediate and indivisible children.

All the styles are applied using the `styled` function provided by Styled Components:

```
import React from 'react';
import styled from 'styled-components';

const CardBody = styled.div`
    color: black;
`

class Card extends React.Component {
    render() {
        return (
            <CardBody>
                {props.children}
            </CardBody>
        );
    }
}
export default Card;
```

You can access teh theme object inside the styled function via `props`:

```
const CardBody = styled.div`
    color: black;
    background-color: ${props => props.theme.colors.primary};
`

```

It is possible to have styles dependant of specific props from the component, an interface is needed for typescript to aknowledge those props:

```

/*
 * Styled Props definition Interface
 */
interface CardStyledProps {
    disabled?: boolean;
}

const CardBody = styled("div")<CardStyledProps>`
    color: black;
    background-color: ${props => props.disabled ? "#5f5f5f" : "#ffffff"};
`

class Card extends React.Component {
    render() {
        return (
            <CardBody disabled>
                {props.children}
            </CardBody>
        );
    }
}
```


## Icons

Icons are imported from the [Iconscout Unicons](https://github.com/Iconscout/react-unicons) library, ththe main import contains all the react nodes available and renders an svg in the dom:

```
const UNICONS = require("@iconscout/react-unicons");

<UNICONS.UilCheck />
```


## Available components

Here are the available components and their accepted props, for a visual guide you can head to /styleguide in your local environment:

## Text
### Heading
Heading is the component used for all titles, it takes a `asTag` prop to define its hierarchy, it defaults to `"h1"`, and a `color` prop, which defaults to `'black'`:

```
<Heading asTag="h2" color="primary">Heading 2 with custom color</Heading>
```

| Prop | Values |
| ----------- | ----------- |
| `asTag` | String: `"h1"`, `"h2"`, `"h3"`, `"h4"`, `"h5"` |
| `color` | String: Any color from the `theme.colors` object located in `Layout.tsx`   |



### Copy 
This components holds all text blocks, it receives a `size` prop and defaults to `"md"`, and a `color` prop, which defaults to `'black'`:

```
<Copy size="lg" color="muted"><strong>Copy Large Size with custom color:</strong> Lorem ipsum dolor sit amet, ... laborum.</Copy>
```


| Prop | Values |
| ----------- | ----------- |
| `size` | String: `"sm"`, `"md"`, `"lg"`. Default is "md" |
| `align` | String: `"left"`, `"center"`, `"right"`. Default is "left" |
| `color` | String: Any color from the `theme.colors` object located in `Layout.tsx`. Default is "black"   |



## Buttons
### Button 
The button component is used to generates two variants of buttons, solid and clear. It can be used as button or as link. It defaults to solid variant and button usage. 

```
<Button>Simple button</Button>

<Button isAnchor url="login">Simple button as Link</Button>

<Button disabled>Disabled button</Button>

<Button variant="clear">Clear button</Button>
```


| Prop | Values |
| ----------- | ----------- |
| `variant` | String: `"clear"` or `"solid"`, defaults to solid |
| `disabled` | Boolean. Defaults to false |
|`isAnchor` | Boolean. Defaults to false|
|`url`| String. Route name from `routes.ts`, only usable when `isAnchor` is true|
|`target`|String. `a` Target attribute, only usable when `isAnchor` is true |
|`onClick`|Function. Optional onClick event |



### ButtonIcon 
This component hold the smaller CTA's made of text and one boxed icon on the right. It receives a prop with the icon and the box's color. Default values are a `<UNICONS.UilAngleRight />` for the icon and `primary` for the color.

```
<ButtonIcon icon={(<UNICONS.UilMultiply />)} color='error'>
    Button Icon Close
</ButtonIcon>
```

| Prop | Values |
| ----------- | ----------- |
| `color` | String: Any color from the `theme.colors` object located in `Layout.tsx`   |
| `icon` | React.ReactNode: Any icon from the `@iconscout/unicons` library |


## Form
### Tag 
Tag component is used in search filters, only takes the inner text.

### Radio and Radio Group


### Checkbox and Checkbox Group


### Search Input
This component is used in search filters. Receivess a Placeholder string, default placeholder is `"¿Cómo podemos ayudarte?"`

```
<InputSearch
    placeholder="Input Search with custom placeholder"
/>
```


### Input
For usage in most forms, takes a `type` prop, defaults to "text", a required `placeholder` string (wich serves as label) and an optional `icon` prop from `@iconscout/unicons`, 

```
<Input
    placeholder="Simple text input"
/>

<Input
    type="email"
    placeholder="Simple email input"
/>

<Input
    type="number"
    placeholder="Input type number with icon"
    icon={(<UNICONS.UilPhone />)}
/> 
```

| Prop | Values |
| ----------- | ----------- |
|`type`| String: HTML Input ype, defaults to "text" |
|`placeholder`| String. Required string that also serves as label |
| `icon` | React.ReactNode: Any icon from the `@iconscout/unicons` library |


### Select
Select component is built using [React Select](https://react-select.com/home), takes an `options` object with value and label pairs to initialize.


```
const selectOptions = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
]

<Select options={selectOptions}/>
```


## Cards

### Card Select Button 
This card is used to select/deselect items from a list, takes an `image` url (absolute or relative), a decription of the image: `imageAlt` and it's inner content.

| Prop | Values |
| ----------- | ----------- |
| `image` | String: Image url (absolute or relative) |
| `imageAlt` | String(optional): Short image description |

```
<CardSelectButton image="https://placeimg.com/135/120/any" imageAlt="Descriptive text">
    <Copy><strong>Card Select Button</strong></Copy>
    <Copy>Another text</Copy>
</CardSelectButton>
```

### Card Check Image 
Same as Card Select Button, but shows a radio check instead of an on/off button, also takes custom button text.

| Prop | Values |
| ----------- | ----------- |
| `image` | String: Image url (absolute or relative) |
| `imageAlt` | String(optional): Short image description |
| `btnText` | String: TExt to show for the CTA |

```
<CardCheckImage 
    image="https://placeimg.com/135/120/any" 
    imageAlt="Descriptive text"
    btnText="Ver destino">
    <Copy><strong>Card Check Image</strong></Copy>
    <Copy>Another text</Copy>
</CardCheckImage>
```

### Card Add

Used to Add/remove items from a list, text is fixed.

### Card Icon

Ilustrative card with text and icon, takes an `icon` and inner content;

```
<CardIcon icon={<UilChart />}>
    Card Icon with custom icon
</CardIcon>
```

| Prop | Values |
| ----------- | ----------- |
| `icon` | Component: Any icon from the `@iconscout/unicons` library |


### Card Icon Link

Clickable card for internal navigation with text and icon, takes `url`, `icon` and inner content;

```
<CardIconLink url="/linkurl" icon={(<UilCardAtm />)}>
    <Copy>Card with icon and Link</Copy>
</CardIconLink>
```

| Prop | Values |
| ----------- | ----------- |
| `url` | String: Route name fron `routes.ts` |
| `icon` | Component: Any icon from the `@iconscout/unicons` library |


###  Card Link
Card for internal navigation takes an `url`, an `image` url (absolute or relative), a decription of the image: `imageAlt` and it's inner content.

```
<CardLink 
    image="https://placeimg.com/135/120/any" 
    imageAlt="Descriptive text"
    url="/">
    <Copy><strong>Card Link</strong></Copy>
    <Copy>Another text</Copy>
</CardLink>
```

| Prop | Values |
| ----------- | ----------- |
| `image` | String: Image url (absolute or relative) |
| `imageAlt` | String(optional): Short image description |
| `url` | String: Route name fron `routes.ts` |

###  Card Link
Card for internal navigation with decorative image takes an `url`, an `image` url (absolute or relative) and it's inner content.

```
<CardIconOffset 
    url="/" 
    image="https://placeimg.com/80/80/any">
    <Copy color="gray"><strong>Realizá tu purdy quiz</strong></Copy>
    <Copy color="gray" size="sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit</Copy>
</CardIconOffset>
```

| Prop | Values |
| ----------- | ----------- |
| `image` | String: Image url (absolute or relative) |
| `url` | String: Route name fron `routes.ts` |


### Card Service

Cards for internal navigation inside the Services section. They take the service `slug`, an `icon`, a `background` color and inner text.

```
<CardService 
    slug="/test" 
    icon={(<UilCalendarAlt />)} 
    background="purple">
    <Copy>Agendar cita de mantenimento</Copy>
</CardService>
```


| Prop | Values |
| ----------- | ----------- |
| `slug` | String: Route name fron `routes.ts` |
| `background` | String: Any color from the `theme.colors` object located in `Layout.tsx`   |
| `icon` | Component: Any icon from the `@iconscout/unicons` library |


## Card Accordion

Expansible card, takes a `title` prop and inner text

```
<CardAccordion 
    title="Accordion Card Title">
    <Copy>Lorem ipsum dolor sit amet, ... consequat.</Copy>
</CardAccordion>
```

| Prop | Values |
| ----------- | ----------- |
| `title` | String |



## Card Accordion Check

Expansible card with togglable state for selecting items off a list, takes a `title` prop and inner text

```
<CardAccordionCheck 
    title="Accordion Card with Checkbox">
    <Copy>Lorem ipsum dolor sit amet, ... consequat.</Copy>
</CardAccordionCheck>
```

| Prop | Values |
| ----------- | ----------- |
| `title` | String |